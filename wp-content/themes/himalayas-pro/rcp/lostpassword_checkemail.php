<?php if( ! is_user_logged_in() ) : ?>

    <?php do_action( 'rcp_before_lostpassword_checkemail_message' ); ?>

    <h3 class="msg-resetPwd"><?php _e('Check your e-mail to complete resetting your password.', 'rcp'); ?></h3>

    <?php do_action( 'rcp_after_lostpassword_checkemail_message' ); ?>

<?php else : ?>
    <div class="rcp_logged_in"><a href="<?php echo wp_logout_url( home_url() ); ?>"><?php _e( 'Log out', 'rcp' ); ?></a></div>
<?php endif; ?>
