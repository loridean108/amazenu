<?php global $rcp_login_form_args; ?>
<?php if( ! is_user_logged_in() ) : ?>

	<div class="section-login">

	<form id="rcp_login_form"  class="rcp_form " method="POST" action="<?php echo esc_url( rcp_get_current_url() ); ?>">
		<h3> Log In </h3>
		<?php do_action( 'rcp_before_login_form_fields' ); ?>

		<fieldset class="rcp_login_data">
			<p class="col-sm-6" style="float: none;">
				<label for="rcp_user_login"><?php _e( 'Username', 'rcp' ); ?></label>
				<input name="rcp_user_login" id="rcp_user_login" class="required" type="text"/>
			</p>
			<p class="col-sm-6" style="float: none;">
				<label for="rcp_user_pass"><?php _e( 'Password', 'rcp' ); ?></label>
				<input name="rcp_user_pass" id="rcp_user_pass" class="required" type="password"/>
			</p>
			<?php do_action( 'rcp_login_form_fields_before_submit' ); ?>
			<p class="remember-wrapper">
				<input type="checkbox" name="rcp_user_remember" id="rcp_user_remember" value="1"/>
				<label id="rememberMe-label" for="rcp_user_remember"><?php _e( 'Remember Me', 'rcp' ); ?></label>
			</p>
			<p class="rcp_lost_password"><a href="<?php echo esc_url( add_query_arg( 'rcp_action', 'lostpassword') ); ?>"><?php _e( 'Lost your password?', 'rcp' ); ?></a></p>
			<p>
				<input type="hidden" name="rcp_action" value="login"/>
				<input type="hidden" name="rcp_redirect" value="<?php echo esc_url( $rcp_login_form_args['redirect'] ); ?>"/>
				<input type="hidden" name="rcp_login_nonce" value="<?php echo wp_create_nonce( 'rcp-login-nonce' ); ?>"/>
				
					<?php rcp_show_error_messages( 'login' ); ?>
				
				<input id="rcp_login_submit" type="submit" value="<?php _e( 'Sign In', 'rcp' ); ?>"/>
			</p>
			<?php do_action( 'rcp_login_form_fields_after_submit' ); ?>
		</fieldset>
	
		<?php do_action( 'rcp_after_login_form_fields' ); ?>
	</form>
    </div>
<?php else : ?>
	<script>
    	window.location = "<?php echo get_bloginfo('url') . '/desired-outcome/'; ?>";
	</script>
	<?php /*<div class="rcp_logged_in"><a href="<?php echo wp_logout_url( home_url() ); ?>"><?php _e( 'Log out', 'rcp' ); ?></a></div> */?>
<?php endif; ?>