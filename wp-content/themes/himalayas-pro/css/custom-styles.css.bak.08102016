
/*--------------------------------------------------------------

AMAZEN Custom styles

--------------------------------------------------------------*/


/* GLOBAL */

header {
    position: absolute;
    z-index: 9999;
    width: 100%;
}

.content-area .page-header {
    position: relative;
}

p, ul, ol { font-weight:500; }

.footer-nav li {
    font-size:13px;
}

#bottom-footer a {
    text-decoration: none;
}

.entry-content hr {
    margin: 2em 0;
    width: 90%;
    display: none;
}

pre {
    background: white;
}

.blog-hover-effect {
    display: none !important;
}

.author header {
    position: relative;
}

@media (min-width: 320px) and (max-width: 1024px ){ 
    .header-wrapper.non-transparent.stick, .header-wrapper.transparent.stick, .header-wrapper.non-transparent, .header-wrapper.transparent {
        position:relative !important;
        background: #ed5634 !important;
        transition: none !important;
        webkit-transition: none !important;
        -moz-transition: none !important;
        -ms-transition: none !important;
        -o-transition: none !important;
        height: 70px !important;
    }

    .header-wrapper.non-transparent, .header-wrapper.transparent {
        height: 85px !important;
    }

    #masthead .header-wrapper .logo img {
        width: 90%;
    }
}

@media (min-width: 768px) and (max-width: 1024px ){ 
    .tg-container {
        width: 100% !important;
        padding: 0 3% !important;
    }

    .section-howItWorks .about-btn .button {
        width: 25%;
    }

    .home-banner h3 {
        line-height: 1.4em;
        padding: 0 10%;
        margin-bottom: 15px;
    }

    .home-banner .caption-sub {
        padding: 0 10%;
    }
}

@media (max-width: 768px) {
    #masthead .logo {
        display: block;
    }
}


.home .header-wrapper.non-transparent {
    background: rgba(239, 238, 238, 0.2) none repeat scroll 0 0;
}

.home .header-wrapper.non-transparent.stick {
    background: #ed5634;
}

.site-content {
    background: #fff;
}


.page-id-447 {
    background: url(/wp-content/uploads/2016/01/blurred-bg.jpg) no-repeat;
}
.ui-state-default .ui-icon::before {
    content: '+';
}
.ui-state-active .ui-icon::before {
    content: '-';
}
.ui-state-default .ui-icon::before, .ui-state-default .ui-icon::after {
    font-family: 'open sans';
    position: absolute;
    top: -14px;
    right: 5px;
    font-size: 2em;
    font-weight: 500;
}

.ui-accordion .ui-accordion-header {
    margin-top:1em;
    background:#ed5634;
    color: white;
}

.ui-accordion-header-active, .ui-accordion-content-active {
    background: #ed5634 !important;
        color: white !important;
}

.ui-state-default a, .ui-state-default a:link, .ui-state-default a:visited {
    color: white;
}

.instructions {
    font-style: italic;
    font-size:14px;
display: inline;
}

@media (min-width: 320px) and (max-width: 767px ){ 
    .instructions {
        display: block;
    }
}

.ttl-desiredOutcome {
    margin-top: 1.45em;
}

.duration {
    color:#1f1b42;
    margin-bottom:0;
    font-size:14px;
}


.video-list .video-box h5 {
    margin-bottom: 5px;
}

.video {
    max-width: 600px;
    margin: 0 auto;
}

/* HOME - top banner */

.home-banner {
    background: url(/wp-content/uploads/2016/01/home-bg.jpg);
    height: 700px;
    width: 100%;
    background-size: cover;
    background-position: center;
}

.mobile-logo {
    display: none;
}

@media (min-width: 320px) and (max-width: 767px) {

    .mobile-logo {
        display: block;
        margin: 30% auto 0 auto;
        width: 130px;
    }
}

.home-banner h3 {
    margin: 0 auto 0 auto;
    font-size: 38px;
    line-height: 100px;
    width: 76%;
    text-transform: inherit;
    letter-spacing: 1px;
    text-align: center;
    color: #fff !important;
}


.slider-wrapper .tg-container {
    top: 25%;

}

@media (max-width: 1200px){
    .slider-wrapper .tg-container {
        left: 0;
}

}
@media (min-width: 320px) and (max-width: 979px) {

    .home-banner h3 {
        width: 100%;
        line-height: 50px;
        font-size: 34px;
        margin-bottom:1em;
    }

    .slider-wrapper .tg-container {
        top: 10%;
        text-align: center;
        left: 50%;
    }
}

@media (min-width: 768px) and (max-width: 979px) {

    .slider-wrapper .tg-container {
        top: 40%;
    }
}

.home-banner .button {
    margin: 50px auto 20px auto;
    display: block;
    width: 30%;
    text-align: center;
    background: #ED5634;
    border: none;
    padding: 20px;
}

.btn-border {
    border: 1px solid #FFF !important;
    background: none !important;
    margin: 0 auto !important;
}

.btn-border:hover {
    background: #F2F2F2 !important;
    -webkit-transition: background 0.4s ease-in-out;
    -moz-transition: background 0.4s ease-in-out;
    -ms-transition: background 0.4s ease-in-out;
    -o-transition: background 0.4s ease-in-out;
    transition: background 0.4s ease-in-out;
    color: #ED5634 !important;
    font-weight: 500;
}

@media (min-width: 320px) and (max-width: 767px) {
    .home-banner .button {
        width: 90%;
    }
}

@media (min-width: 768px) and (max-width: 979px) {
    .home-banner .button {
        width: 40%;
    }
}

.home-banner .button:hover {
    background: #C33211;
    -webkit-transition: background 0.4s ease-in-out;
    -moz-transition: background 0.4s ease-in-out;
    -ms-transition: background 0.4s ease-in-out;
    -o-transition: background 0.4s ease-in-out;
    transition: background 0.4s ease-in-out;
}

.home-banner .button:active {
    color: #fff;
}

.home-banner .caption-sub {
    margin: 0 auto;
}

.button {
    font-size: 18px;
    line-height: 13px;
    padding: 13px 18px;
    border: 1px solid #fff;
    color: #fff;
}

.button:hover {
    background: #ED5634;
    color: #fff;
}



/*HOW IT WORKS*/

.section-howItWorks {
    width: 70%;
    margin: 0 auto;
    padding: 120px 0 60px 0;
}

@media (min-width: 320px) and (max-width: 979px) {

    .section-howItWorks {
        width: 100%;
        padding: 0 5%;
    }
}

.section-howItWorks .tg-column-2 {
    width: 100%;
    margin: 0;
}

.section-howItWorks .about-btn .button {
    font-size: 18px;
    line-height: 13px;
    padding: 20px;
    border: 1px solid #fff;
    color: #fff;
    background: #26D3C2;
    border-radius: 0;
    font-weight: 500;
    margin: 50px auto 0 auto;
    width: 20%;
    display: block;
    text-align: center;
}

.section-howItWorks .button:hover {
    background: #00A08F;
    -webkit-transition: background 0.4s ease-in-out;
    -moz-transition: background 0.4s ease-in-out;
    -ms-transition: background 0.4s ease-in-out;
    -o-transition: background 0.4s ease-in-out;
    transition: background 0.4s ease-in-out;
}


@media (min-width: 320px) and (max-width: 767px) {

    .section-howItWorks .about-btn .button {
        width: 80%;
        margin: 50px auto 50px auto;
    }
}

@media (min-width: 768px) and (max-width: 979px) {

    .section-howItWorks .about-btn .button {
        margin: 50px auto 50px auto;
    }
}

@media (min-width: 768px) and (max-width: 1024px) {

    .section-howItWorks .about-btn .button, .cta-text-style-2 .cta-text-btn {
        width: 35%;
    }
}

.section-howItWorks h2 {
    margin: 0 auto 40px auto;
    letter-spacing: 1px;
    font-weight: 300;
    text-align: center;
    position: relative;
    z-index: 10;
    width: 30%;
    padding-bottom: 15px;
    border-bottom: 1px solid #F5D16E;
}


@media (min-width: 320px) and (max-width: 767px) {

    .section-howItWorks h2 {
        letter-spacing: 0;
        width: 70%;
    }
}

@media (min-width: 768px) and (max-width: 979px) {

    .section-howItWorks h2 {
        width: 32%;
    }
}

.section-howItWorks h4 {
    font-size: 26px;
    letter-spacing: 0;
    text-align: center;
    line-height: 1.6em;
    font-weight: 400;
    margin-bottom:5px;
}


.section-howItWorks .intro {
    text-align: center;
    width: 67% !important;
    font-size: 18px;
}

@media (min-width: 320px) and (max-width: 767px) {

    .section-howItWorks .intro {
        width: 100% !important;
    }
}

.section-howItWorks .about-content p {
    font-weight: 400;
    width: 80%;
    margin: 20px auto 0 auto;
    color: #1f1b42;
    font-weight: 500;
    text-align: center;
    font-size: 16px;
}

.section-howItWorks .col-wrapper {
    margin-top: 60px;
    overflow: hidden;
    margin-bottom: 50px;
}

.section-howItWorks .col-wrapper .col-4 {
    width: 33%;
    padding: 10px;
    float: left;
    display: inline-block;
}


@media (min-width: 320px) and (max-width: 767px) {

    .section-howItWorks .col-wrapper .col-4 {
        width: 100%;
    }
}

.section-howItWorks .col-wrapper .col-4 p {
    text-align: center;
}

.section-howItWorks .col-wrapper .col-4 hr {
    border: none;
    background: #ccc;
    height: 1px;
    margin: 20px auto;
    display: block;
    width: 90%;
}

.section-howItWorks .about-image {
    border-top: 1px solid #ccc;
    margin: 50px auto;
    padding: 100px 0;
    display: block;
}

@media (min-width: 320px) and (max-width: 767px) {
    .section-howItWorks .about-image {
        border-top: none;
        padding: 0;
    }

    .cta-text-desc {
        padding: 0 5%;
    }
}

.section-howItWorks .about-image img {
    display: block;
     margin: 50px auto;

}

.section-howItWorks .col-wrapper .col-4 i.fa {
    color: white;
    background:#C33211;
    display: inline-block;
    cursor: default;
    margin: 15px auto;
    width: 90px;
    height: 90px;
    border-radius: 50%;
    text-align: center;
    position: relative;
    display: block;
}


.section-howItWorks .col-wrapper .col-4 i.fa:before {
     color: white;
     display: block;
     font-size: 4em;
     padding-top: 13px;
}

.section-howItWorks .col-wrapper .col-4 i.fa-play:before {
    font-size: 3.5em;
    padding-top: 18px;
    padding-left: 10px;
}


.video-homepage {
    margin: 50px 0;
    overflow: hidden;
}

.video-homepage p {
font-size: 16px;
text-align: center;
margin-bottom: 40px;
}

@media (min-width: 320px) and (max-width: 1024px) { 
    .video-homepage {
        margin: 30px 0;
        overflow: hidden;
    }
}

.video-homepage h2 {
    width: 100%;
    font-size:30px;
}

.video-homepage .video-container {
    position: relative;
    padding-bottom: 22.5%;
    padding-top: 35px;
    height: 0;
    overflow: hidden;
    width: 45%;
    float: left;
    margin-left: 4%;
}


@media (min-width: 320px) and (max-width: 667px) { 
    .video-homepage .video-container {
        margin-left:0;
        float: none;
        width: 100%;
        padding-bottom: 47%;
        margin-bottom:40px;
    }
}

@media (min-width: 568px) and (max-width: 667px) { 
    .video-homepage .video-container {
        padding-bottom: 49%;
    }
}

@media (min-width: 768px) and (max-width: 1250px) { 
    .video-homepage .video-container {
        padding-bottom: 21%;
    }
}

.video-homepage  .video-container iframe {
    position: absolute;
    top:0;
    left: 0;
    width: 100%;
    height: 100%;
}


/*TESTIMONIALS*/

#testimonials {
    min-height: 485px;
}

@media (min-width: 768px) and (max-width: 1024px){
    #testimonials {
        min-height: 595px;
    }
}

@media (min-width: 320px) and (max-width: 767px) { 
    #testimonials {
        min-height: 845px;
    }
}


#testimonials .section-wrapper {
    padding: 130px 0 90px 0;
}

.testimonial-name {
    text-align: center;
    margin-top: 0;
    font-size:20px;
}

.testimonial-deg {
    text-align: center;
    display: block;
}

.testimonial-content {
    font-size:20px;
}

@media (min-width: 320px) and (max-width: 600px) { 

    .testimonial-name {
        font-size:18px;
    }
    
    .testimonial-content {
        font-size: 16px;
    }

}

.testimonial-img {
    display: none;
}

.fa-quote-left:before, .fa-quote-right:before {
    font-size: 24px;
}

.fa-quote-right, .fa-quote-left {
    line-height: 24px;
    margin: 0 10px;
} 




/*ABOUT US*/

.section-aboutUs {
    background: #26D3C2;
    padding: 60px 5% 100px 5%;
    position: relative;
    overflow: hidden;
}


@media (min-width: 320px) and (max-width: 767px) {

    .section-aboutUs {
        padding: 60px 10% 80px 10%;
    }
}

.section-aboutUs h2 {
    margin: 0 auto;
    color: #fff !important;
    letter-spacing: 1px;
    font-weight: 700;
    text-align: center;
    position: relative;
    z-index: 10;
    text-align: center;
    display: inline-block;
    padding-bottom: 15px;
    width: 100%;
}

.section-aboutUs h2:after {
    content: '';
    height: 1px;
    background: #f5d16e;
    width: 23%;
    display: block;
    text-align: center;
    margin: 10px auto;
}

.section-aboutUs .txt-wrapper {
    position: relative;
    margin: 30px 0;
    width: 60%;
    float:left;
}


/*@media (min-width: 320px) and (max-width: 767px) {

    .section-aboutUs .txt-wrapper.last img {
        margin-top: -240px;
    }
}


.section-aboutUs .txt-wrapper img {
    left: 50%;
    margin-top: -218px;
    position: absolute;
    margin-left: -79px;
    width: 158px;
}*/

.section-aboutUs p, .section-aboutUs h3 {
    color: #fff;
}

.section-aboutUs h3 {
    text-align: center;
    margin-top:30px;
}


.section-aboutUs p {
    padding: 0 5%;
    font-size:16px;
    text-align: left;
}

.section-aboutUs .img-about {
    margin-top: 100px;
    float:right;
    width: 40%;
}

@media (min-width: 320px) and (max-width: 767px) {
    
    .section-aboutUs .txt-wrapper {
        width: 100%;
    }

    .section-aboutUs .img-about  {
        display: none;
    }

    .section-aboutUs p {
        padding: 0;
    }
}


/*FUN FACTS WIDGET*/

@media (min-width: 320px) and (max-width: 767px) {

    #himalayas_fun_facts_widget-2 .sub-title {
        padding: 0 10%;
    }
}


#himalayas_fun_facts_widget-2 h2 {
    color: #fff !important;
    padding-bottom: 15px;
    border-bottom: 1px solid #F5D16E;
}

#himalayas_fun_facts_widget-2 h2:before, #himalayas_fun_facts_widget-2 h2:after {
    border: none !important;
    content: ''
}

#himalayas_fun_facts_widget-2 .sub-title {
    font-family: "Open Sans", sans-serif;
    font-size: 18px;
    color: #fff !important;
}

.icon-wrap:hover, .image-wrap:hover, .port-link a:hover, .counter-block-wrapper,  {
    border-color: white;
}

.counter-block-wrapper .counter {
    color: white !important;
}

.counter-block-wrapper .counter-text {
    font-size: 18px;
}

 @media (min-width: 676px) and (max-width: 1080px) {

    .widget_fun_facts .counter-icon {
        font-size:15px;
    }
    .counter-block-wrapper .counter-text {
        font-size: 16px;
    }
}



/*EXPERIENCE*/

.section-experience {
    padding-top: 150px !important;
}


@media (min-width: 320px) and (max-width: 767px) {

    .section-experience h2 {
        letter-spacing: 0;
        font-size: 20px;
    }

    .counter-text {
        font-size:18px;
        font-weight: 500;
    }
}

.section-experience .sub-title {
    padding: 0 20%;
    margin-top: 1.8em;
}

@media (min-width: 320px) and (max-width: 767px) {

    .section-experience .sub-title {
        padding: 0 10%;
        font-size: 18px;
    }
}

.section-experience .col-6 {
    width: 45%;
    margin: 0 2.5%;
    float: left;
}

@media (min-width: 320px) and (max-width: 767px) {

    .section-experience .col-6 {
        width: 100%;
        margin: 0;
        padding: 0 10%;
    }
}

.section-experience .col-6 h5 {
    font-size: 1.4em;
}

.section-experience .col-6 h5, .section-experience .col-6 p {
    text-align: center;
}

.added-bottom {
    float: left;
    padding: 10px;
}


/*TEAM*/

@media (min-width: 667px) and (max-width: 768px) {

    .team-content-wrapper .tg-column-3 {
        float: left;
            margin: 0 2.5%;
        width: 28.33%;
    }

    .team-desc-wrapper {
        padding:5px;
    }

    .team-desc-wrapper .team-name {
        margin-top:-2px;
    }
}

/* PORTFOLIO*/

#portfolio {
    background: #F5D16E;
}

#portfolio .section-title-wrapper {
    margin-bottom:50px;
}

.Portfolio-content-wrapper {
    padding: 0 2em;
}

.portfolio-images-wrapper {
    padding: 15px;
}


/*BLOG*/

.blog-block {
    background: #F5D16E;
}

.blog-block p {
    font-weight: 500;
}

.blog-block .posted-date {
     font-weight: 500;
}

.blog-block .blog-readmore {
    background: #ed5634;
    padding: 5px 10px;
    font-weight: 500;
    color: white;
}

/*CTA widget*/
.cta-text-style-2 .cta-text-btn {
    background: white;
    color: #1f1b42;
    border: none;
}
.cta-text-style-2 .cta-text-btn:hover  {
    background: #1f1b42;
    color: #F5D16E !important;
    border: none;
}

.cta-text-style-2 p {
    color: #1f1b42;
    font-weight:500;
}
.cta-text-style-2 h2 {
    color: #1f1b42 !important;
}

/*CONTACT*/
#contact .sub-title {
    font-size: 18px;
}
}
    


/*SEARCH RESULTS*/

.search-results h1 {
    margin-bottom: 50px;
}
.search-results article {
    border-bottom: 1px dashed #1F1B42 !important;
}

.search-results article  .entry-thumbnail {
    width: 32%;
}

.search-result-item {
    background:#f7f7f7;
    margin: 30px 0;
    border-radius: 20px;
    padding: 20px;
}

.search-result-item article {
    border-bottom: none !important;
    margin-bottom: 0 !important;
    padding-bottom: 0 !important;
}

.search-result-item .video {
    max-width:100%;
}

.search-result-item h2.entry-title {
    margin-bottom: 0;
    text-align: left;
}

.search-result-item .video h2.entry-title a:before {
    content:'Video: ';
    margin-right: 10px;
}

.search-result-item .entry-content {
    display: none;
}

.portfolio-hover {
    display: none;
}



/*404 ERROR PAGE*/
.error404 {
    background: url(/wp-content/uploads/2016/01/blurred-bg.jpg) no-repeat;
    background-size: 100%;
}

@media (min-width: 320px) and (max-width: 767px) {

   .error404 {
        background-size: inherit;
    }
}

.error404 .site-content {
    background: none;
}

.error404 .page-title  {
    color: white;
}

.error404 .page-header {
    position: relative;
}

.error404 .page-content p {
     color: white;
}

.widget.widget_image_gallery_block ul li {
    height:55px;
    border-bottom: none !important;

}

.widget.widget_image_gallery_block ul li img {
    height: 100%;
    margin: 0 auto;
    display: block;
}




/*INNER PAGES*/
.category-selector {
    overflow: hidden;
    display: block;
}

.post-384 {
    min-height:300px;
}

.grades-selector {
    margin-top:40px;
}

.category-box {
    float: left;
    border-radius: 5px;
    margin: 20px 10px;
    width: 93%;
    padding: 15px;
    min-height: 130px;
    border: 1px solid #d3d3d3;
    
}

.category-box a {
    display: block;
    border-radius: 5px;
    padding: 15px 0;
    box-shadow: 2px 2px 5px #d1d1d1;
    background: #26D3C2;
    text-align: center;
    margin-bottom: 20px;
    min-height: 50px;
    text-align: center;
}

.category-box h4 {
    margin: 0;
}

.category-box a:hover {
    background: #00A08F;
}





.grades-selector .category-box, .grades-selector .category-box:hover, .activity-selector .category-box, .activity-selector .category-box:hover{
    background: #fff !important;
    border: none;
    padding: 0;
    min-height:0;
    margin: 0 17px 0 0;
}

@media (min-width: 320px) and (max-width: 767px) {

    .grades-selector, .activity-selector {
        margin-top: 30px;
    }

   .grades-selector .category-box{
        width: 100%;
        padding: 0;
        min-height: 50px;
        margin: 0;
    }

    .activity-selector .category-box {
        width: 99%;
        padding: 0;
        min-height: 50px;
        margin: 10px 0;
    }
}

@media (min-width: 768px) and (max-width: 1000px) {

    .video-list .video-box {
        width:25%;
        margin: 20px 20px 20px 0;

    }
    .activity-selector .category-box {
        width: 44%;
        min-height: auto;
        padding: 0;
    }
}

@media (min-width:1000px) and (max-width:1024px) {
    .activity-selector .category-box {
        width: 18%;
        margin: 15px 5px 0 5px;
        padding: 0;
    }
}

.no-results-found {
    margin-top:30px;
}


.category-box p {
    color: #1f1b42;

}

.category-box:hover h4{
    color: #fff;
}


.video-list {
    /*margin-bottom: 2em;*/
}

.video-box {
    display: inline-block;
    margin: 0 20px 20px 0;
    width: 100%;
    background: #f7f7f7;
    padding: 20px;
    border-radius:10px;
}

.video-box:hover {
    box-shadow: 2px 2px 5px #d1d1d1;
}

.video-list .video-box h5 {
    margin-top: 10px;
    text-align: left;
    min-height: 40px;
    font-size:15px;
}

.my-favorites {
    min-height: 300px;
}

.my-favorites .video-box:hover {
    box-shadow: none;
}

.my-favorites .video-box h5 {
    text-align: left;
    margin-top: 0;
    font-size: 18px;
}

.video-box .content {
    position: relative;
}

.video-box .video-play {
    position: absolute;
    width: 100px;
    height: 100px;
    left: 50%;
    top: 50%;
    margin-left: -50px;
    margin-top: -50px;
}

h2.entry-title {
    margin-bottom: 0 !important;
    font-size: 30px;
    line-height: 1.3;
    text-transform: none;
}

h3.entry-title {
    font-size: 24px;
    margin: 2em 0 10px 0;
}

.entry-title span {
    background: #ED5634;
    color: #fff;
    margin: 0 10px;
    padding: 5px 20px;
    text-align: center;
    font-weight:300;
    border-radius: 5px;
}

.video h2.entry-title {
    margin-bottom: 1em;
}

.video-wrapper {
    position: relative;
    padding-bottom: 52%;
    padding-top: 35px;
    height: 0;
    overflow: hidden;
}

.video-wrapper iframe {
    position: absolute;
    top:0;
    left: 0;
    width: 100%;
    height:100%;
}

@media (min-width:320px) and (max-width:640px) { 
    .video-wrapper {
        padding-bottom: 47%;
    }
}


.link-savedActivities {
    display: block;
    font-weight: 500;
    margin-top: 15px;
    float:right;
}

@media (min-width:320px) and (max-width: 480px) { 
    .link-savedActivities {
        float: none;
    }
}

span.wpfp-span {
    background: #ED5634;
    padding: 8px 30px;
    color: #fff;
    display: inline-block;
    font-size: 16px;
    font-weight: 500;
    border-radius: 5px;
}
img.wpfp-img {
    margin-right: 15px;
    margin-top: -5px;
}
a.wpfp-link {
    color: #fff;
    padding: 0 20px 0 40px;
    margin-left: -51px;
    margin-right: -20px;
    font-size: 16px;
    font-family: 'Open Sans';
    font-weight: 500;
    font-style: normal;
    display: inline-block;
}

a.wpfp-link .fa {
    font-size: 1.3em;
    margin-right: 8px;
    margin-top: 2px;
    float: left;
}

.my-favorites .video-box {
    width: 100%;
    margin: 0;
    margin-bottom: 20px;
}

.my-favorites .video-box > div {
    float:left;
}
.my-favorites .video-box .video-content {
    width: 30%;
}
.my-favorites .video-box .video-categories {
    width: 36%;
    margin-left: 2%;
    margin-top: 75px !important;
}
.my-favorites .video-box .btn-remove {
    text-align: right;
    width: 30%;
    float: left;
    margin-top: 75px !important;
}

.btn-remove a.wpfp-link {
    background: #ED5634;
    margin: 0;
    padding: 10px 20px;
    float: right;
    border-radius: 5px;
}
.category-list {
    list-style: none;
}
.video-categories table {
    width: 100%;
}
.video-categories table th, .video-categories table td {
    border: none;
    padding: 10px;
}

.video-categories table th {
    text-align:right;
    width: 155px;
}

.btn-yellow {
    background: #F5D16E;
    border-radius:5px;
    width: 100%;
    padding: 5px 10px;
}

table.rcp-table th {
    background: #F5D16E !important;
}

table.rcp-table th, table.rcp-table td {
    padding: 10px !important;
}

#rcp_profile_editor_form {
    background: #26D3C2;
    padding: 2em 3em;
    width: 55%;
    margin: 0 auto;
    border-radius: 10px;
}

#rcp_profile_editor_form fieldset h2 {
    font-family: "Oswald";
        font-size: 1.5em;
    padding: 1em 0;
    margin-bottom: 0;
}

#rcp_profile_submit_wrap input {
    border-radius:5px;
    margin-top: 2em;
    width: 100%;
    text-align: center;
    min-height: 40px;
    font-size:16px;
}

.rcp_success {
    width: 55%;
    margin: 1em auto !important;
}

.post-451 h1 {
    text-align: center;
}

article {
    border: none !important;
    padding-bottom:0;
}

#main {
    padding-top:2.5em;
}

.btn-login a {
    background: white;
    padding: 5px 13px;
    border-radius: 5px;
    border: 1px solid white;
    color: #1f1b42 !important;
    text-transform: capitalize !important;
}

.btn-login a:hover {
    color: white !important;
    border: 1px solid white;
    background: none;
}

.stick .btn-login a {
    color:  #C33211 !important;
}

.stick .btn-login a:hover {
    color:  #fff !important;
}

.widget_nav_menu .btn-login a {
    background: none !important;
    color: white !important;
    border: none !important;
    padding: 0;
}

#post-447 div {
    color: white;
}

@media (min-width: 320px) and (max-width: 374px ){ 
    input[type="text"], input[type="email"], input[type="url"], input[type="password"], input[type="search"], select, #rcp_profile_submit_wrap input {
        width: 83% !important;
    }

    .section-login input[type="submit"], #rcp_login_submit {
         width: 83% !important;
    }
}

@media (min-width: 320px) and (max-width: 768px ){ 

    #site-navigation .menu li a {
        display: block;
        width: 100%;
    }

    #site-navigation .menu li a:hover {
        color: #ed5634 !important;
    }

    .home-banner .button {
        line-height: 1.4em;
        padding: 10px;
    }

    #post-450 .entry-content {
        overflow-x: scroll;
    }

    #post-450 .entry-content #rcp-account-overview, #post-450 .entry-content #rcp-payment-history {
        min-width: 600px;
    }

    .widget_nav_menu .btn-login a {
        text-transform: capitalize !important;
    }

    #site-navigation .menu-primary-container, #site-navigation div.menu {
        background:#1f1b42;
        top:64px;
    }

    
    #site-navigation .menu .btn-login {
        border-bottom: none !important;
    }
    .btn-login a {
        border: none;
        color: white !important;
        background: none !important;
        text-transform: uppercase !important;
        padding: 0;
    }
    .btn-login a:hover {
        border: none;
    }

    .service-read-more:hover, .cta-text-btn:hover, .blog-readmore:hover, #site-navigation .menu li:hover a, #site-navigation .menu li.current-one-page-item a, .header-wrapper.stick #site-navigation .menu li:hover a, .header-wrapper.stick #site-navigation .menu li.current-one-page-item a, .header-wrapper.no-slider #site-navigation .menu li:hover a, .header-wrapper.no-slider #site-navigation .menu li.current-one-page-item a {
        color: #ed5634 !important;
    }

    .stick .btn-login a {
        color: white !important;
    }
    
    #rcp_profile_editor_form {
        width: 95%;
        padding: 2em 1.5em;
    }
    
    .rcp_success {
        width: 90%;
    }
    
    .rcp_form .col-sm-6 {
        padding: 10px 0 0 0 !important;
    }
    
    
    #post-387 h2.entry-title, #post-389 h2.entry-title  {
        width: 100%;
    }
    
    .video-box {
        width: 81%;
        margin: 0 auto 10px auto;
        display: block;
    }

    /*.video-box iframe {
        height: 37.25vw;
         margin: 0 auto;
        display: block;
    }*/

    .my-favorites {
        min-height: 150px;
    }

    .my-favorites .video-box > div {
        width: 100% !important;
    }

    .my-favorites .video-box .video-categories {
        margin-top: 25px !important;
    }

    .my-favorites .video-box .video-categories table th {
        width: 110px;
        text-align: left;
    }

    .my-favorites .video-box .btn-remove {
        margin-top: 25px !important;
        text-align: center;
    }

    .my-favorites .video-box .btn-remove a.wpfp-link {
        float: none;
        margin: 0 auto;
        display: block;
        text-align: center;
        margin-top:0;
    }

    .my-favorites .video-box .btn-remove a.wpfp-link .fa {
        float: none;
    }

    .my-favorites .video-thub {
        display: block;
        margin: 0 auto;
    }
    
    .activity-selector .category-box a {
        margin-bottom: 0;
    }

    .filter-panel {
        margin-left:0;
    }

    .filter-panel > li {
        display: block;
        margin-bottom: 15px;

    }
}


@media (min-width: 768px) and (max-width: 1023px ){ 
    .rcp_form .col-sm-6 {
        padding: 20px 20px 0 0 !important;
    }

    #rcp_profile_editor_form {
        width: 70% !important;
    }

    .video-box {
        display:inline-block;
    }


}


/*LOGIN & REGISTER & MEMBERS ONLY*/

.page-id-447, .page-id-461, .page-id-453 {
    background: url(/wp-content/uploads/2016/01/blurred-bg.jpg) no-repeat;
    background-size:100%;
} 

.page-id-447 .site-content, .page-id-461 .site-content, .page-id-453 .site-content {
    background-color: transparent;
}

.page-id-453 main {
    min-height: 500px;
}

.page-id-453 h1 {
    margin-top: 2em;
    font-size: 3.5em !important;
    margin-bottom: 1em !important;
}

.page-id-453 h1, .page-id-453 p {
    color: white;
    text-align: center;
}

.section-login {
    margin: 0 auto;
    width: 45%; 
}

.section-register {
    margin: 0 auto;
    width: 60%; 
    border-radius: 10px;
    background: #26D3C2;
    
}

.section-login form {
    border-radius: 10px;
    background: #26D3C2;
    padding: 4em 5em !important;
}

.section-register form {
    padding: 4em 5em !important;
}

.section-register h2 {
    text-align: center;
    margin-bottom: 2em;
}

.rcp_form select {
    height: 44px;
    border-radius: 5px;
    -webkit-appearance:none;
}

#rcp_zip {
    width: 50%;
}

.section-login form .col-sm-6 {
    width: 100%;
    margin: 0;
    padding-right: 0 !important;
}

.section-login .rcp_form p > label {
    color: white;
}

.remember-wrapper {
    margin-top: 15px !important;
}

#rememberMe-label {
    display: inline-block;
    width: auto !important;
    margin: 0;
}

.rcp_form input[type="checkbox"] {
    width:15px;
    height: 15px;
    margin:3px 8px 0 0 !important;
}

#rcp_card_exp_wrap label {
    width: 100%;
}

#rcp_card_exp_wrap select {
    width: 30%;

}

.rcp_discounts_fieldset label {
    width: 100% !important;

}
.rcp_discount_invalid {
    font-weight: 500;
}

#rcp_subscription_levels li {
    font-weight: 500;
}

.rcp_lost_password {
    margin-top: 1em;
}

#rcp_login_submit, #rcp_submit, #rcp_lostpassword_submit, #rcp_password_submit {
    width: 100%;
        padding: 15px;
        height: 42px;
        font-size: 16px;
    margin-top: 1em;
        border-radius:5px;
}

#rcp_registration_form h3 {
    margin-top: 2em;
    display:block;
    width: 100%;
    overflow: hidden;       
}

.rcp_subscription_fieldset h3 {
    margin-top: 1em !important;
}

.rcp_form input[type="text"], .rcp_form input[type="password"], .rcp_form input[type="email"], .rcp_state {
    height: 45px;
    border-radius:5px;
}

.rcp_form p > label {
    font-weight: 500;
    line-height: 1.4em;
    margin-top: 15px;
        margin-bottom: 5px;
}

.rcp_form .col-sm-6 {
    padding: 20px 20px 0 0;
}

.rcp_message.error {}

p.rcp_error {
    width: 100%;
    background: #C33211 !important; 
    border-radius:5px !important;
    border: none !important;
    padding: 0;
}

#post-450 p.rcp_success {
    margin: 0 0 2em 0 !important;
}

p.rcp_success span, p.rcp_error span {
    display: block;
    text-align:left !important;
    padding: 0 0 0 10px !important;
    
}

p.rcp_error span {
    color: #fff;
    letter-spacing:0;
    text-align: center;
     text-transform: capitalize;
        font-size: 13px;
        font-weight:normal;
}

.msg-resetPwd {
    text-align: center;
    color:#26D3C2;
    padding: 3em 0 7em 0;
    letter-spacing: 0.5px;
}

#rcp_apply_discount {
    border-radius: 5px;
    min-height: 43px;
    padding: 0 5%;
    font-size: 14px;
}

#rcp_auto_renew_wrap label {
    margin:0;
}

.rcp_message {
    background: none !important;
    padding: 0 !important;
}


.icon-number {
    width: 50px;
    height: 50px;
    background:#ed5634;
    color: #fff;
    border-radius: 50%;
    font-weight: 700;
    margin-right: 10px;
    display: inline-block;
    text-align: center;
}

.icon-number .fa {
    font-size: 0.8em;
    padding-top:14px;
        padding-left: 6px;
}


select {
    padding: 5px 8px;
    width: 100%;
}


.rcp_card_cvc.card-cvc, .rcp_card_zip.card-zip {
    min-width: 150px;
    width: 20%;
}

#page header div.header-wrapper {
    margin-bottom: 0;
}
#page div.site-content {
    padding-top: 100px;
}

body.home #page header div.header-wrapper {
    margin-bottom: 70px;
}
body.home #page div.site-content {
    padding-top: 0px;
}
.filter-panel {
    display: inline-block;
    list-style: outside none none;
}
.filter-panel > li {
    display: inline-block;
}
.btn.btn-filter {
    background: #ed5634 none repeat scroll 0 0;
    border-radius: 5px;
    color: #fff;
    font-family: "Oswald";
    font-size: 20px;
    font-weight: 300;
    margin: 0 5px;
    padding: 5px 30px;
    text-align: center;
}
.col-xs-6 {
    width: 50%;
    float: left;
    padding: 0 25px 0 0 !important;
}
.col-xs-12 {
    width: 100%;
    float: left;
    padding: 0 25px 0 0 !important;
}

@media (min-width: 320px) and (max-width: 767px ){
    .section-login, .section-register {
            width: 100%; 
    }
    
    .section-login form, .section-register form {
        padding: 2em 1.5em !important;
    }
    
    .section-login form .col-sm-6 {
        width: 95%;
    }
    
    .section-login input[type="submit"], #rcp_login_submit {
        width: 95%;
    }
    
    .tg-container {
        width: 97%;
        padding: 0 3%;
    }

}

@media (min-width: 768px) and (max-width:1023px){
    .outcome-selector .category-box {
        padding:12px;
    } 
}

@media (min-width: 768px) {
    
    .col-sm-6 {
        width: 50%;
        float: left;
        padding: 0 25px 0 0 !important;
    }
    .col-sm-12 {
        width: 100%;
        float: left;
        padding: 0 25px 0 0 !important;
    }  
    
    .outcome-selector .category-box {
        width: 30%;
        min-height: 195px;
    }  

    .category-box {
        width: 44%;
    }

    .video-box {
        width: 44%;
    }
}

@media (min-width: 992px) {

    .outcome-selector .category-box {
        width: 30%;
    min-height: 175px;
    }

    .category-box {
        width: 17%;
    }

    .video-box {
        width: 20%;
    }
}
