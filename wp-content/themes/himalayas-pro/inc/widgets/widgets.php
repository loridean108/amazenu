<?php
/**
 * Contains all the functions related to sidebar and widget.
 *
 * @package ThemeGrill
 * @subpackage Himalayas Pro
 * @since Himalayas Pro 1.0
 */

add_action( 'widgets_init', 'himalayas_widgets_init');
/**
 * Function to register the widget areas(sidebar) and widgets.
 */
function himalayas_widgets_init() {

   /**
    * Registering widget areas for front page
    */
   // Registering main Right Sidebar
   register_sidebar( array(
      'name'            => esc_html__( 'Right Sidebar', 'himalayas' ),
      'id'              => 'himalayas_right_sidebar',
      'description'     => __( 'Shows widgets at Right side.', 'himalayas' ),
      'before_widget'   => '<aside id="%1$s" class="widget %2$s clearfix">',
      'after_widget'    => '</aside>',
      'before_title'    => '<h4 class="widget-title"><span>',
      'after_title'     => '</span></h4>'
   ) );
   // Registering main Left Sidebar
   register_sidebar( array(
      'name'            => esc_html__( 'Left Sidebar', 'himalayas' ),
      'id'              => 'himalayas_left_sidebar',
      'description'     => __( 'Shows widgets at Left side.', 'himalayas' ),
      'before_widget'   => '<aside id="%1$s" class="widget %2$s clearfix">',
      'after_widget'    => '</aside>',
      'before_title'    => '<h4 class="widget-title"><span>',
      'after_title'     => '</span></h4>'
   ) );
   // Registering WooCommerce Sidebar
   if ( class_exists('woocommerce') ) {
      register_sidebar( array(
         'name'            => __( 'Shop Sidebar', 'himalayas' ),
         'id'              => 'himalayas_woocommerce_sidebar',
         'description'     => __( 'Show widgets at WooCommerce Pages.', 'himalayas' ),
         'before_widget'   => '<aside id="%1$s" class="widget %2$s  clearfix">',
         'after_widget'    => '</aside>',
         'before_title'    => '<h4 class="widget-title"><span>',
         'after_title'     => '</span></h4>'
      ) );
   }
   // Registering the Front Page Sidebar
   register_sidebar( array(
      'name'            => esc_html__( 'Front Page Sidebar', 'himalayas' ),
      'id'              => 'himalayas_front_page_section',
      'description'     => __( 'Show widgets at Front Page Content Section', 'himalayas' ),
      'before_widget'   => '<section id="%1$s" class="widget %2$s clearfix">',
      'after_widget'    => '</section>',
      'before_title'    => '<h2 class="main-title">',
      'after_title'     => '</h2>'
   ) );
   // Registering Error 404 Page sidebar
   register_sidebar( array(
      'name'            => esc_html__( 'Error 404 Page Sidebar', 'himalayas' ),
      'id'              => 'himalayas_error_404_page_sidebar',
      'description'     => __( 'Shows widgets on Error 404 page.', 'himalayas' ),
      'before_widget'   => '<aside id="%1$s" class="widget %2$s clearfix">',
      'after_widget'    => '</aside>',
      'before_title'    => '<h3 class="widget-title"><span>',
      'after_title'     => '</span></h3>'
   ) );
   // Registering Footer Sidebar One
   register_sidebar( array(
      'name'            => esc_html__( 'Footer Sidebar One', 'himalayas' ),
      'id'              => 'himalayas_footer_sidebar_one',
      'description'     => __( 'Shows widgets on footer.', 'himalayas' ),
      'before_widget'   => '<aside id="%1$s" class="widget %2$s clearfix">',
      'after_widget'    => '</aside>',
      'before_title'    => '<h4 class="widget-title"><span>',
      'after_title'     => '</span></h4>'
   ) );
   // Registering Footer Sidebar Two
   register_sidebar( array(
      'name'            => esc_html__( 'Footer Sidebar Two', 'himalayas' ),
      'id'              => 'himalayas_footer_sidebar_two',
      'description'     => __( 'Shows widgets on footer.', 'himalayas' ),
      'before_widget'   => '<aside id="%1$s" class="widget %2$s clearfix">',
      'after_widget'    => '</aside>',
      'before_title'    => '<h4 class="widget-title"><span>',
      'after_title'     => '</span></h4>'
   ) );
   // Registering Footer Sidebar Three
   register_sidebar( array(
      'name'            => esc_html__( 'Footer Sidebar Three', 'himalayas' ),
      'id'              => 'himalayas_footer_sidebar_three',
      'description'     => __( 'Shows widgets on footer.', 'himalayas' ),
      'before_widget'   => '<aside id="%1$s" class="widget %2$s clearfix">',
      'after_widget'    => '</aside>',
      'before_title'    => '<h4 class="widget-title"><span>',
      'after_title'     => '</span></h4>'
   ) );

   register_widget( "himalayas_about_us_widget" );
   register_widget( "himalayas_service_widget" );
   register_widget( "himalayas_call_to_action_widget" );
   register_widget( "himalayas_portfolio_widget" );
   register_widget( "himalayas_featured_posts_widget" );
   register_widget( "himalayas_our_team_widget" );
   register_widget( "himalayas_contact_widget" );
   register_widget( "himalayas_image_gallery_widget" );
   register_widget( "himalayas_testimonial_widget" );
   register_widget( "himalayas_our_clients_widget" );
   register_widget( "himalayas_fun_facts_widget" );
}

/**************************************************************************************/

/**
 * About us section.
 */
class himalayas_about_us_widget extends WP_Widget {
   function __construct() {
      $widget_ops = array( 'classname' => 'widget_about_block', 'description' => __( 'Show your about page.', 'himalayas' ) );
      $control_ops = array( 'width' => 200, 'height' =>250 );
      parent::__construct( false, $name = __( 'TG: About Widget', 'himalayas' ), $widget_ops, $control_ops);
   }

   function form( $instance ) {
      $defaults[ 'about_menu_id' ] = '';
      $defaults[ 'title' ] = '';
      $defaults[ 'text' ] = '';
      $defaults[ 'page_id' ] = '';
      $defaults[ 'default_button_text' ] = '';
      $defaults[ 'default_button_url' ] = '';
      $defaults[ 'default_button_icon' ] = '';
      $defaults[ 'button_text' ] = '';
      $defaults[ 'button_url' ] = '';
      $defaults[ 'button_icon' ] = '';

      $instance = wp_parse_args( (array) $instance, $defaults );

      $about_menu_id = esc_attr( $instance[ 'about_menu_id' ] );
      $title = esc_attr( $instance[ 'title' ] );
      $text = esc_textarea( $instance['text'] );
      $page_id = absint( $instance[ 'page_id' ] );
      $default_button_text = esc_attr( $instance[ 'default_button_text' ] );
      $default_button_url = esc_url( $instance[ 'default_button_url' ] );
      $default_button_icon = esc_attr( $instance[ 'default_button_icon' ] );
      $button_text = esc_attr( $instance[ 'button_text' ] );
      $button_url = esc_url( $instance[ 'button_url' ] );
      $button_icon = esc_attr( $instance[ 'button_icon' ] );
      ?>
      <p><?php _e( 'Note: Enter the About Section ID and use same for Menu item. Only used for One Page Menu.', 'himalayas' ); ?></p>

      <p>
         <label for="<?php echo $this->get_field_id( 'about_menu_id' ); ?>"><?php _e( 'About Section ID:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'about_menu_id' ); ?>" name="<?php echo $this->get_field_name( 'about_menu_id' ); ?>" type="text" value="<?php echo $about_menu_id; ?>" />
      </p>

      <p>
         <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
      </p>

      <?php _e( 'Description','himalayas' ); ?>
      <textarea class="widefat" rows="5" cols="20" id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>"><?php echo $text; ?></textarea>

      <p><?php _e('Select a page to display Title, Excerpt and Featured image.', 'himalayas') ?></p>
      <label for="<?php echo $this->get_field_id( 'page_id' ); ?>"><?php _e( 'Page', 'himalayas' ); ?>:</label>
      <?php wp_dropdown_pages( array( 'show_option_none' =>' ','name' => $this->get_field_name( 'page_id' ), 'selected' => $instance[ 'page_id' ] ) ); ?>

      <p>
         <label for="<?php echo $this->get_field_id( 'default_button_text' ); ?>"><?php _e( 'Button Text:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'default_button_text' ); ?>" name="<?php echo $this->get_field_name( 'default_button_text' ); ?>" type="text" value="<?php echo $default_button_text; ?>" />
      </p>
      <p>
         <label for="<?php echo $this->get_field_id( 'default_button_url' ); ?>"><?php _e( 'Button Redirect Link:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'default_button_url' ); ?>" name="<?php echo $this->get_field_name( 'default_button_url' ); ?>" type="text" value="<?php echo $default_button_url; ?>" />
      </p>
      <p>
         <label for="<?php echo $this->get_field_id( 'default_button_icon' ); ?>"><?php _e( 'Button Icon Class:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'default_button_icon' ); ?>" name="<?php echo $this->get_field_name( 'default_button_icon' ); ?>" placeholder="fa-refresh" type="text" value="<?php echo $default_button_icon; ?>" />
      </p>

      <p>
         <label for="<?php echo $this->get_field_id( 'button_text' ); ?>"><?php _e( 'Button Text:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'button_text' ); ?>" name="<?php echo $this->get_field_name( 'button_text' ); ?>" type="text" value="<?php echo $button_text; ?>" />
      </p>
      <p>
         <label for="<?php echo $this->get_field_id( 'button_url' ); ?>"><?php _e( 'Button Redirect Link:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'button_url' ); ?>" name="<?php echo $this->get_field_name( 'button_url' ); ?>" type="text" value="<?php echo $button_url; ?>" />
      </p>
      <p>
         <label for="<?php echo $this->get_field_id( 'button_icon' ); ?>"><?php _e( 'Button Icon Class:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'button_icon' ); ?>" name="<?php echo $this->get_field_name( 'button_icon' ); ?>" placeholder="fa-cog" type="text" value="<?php echo $button_icon; ?>" />
      </p>
      <p>
         <?php
         $url = 'http://fontawesome.io/icons/';
         $link = sprintf( __( '<a href="%s" target="_blank">Refer here</a> For Icon Class', 'himalayas' ), esc_url( $url ) );
         echo $link;
         ?>
      </p>
   <?php }

   function update( $new_instance, $old_instance ) {
      $instance = $old_instance;
      $instance[ 'about_menu_id' ] = strip_tags( $new_instance[ 'about_menu_id' ] );
      $instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );

      if ( current_user_can('unfiltered_html') )
         $instance[ 'text' ] =  $new_instance[ 'text' ];
      else
         $instance[ 'text' ] = stripslashes( wp_filter_post_kses( addslashes( $new_instance[ 'text' ] ) ) ); // wp_filter_post_kses() expects slashed

      $instance[ 'page_id' ] = absint( $new_instance[ 'page_id' ] );
      $instance[ 'default_button_text' ] = strip_tags( $new_instance[ 'default_button_text' ] );
      $instance[ 'default_button_url' ] = esc_url_raw( $new_instance[ 'default_button_url' ] );
      $instance[ 'default_button_icon' ] = strip_tags( $new_instance[ 'default_button_icon' ] );
      $instance[ 'button_text' ] = strip_tags( $new_instance[ 'button_text' ] );
      $instance[ 'button_url' ] = esc_url_raw( $new_instance[ 'button_url' ] );
      $instance[ 'button_icon' ] = strip_tags( $new_instance[ 'button_icon' ] );

      return $instance;
   }

   function widget( $args, $instance ) {
      extract( $args );
      extract( $instance );

      global $post;
      $about_menu_id = isset( $instance[ 'about_menu_id' ] ) ? $instance[ 'about_menu_id' ] : '';
      $title = apply_filters( 'widget_title', isset( $instance[ 'title' ] ) ? $instance[ 'title' ] : '');
      $text = apply_filters( 'widget_text', empty( $instance[ 'text' ] ) ? '' : $instance['text'], $instance );
      $page_id = isset( $instance[ 'page_id' ] ) ? $instance[ 'page_id' ] : '';
      $default_button_text = isset( $instance[ 'default_button_text' ] ) ? $instance[ 'default_button_text' ] : '';
      $default_button_url = empty( $instance[ 'default_button_url' ] ) ? '#' : $instance[ 'default_button_url' ];
      $default_button_icon = isset( $instance[ 'default_button_icon' ] ) ? $instance[ 'default_button_icon' ] : '';
      $button_text = isset( $instance[ 'button_text' ] ) ? $instance[ 'button_text' ] : '';
      $button_url = empty( $instance[ 'button_url' ] ) ? '#' : $instance[ 'button_url' ];
      $button_icon = isset( $instance[ 'button_icon' ] ) ? $instance[ 'button_icon' ] : '';

      // For Multilingual compatibility
      if ( function_exists( 'icl_register_string' ) ) {
         icl_register_string( 'Himalayas Pro', 'TG: About widget button one' . $this->id, $default_button_text );
         icl_register_string( 'Himalayas Pro', 'TG: About widget url one' . $this->id, $default_button_url );
         icl_register_string( 'Himalayas Pro', 'TG: About widget button two' . $this->id, $button_text );
         icl_register_string( 'Himalayas Pro', 'TG: About widget url two' . $this->id, $button_url );
      }
      if ( function_exists( 'icl_t' ) ) {
         $default_button_text = icl_t( 'Himalayas Pro', 'TG: About widget button one'. $this->id, $default_button_text );
         $default_button_url = icl_t( 'Himalayas Pro', 'TG: About widget url one'. $this->id, $default_button_url );
         $button_text = icl_t( 'Himalayas Pro', 'TG: About widget button two'. $this->id, $button_text );
         $button_url = icl_t( 'Himalayas Pro', 'TG: About widget url two'. $this->id, $button_url );
      }

      $section_id = '';
      if( !empty( $about_menu_id ) )
         $section_id = 'id="' . $about_menu_id . '"';

      echo $before_widget; ?>
      <div <?php echo $section_id; ?> >
         <div class="section-wrapper">
            <div class="tg-container">

               <div class="section-title-wrapper">
                  <?php if( !empty( $title ) ) echo $before_title . esc_html( $title ) . $after_title;
                  if( !empty( $text ) ) { ?>
                     <h4 class="sub-title"><?php echo esc_textarea( $text ); ?></h4>
                  <?php } ?>
               </div>

               <?php if( $page_id ) : ?>
               <div class="about-content-wrapper tg-column-wrapper clearfix">
                  <?php
                  $the_query = new WP_Query( 'page_id='.$page_id );
                  while( $the_query->have_posts() ):$the_query->the_post();
                     $title_attribute = the_title_attribute( 'echo=0' );

                     if( has_post_thumbnail() ) { ?>
                        <div class="about-image wow fadeInUp tg-column-2" data-wow-delay="0.8s">
                           <?php the_post_thumbnail( 'full' ); ?>
                        </div>
                     <?php } ?>

                     <div class="about-content tg-column-2">
                        <?php
                        $output = '<h2 class="about-title"> <a href="' . get_permalink() . '" title="' . $title_attribute . '" alt ="' . $title_attribute . '">' . get_the_title() . '</a></h2>';
                        $output .= '<div class="about-content">' . '<p>' . get_the_excerpt() . '</p></div>';

                        if( !empty( $default_button_text ) ) {
                           $output .= '<div class="about-btn"> <a href="' . $default_button_url . '">' . esc_html( $default_button_text ) . '<i class="fa ' . $default_button_icon . '"></i></a>';
                        }
                        else {
                           $output .= '<div class="about-btn"> <a href="'. get_permalink() . '">' . get_theme_mod( 'himalayas_read_more', __( 'Read more', 'himalayas' ) ) . '</a>';
                        }

                        if ( !empty ( $button_text ) ) {
                           $output .= '<a href="' . $button_url . '">' . esc_html( $button_text ) . '<i class="fa ' . $button_icon . '"></i></a>';
                        }
                        $output .= '</div>';
                        echo $output;
                        ?>
                     </div>
                  <?php endwhile;

                  // Reset Post Data
                  wp_reset_query(); ?>
               </div><!-- .about-content-wrapper -->
               <?php endif; ?>
            </div><!-- .tg-container -->
         </div>
      </div>
   <?php echo $after_widget;
   }
}

/**************************************************************************************/

/**
 * Service Widget section.
 */
class himalayas_service_widget extends WP_Widget {
   function __construct() {
      $widget_ops = array( 'classname' => 'widget_service_block', 'description' => __( 'Display some pages as services.', 'himalayas' ) );
      $control_ops = array( 'width' => 200, 'height' =>250 );
      parent::__construct( false, $name = __( 'TG: Service Widget', 'himalayas' ), $widget_ops, $control_ops);
   }

   function form( $instance ) {
      $defaults['service_menu_id'] = '';
      $defaults['title'] = '';
      $defaults['text'] = '';
      $defaults['number'] = '6';
      $defaults[ 'select' ] = 'service-style-1';

      $instance = wp_parse_args( (array) $instance, $defaults );

      $service_menu_id = esc_attr( $instance[ 'service_menu_id' ] );
      $title = esc_attr( $instance['title'] );
      $text = esc_textarea( $instance['text'] );
      $number = absint( $instance[ 'number' ] );
      $select = $instance[ 'select' ]; ?>

      <p><?php _e( 'Note: Enter the Service Section ID and use same for Menu item. Only used for One Page Menu.', 'himalayas' ); ?></p>

      <p>
         <label for="<?php echo $this->get_field_id('service_menu_id'); ?>"><?php _e( 'Service Section ID:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id('service_menu_id'); ?>" name="<?php echo $this->get_field_name('service_menu_id'); ?>" type="text" value="<?php echo $service_menu_id; ?>" />
      </p>

      <p>
         <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
      </p>

      <?php _e( 'Description','himalayas' ); ?>
      <textarea class="widefat" rows="5" cols="20" id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name('text'); ?>"><?php echo $text; ?></textarea>

      <p>
         <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of pages to display:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" />
      </p>

      <p><?php _e( 'Note: Create the pages and select Services Template to display Services pages.', 'himalayas' ); ?></p>
      <?php _e( 'Choose the layout','himalayas' ); ?>
      <p>
         <select id="<?php echo $this->get_field_id('select'); ?>" name="<?php echo $this->get_field_name('select'); ?>">
            <option value="service-style-1" <?php if ( $select == 'service-style-1' ) echo 'selected="selected"'; ?> ><?php _e( 'Layout One', 'himalayas' );?></option>
            <option value="service-style-2" <?php if ( $select == 'service-style-2' ) echo 'selected="selected"';?> ><?php _e( 'Layout Two', 'himalayas' );?></option>
         </select>
      </p>
   <?php }

   function update( $new_instance, $old_instance ) {
      $instance = $old_instance;
      $instance[ 'service_menu_id' ] = strip_tags( $new_instance[ 'service_menu_id' ] );
      $instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );

      if ( current_user_can('unfiltered_html') )
         $instance[ 'text' ] =  $new_instance[ 'text' ];
      else
         $instance[ 'text' ] = stripslashes( wp_filter_post_kses( addslashes( $new_instance[ 'text' ] ) ) ); // wp_filter_post_kses() expects slashed

      $instance[ 'number' ] = absint( $new_instance[ 'number' ] );
      $instance[ 'select' ] = $new_instance[ 'select' ];

      return $instance;
   }

   function widget( $args, $instance ) {
      extract( $args );
      extract( $instance );

      global $post;
      $service_menu_id = isset( $instance[ 'service_menu_id' ] ) ? $instance[ 'service_menu_id' ] : '';
      $title = apply_filters( 'widget_title', isset( $instance[ 'title' ] ) ? $instance[ 'title' ] : '');
      $text = apply_filters( 'widget_text', empty( $instance['text'] ) ? '' : $instance['text'], $instance );
      $number = empty( $instance[ 'number' ] ) ? 6 : $instance[ 'number' ];
      $select = isset( $instance[ 'select' ] ) ? $instance[ 'select' ] : '';

      $page_array = array();
      $pages = get_pages();
      // get the pages associated with Services Template.
      foreach ( $pages as $page ) {
         $page_id = $page->ID;
         $template_name = get_post_meta( $page_id, '_wp_page_template', true );
         if( $template_name == 'page-templates/template-services.php' ) {
            array_push( $page_array, $page_id );
         }
      }

      $get_featured_pages = new WP_Query( array(
         'posts_per_page'        => $number,
         'post_type'             =>  array( 'page' ),
         'post__in'              => $page_array,
         'orderby'               => 'date'
      ) );

      $section_id = '';
      if( !empty( $service_menu_id ) )
         $section_id = 'id="' . $service_menu_id . '"';

      echo $before_widget; ?>
      <div <?php echo $section_id; ?> >
         <div  class="section-wrapper <?php echo $select; ?>">
            <div class="tg-container">

               <div class="section-title-wrapper">
                  <?php if( !empty( $title ) ) echo $before_title . esc_html( $title ) . $after_title;

                  if( !empty( $text ) ) { ?>
                     <h4 class="sub-title"><?php echo esc_textarea( $text ); ?></h4>
                  <?php } ?>
               </div>

               <?php
               if( !empty( $page_array ) ) {
                  $count = 0; ?>
                  <div class="service-content-wrapper clearfix">
                     <div class="tg-column-wrapper clearfix">

                        <?php while( $get_featured_pages->have_posts() ):$get_featured_pages->the_post();

                           if ( $count % 3 == 0 && $count > 1 ) { ?> <div class="clearfix"></div> <?php } ?>

                           <div class="tg-column-3 tg-column-bottom-margin wow fadeInDown" data-wow-delay="0.8s">
                              <?php
                              $himalayas_icon = get_post_meta( $post->ID, 'himalayas_font_icon', true );
                              $himalayas_icon = isset( $himalayas_icon ) ? esc_attr( $himalayas_icon ) : '';

                              $icon_image_class = '';
                              if( !empty ( $himalayas_icon ) ) {
                                 $icon_image_class = 'service_icon_class';
                                 $services_top = '<i class="fa ' . esc_html( $himalayas_icon ) . '"></i>';
                              }
                              if( has_post_thumbnail() ) {
                                 $icon_image_class = 'service_image_class';
                                 $services_top = get_the_post_thumbnail( $post->ID, 'himalayas-services' );
                              }

                              if( has_post_thumbnail() || !empty ( $himalayas_icon ) ) { ?>

                                 <div class="<?php echo $icon_image_class; ?>">
                                    <div class="image-wrap">
                                       <?php echo $services_top; ?>
                                    </div>
                                 </div>
                              <?php } ?>

                              <div class="service-desc-wrap">
                                 <h5 class="service-title"><a title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>" alt="<?php the_title_attribute(); ?>"> <?php echo esc_html( get_the_title() ); ?></a></h5>

                                 <div class="service-content">
                                    <?php the_excerpt(); ?>
                                 </div>

                                 <a class="service-read-more" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"> <?php  echo get_theme_mod( 'himalayas_read_more', __( 'Read more', 'himalayas' ) ); ?><i class="fa fa-angle-double-right"> </i></a>
                              </div>
                           </div>
                           <?php $count++;
                        endwhile; ?>
                     </div>
                  </div>
                  <?php
                  // Reset Post Data
                  wp_reset_query();
               } ?>
            </div>
         </div>
      </div>
      <?php echo $after_widget;
   }
}

/**************************************************************************************/

/**
 * Call to action widget.
 */
class himalayas_call_to_action_widget extends WP_Widget {
   function __construct() {
      $widget_ops = array( 'classname' => 'widget_call_to_action_block', 'description' => __( 'Use this widget to show the call to action section.', 'himalayas' ) );
      $control_ops = array( 'width' => 200, 'height' =>250 );
      parent::__construct( false, $name = __( 'TG: Call To Action Widget', 'himalayas' ), $widget_ops, $control_ops);
   }

   function form( $instance ) {
      $defaults[ 'background_color' ] = '#32c4d1';
      $defaults[ 'background_image' ] = '';
      $defaults[ 'text_main' ] = '';
      $defaults[ 'text_additional' ] = '';
      $defaults[ 'button_text' ] = '';
      $defaults[ 'button_url' ] = '';
      $defaults[ 'select' ] = 'cta-text-style-1';

      $instance = wp_parse_args( (array) $instance, $defaults );

      $background_color = esc_attr( $instance[ 'background_color' ] );
      $background_image = esc_url_raw( $instance[ 'background_image' ] );
      $text_additional = esc_textarea( $instance[ 'text_additional' ] );
      $text_main = esc_textarea( $instance[ 'text_main' ] );
      $button_text = esc_attr( $instance[ 'button_text' ] );
      $button_url = esc_url( $instance[ 'button_url' ] );
      $select = $instance[ 'select' ];
      ?>
      <p>
         <strong><?php _e( 'DESIGN SETTINGS :', 'himalayas' ); ?></strong><br />
         <label for="<?php echo $this->get_field_id( 'background_color' ); ?>"><?php _e( 'Background Color:', 'himalayas' ); ?></label><br />
         <input class="my-color-picker" type="text" data-default-color="#32c4d1" id="<?php echo $this->get_field_id( 'background_color' ); ?>" name="<?php echo $this->get_field_name( 'background_color' ); ?>" value="<?php echo  $background_color; ?>" />
      </p>
      <p>
         <label for="<?php echo $this->get_field_id( 'background_image' ); ?>"> <?php _e( 'Image:', 'himalayas' ); ?> </label> <br />

         <?php
         if ( $instance[ 'background_image' ]  != '' ) :
            echo '<img id="' . $this->get_field_id( 'background_image' . 'preview') . '"src="' . $instance[ 'background_image' ] . '"style="max-width: 250px;" /><br />';
         endif;
         ?>
         <input type="text" class="widefat custom_media_url" id="<?php echo $this->get_field_id( 'background_image' ); ?>" name="<?php echo $this->get_field_name( 'background_image' ); ?>" value="<?php echo $instance[ 'background_image' ]; ?>" style="margin-top: 5px;"/>

         <input type="button" class="button button-primary custom_media_button" id="custom_media_button_action" name="<?php echo $this->get_field_name( 'background_image' ); ?>" value="<?php _e( 'Upload Image', 'himalayas' ) ?>" style="margin-top: 5px; margin-right: 30px;" onclick="imageWidget.uploader( '<?php echo $this->get_field_id( 'background_image' ); ?>' ); return false;"/>
      </p>

      <strong><?php _e( 'OTHER SETTINGS :', 'himalayas' ); ?></strong><br />

      <?php _e( 'Call to Action Main Text','himalayas' ); ?>
      <textarea class="widefat" rows="3" cols="20" id="<?php echo $this->get_field_id('text_main'); ?>" name="<?php echo $this->get_field_name('text_main'); ?>"><?php echo $text_main; ?></textarea>
      <?php _e( 'Call to Action Additional Text','himalayas' ); ?>
      <textarea class="widefat" rows="4" cols="20" id="<?php echo $this->get_field_id('text_additional'); ?>" name="<?php echo $this->get_field_name('text_additional'); ?>"><?php echo $text_additional; ?></textarea>
      <p>
         <label for="<?php echo $this->get_field_id('button_text'); ?>"><?php _e( 'Button Text:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id('button_text'); ?>" name="<?php echo $this->get_field_name('button_text'); ?>" type="text" value="<?php echo $button_text; ?>" />
      </p>
      <p>
         <label for="<?php echo $this->get_field_id('button_url'); ?>"><?php _e( 'Button Redirect Link:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id('button_url'); ?>" name="<?php echo $this->get_field_name('button_url'); ?>" type="text" value="<?php echo $button_url; ?>" />
      </p>
      <?php _e( 'Choose the layout','himalayas' ); ?>
      <p>
         <select id="<?php echo $this->get_field_id('select'); ?>" name="<?php echo $this->get_field_name('select'); ?>">
            <option value="cta-text-style-1" <?php if ( $select == 'cta-text-style-1' ) echo 'selected="selected"'; ?> ><?php _e( 'Layout One', 'himalayas' );?></option>
            <option value="cta-text-style-2" <?php if ( $select == 'cta-text-style-2' ) echo 'selected="selected"';?> ><?php _e( 'Layout Two', 'himalayas' );?></option>
            <option value="cta-text-style-3" <?php if ( $select == 'cta-text-style-3' ) echo 'selected="selected"';?> ><?php _e( 'Layout Three', 'himalayas' );?></option>
         </select>
      </p>
   <?php
   }

   function update( $new_instance, $old_instance ) {
      $instance = $old_instance;

      $instance['background_color'] =  $new_instance['background_color'];
      $instance['background_image'] =  esc_url_raw( $new_instance['background_image'] );

      if ( current_user_can('unfiltered_html') )
         $instance['text_main'] =  $new_instance['text_main'];
      else
         $instance['text_main'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['text_main']) ) ); // wp_filter_post_kses() expects slashed

      if ( current_user_can('unfiltered_html') )
         $instance['text_additional'] =  $new_instance['text_additional'];
      else
         $instance['text_additional'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['text_additional']) ) ); // wp_filter_post_kses() expects slashed

      $instance[ 'button_text' ] = strip_tags( $new_instance[ 'button_text' ] );
      $instance[ 'button_url' ] = esc_url_raw( $new_instance[ 'button_url' ] );
      $instance[ 'select' ] = $new_instance[ 'select' ];

      return $instance;
   }

   function widget( $args, $instance ) {
      extract( $args );
      extract( $instance );

      global $post;
      $background_color = isset( $instance[ 'background_color' ] ) ? $instance[ 'background_color' ] : '';
      $background_image = isset( $instance[ 'background_image' ] ) ? $instance[ 'background_image' ] : '';
      $text_main = empty( $instance[ 'text_main' ] ) ? '' : $instance[ 'text_main' ];
      $text_additional = empty( $instance['text_additional'] ) ? '' : $instance['text_additional'];
      $button_text = isset( $instance[ 'button_text' ] ) ? $instance[ 'button_text' ] : '';
      $button_url = empty( $instance[ 'button_url' ] ) ? '#' : $instance[ 'button_url' ];
      $select = isset( $instance[ 'select' ] ) ? $instance[ 'select' ] : '';

      // For Multilingual compatibility
      if ( function_exists( 'icl_register_string' ) ) {
         icl_register_string( 'Himalayas Pro', 'TG: Call to action widget image' . $this->id, $background_image );
         icl_register_string( 'Himalayas Pro', 'TG: Call to action widget main text' . $this->id, $text_main );
         icl_register_string( 'Himalayas Pro', 'TG: Call to action widget additional text' . $this->id, $text_additional );
         icl_register_string( 'Himalayas Pro', 'TG: Call to action widget button text' . $this->id, $button_text );
         icl_register_string( 'Himalayas Pro', 'TG: Call to action widget button url' . $this->id, $button_url );
      }
      if ( function_exists( 'icl_t' ) ) {
         $background_image = icl_t( 'Himalayas Pro', 'TG: Call to action widget image'. $this->id, $background_image );
         $text_main = icl_t( 'Himalayas Pro', 'TG: Call to action widget main text'. $this->id, $text_main );
         $text_additional = icl_t( 'Himalayas Pro', 'TG: Call to action widget additional text'. $this->id, $text_additional );
         $button_text = icl_t( 'Himalayas Pro', 'TG: Call to action widget button text'. $this->id, $button_text );
         $button_url = icl_t( 'Himalayas Pro', 'TG: Call to action widget button url'. $this->id, $button_url );
      }

      echo $before_widget;
      $bg_image_style = '';
      if ( !empty( $background_image ) ) {
         $bg_image_style .= 'background-image:url(' . $background_image . ');background-repeat:no-repeat;background-size:cover;background-attachment:fixed;';
         $bg_image_class = 'parallax-section';
      }else {
         $bg_image_style .= 'background-color:' . $background_color . ';';
         $bg_image_class = 'no-bg-image';
      }?>
      <div class="<?php echo $bg_image_class . ' ' . $select; ?> clearfix" style="<?php echo $bg_image_style; ?>">
         <div class="parallax-overlay"></div>
         <div class="section-wrapper cta-text-section-wrapper">
            <div class="tg-container">

               <div class="cta-text-content">
                  <?php if( !empty( $text_main ) ) { ?>
                     <div class="cta-text-title">
                        <h2><?php echo esc_html( $text_main ); ?></h2>
                     </div>
                  <?php }

                  if( !empty( $text_additional ) ) { ?>
                     <div class="cta-text-desc">
                        <p><?php echo esc_html( $text_additional ); ?></p>
                     </div>
                  <?php } ?>
               </div>
               <?php if( !empty( $button_text ) ) { ?>
                  <a class="cta-text-btn wow pulse" data-wow-delay="0.6s" href="<?php echo $button_url; ?>" title="<?php echo esc_attr( $button_text ); ?>"><?php echo esc_html( $button_text ); ?></a>
               <?php } ?>
            </div>
         </div>
      </div>
      <?php echo $after_widget;
   }
}

/**************************************************************************************/

/**
 * Portfolio widget section
 */
class himalayas_portfolio_widget extends WP_Widget {

   function __construct() {
      $widget_ops = array( 'classname' => 'widget_portfolio_block', 'description' => __( 'Display portfolio by using specific category', 'himalayas') );
      $control_ops = array( 'width' => 200, 'height' =>250 );
      parent::__construct( false,$name= __( 'TG: Portfolio', 'himalayas' ), $widget_ops);
   }

   function form( $instance ) {
      $defaults[ 'portfolio_menu_id' ] = '';
      $defaults[ 'title' ] = '';
      $defaults[ 'text' ] = '';
      $defaults[ 'number' ] = 8;

      $instance = wp_parse_args( (array) $instance, $defaults );

      $portfolio_menu_id = esc_attr( $instance[ 'portfolio_menu_id' ] );
      $title = esc_attr( $instance[ 'title' ] );
      $text = esc_textarea( $instance[ 'text' ] );
      $number = absint( $instance[ 'number' ] ); ?>

      <p><?php _e( 'Note: Enter the Portfolio Section ID and use same for Menu item. Only used for One Page Menu.', 'himalayas' ); ?></p>

      <p>
         <label for="<?php echo $this->get_field_id( 'portfolio_menu_id' ); ?>"><?php _e( 'Portfolio Section ID:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'portfolio_menu_id' ); ?>" name="<?php echo $this->get_field_name( 'portfolio_menu_id' ); ?>" type="text" value="<?php echo $portfolio_menu_id; ?>" />
      </p>

      <p>
         <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
      </p>

      <?php _e( 'Description','himalayas' ); ?>
      <textarea class="widefat" rows="5" cols="20" id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name('text'); ?>"><?php echo $text; ?></textarea>

      <p>
         <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to display:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" />
      </p>

      <p><?php _e( 'Note: Create the pages and select Portfolio Template to display Portfolio pages.', 'himalayas' ); ?></p>
      <?php
   }

   function update( $new_instance, $old_instance ) {
      $instance = $old_instance;

      $instance[ 'portfolio_menu_id' ] = strip_tags( $new_instance[ 'portfolio_menu_id' ] );
      $instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );
      if ( current_user_can('unfiltered_html') )
         $instance[ 'text' ] =  $new_instance[ 'text' ];
      else
         $instance[ 'text' ] = stripslashes( wp_filter_post_kses( addslashes($new_instance[ 'text' ]) ) ); // wp_filter_post_kses() expects slashed
      $instance[ 'number' ] = absint( $new_instance[ 'number' ] );

      return $instance;
   }

   function widget( $args, $instance ) {
      extract( $args );
      extract( $instance );
      global $post;

      $portfolio_menu_id = isset( $instance[ 'portfolio_menu_id' ] ) ? $instance[ 'portfolio_menu_id' ] : '';
      $title = apply_filters( 'widget_title', isset( $instance[ 'title' ] ) ? $instance[ 'title' ] : '');
      $text = apply_filters( 'widget_text', empty( $instance['text'] ) ? '' : $instance['text'], $instance );
      $number = empty( $instance[ 'number' ] ) ? 8 : $instance[ 'number' ];

      $page_array = array();
      $pages = get_pages();
      // get the pages associated with Portfolio Template.
      foreach ( $pages as $page ) {
         $page_id = $page->ID;
         $template_name = get_post_meta( $page_id, '_wp_page_template', true );
         if( $template_name == 'page-templates/template-portfolio.php' ) {
            array_push( $page_array, $page_id );
         }
      }
      $get_featured_posts = new WP_Query( array(
         'posts_per_page'        => $number,
         'post_type'             =>  array( 'page' ),
         'post__in'              => $page_array,
         'orderby'               => 'date'
      ) );

      echo $before_widget;

      $section_id = '';
      if( !empty( $portfolio_menu_id ) )
         $section_id = 'id="' . $portfolio_menu_id . '"'; ?>

      <div <?php echo $section_id; ?> class="" >
         <div class="section-wrapper">
            <div class="tg-container">
               <div class="section-title-wrapper">
                  <?php
                  if( !empty( $title ) ) { echo $before_title . esc_html( $title ) . $after_title; }
                  if( !empty( $text ) ) { ?> <h4 class="sub-title"> <?php echo esc_textarea( $text ); ?> </h4> <?php } ?>
               </div>
            </div>

            <?php if( !empty( $page_array ) ) : ?>
               <div class="Portfolio-content-wrapper clearfix">
                  <?php
                  $i = 0.22;
                  while( $get_featured_posts->have_posts() ):$get_featured_posts->the_post(); ?>

                     <div class="portfolio-images-wrapper wow fadeInDown" data-wow-delay="<?php echo $i; ?>s">
                        <?php
                        // Get the full URI of featured image
                        $image_popup_id = get_post_thumbnail_id();
                        $image_popup_url = wp_get_attachment_url( $image_popup_id ); ?>

                        <div class="port-img">
                           <?php if( has_post_thumbnail() ) {
                              the_post_thumbnail('himalayas-portfolio-image');

                           } else { $image_popup_url = get_template_directory_uri() . '/images/placeholder-portfolio.jpg';
                              echo '<img src="' . $image_popup_url . '">';
                           } ?>
                        </div>

                        <div class="portfolio-hover">
                           <div class="port-link">
                              <a class="image-popup" href="<?php echo $image_popup_url; ?>" ><i class="fa fa-search-plus"></i></a>
                              <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute();?>"><i class="fa fa-link"></i></a>
                           </div>

                           <div class="port-title-wrapper">
                              <h4 class="port-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute();?>"><?php the_title(); ?></a></h4>
                              <div class="port-desc"> <?php echo himalayas_excerpt( 16 ); ?> </div>
                           </div>
                        </div>
                     </div>
                  <?php $i = $i + 0.22;
                  endwhile; ?>
               </div><!-- .Portfolio-content-wrapper -->
               <?php
               // Reset Post Data
               wp_reset_query();
            endif; ?>
         </div>
      </div><!-- .section-wrapper -->

      <?php echo $after_widget;
   }
}

/**************************************************************************************/

/**
 * Featured Posts widget
 */
class himalayas_featured_posts_widget extends WP_Widget {

   function __construct() {
      $widget_ops = array( 'classname' => 'widget_featured_posts_block', 'description' => __( 'Display latest posts or posts of specific category', 'himalayas') );
      $control_ops = array( 'width' => 200, 'height' =>250 );
      parent::__construct( false,$name= __( 'TG: Featured Posts', 'himalayas' ),$widget_ops);
   }

   function form( $instance ) {
      $defaults[ 'featured_menu_id' ] = '';
      $defaults['background_color'] = '#f1f1f1';
      $defaults['background_image' ] = '';
      $defaults[ 'title' ] = '';
      $defaults[ 'text' ] = '';
      $defaults[ 'number' ] = 3;
      $defaults[ 'type' ] = 'latest';
      $defaults[ 'category' ] = '';
      $defaults['button_text'] = '';
      $defaults['button_url' ] = '';

      $instance = wp_parse_args( (array) $instance, $defaults );

      $featured_menu_id = esc_attr( $instance[ 'featured_menu_id' ] );
      $background_color = esc_attr( $instance[ 'background_color' ] );
      $background_image = esc_url_raw( $instance[ 'background_image' ] );
      $title = esc_attr( $instance[ 'title' ] );
      $text = esc_textarea( $instance[ 'text' ] );
      $number = absint( $instance[ 'number' ] );
      $type = $instance[ 'type' ];
      $category = $instance[ 'category' ];
      $button_text = esc_attr( $instance[ 'button_text' ] );
      $button_url = esc_url( $instance[ 'button_url' ] ); ?>

      <p><?php _e( 'Note: Enter the Featured Post Section ID and use same for Menu item. Only used for One Page Menu.', 'himalayas' ); ?></p>
       <p>
         <label for="<?php echo $this->get_field_id( 'featured_menu_id' ); ?>"><?php _e( 'Featured Post Section ID:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'featured_menu_id' ); ?>" name="<?php echo $this->get_field_name( 'featured_menu_id' ); ?>" type="text" value="<?php echo $featured_menu_id; ?>" />
      </p>

      <p>
         <strong><?php _e( 'DESIGN SETTINGS :', 'himalayas' ); ?></strong><br />
         <label for="<?php echo $this->get_field_id( 'background_color' ); ?>"><?php _e( 'Background Color:', 'himalayas' ); ?></label><br />
         <input class="my-color-picker" type="text" data-default-color="#f1f1f1" id="<?php echo $this->get_field_id( 'background_color' ); ?>" name="<?php echo $this->get_field_name( 'background_color' ); ?>" value="<?php echo  $background_color; ?>" />
      </p>

      <p>
         <label for="<?php echo $this->get_field_id( 'background_image' ); ?>"> <?php _e( 'Image:', 'himalayas' ); ?> </label> <br />

         <?php
         if ( $background_image  != '' ) :
            echo '<img id="' . $this->get_field_id( 'background_image' . 'preview' ) . '"src="' . $background_image . '"style="max-width: 250px;" /><br />';
         endif;
         ?>
         <input type="text" class="widefat custom_media_url" id="<?php echo $this->get_field_id( 'background_image' ); ?>" name="<?php echo $this->get_field_name( 'background_image' ); ?>" value="<?php echo $background_image; ?>" style="margin-top: 5px;"/>

         <input type="button" class="button button-primary custom_media_button" id="custom_media_button_portfolio" name="<?php echo $this->get_field_name( 'background_image' ); ?>" value="<?php _e( 'Upload Image', 'himalayas' ) ?>" style="margin-top: 5px; margin-right: 30px;" onclick="imageWidget.uploader( '<?php echo $this->get_field_id( 'background_image' ); ?>' ); return false;"/>
      </p>

      <strong><?php _e( 'OTHER SETTINGS :', 'himalayas' ); ?></strong><br />

      <p>
         <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
      </p>
      <?php _e( 'Description','himalayas' ); ?>
      <textarea class="widefat" rows="6" cols="20" id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>"><?php echo $text; ?></textarea>

      <p>
         <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to display:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" />
      </p>

      <p>
         <input type="radio" <?php checked( $type, 'latest' ) ?> id="<?php echo $this->get_field_id( 'type' ); ?>" name="<?php echo $this->get_field_name( 'type' ); ?>" value="latest"/><?php _e( 'Show latest Posts', 'himalayas' );?><br />
         <input type="radio" <?php checked( $type,'category' ) ?> id="<?php echo $this->get_field_id( 'type' ); ?>" name="<?php echo $this->get_field_name( 'type' ); ?>" value="category"/><?php _e( 'Show posts from a category', 'himalayas' );?><br />
      </p>

      <p>
         <label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Select category', 'himalayas' ); ?>:</label>
         <?php wp_dropdown_categories( array( 'show_option_none' =>' ','name' => $this->get_field_name( 'category' ), 'selected' => $category ) ); ?>
      </p>

      <p>
         <label for="<?php echo $this->get_field_id( 'button_text' ); ?>"><?php _e( 'Button Text:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'button_text' ); ?>" name="<?php echo $this->get_field_name( 'button_text' ); ?>" type="text" value="<?php echo $button_text; ?>" />
      </p>
      <p>
         <label for="<?php echo $this->get_field_id( 'button_url' ); ?>"><?php _e( 'Button Redirect Link:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'button_url' ); ?>" name="<?php echo $this->get_field_name( 'button_url' ); ?>" type="text" value="<?php echo $button_url; ?>" />
      </p>
      <?php
   }

   function update( $new_instance, $old_instance ) {
      $instance = $old_instance;

      $instance[ 'featured_menu_id' ] = strip_tags( $new_instance[ 'featured_menu_id' ] );
      $instance[ 'background_color' ] = $new_instance[ 'background_color' ];
      $instance[ 'background_image' ] = esc_url_raw( $new_instance[ 'background_image' ] );
      $instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );
      if ( current_user_can( 'unfiltered_html' ) )
         $instance[ 'text' ] =  $new_instance[ 'text' ];
      else
         $instance['text'] = stripslashes( wp_filter_post_kses( addslashes($new_instance[ 'text' ]) ) ); // wp_filter_post_kses() expects slashed
      $instance[ 'number' ] = absint( $new_instance[ 'number' ] );
      $instance[ 'type' ] = $new_instance[ 'type' ];
      $instance[ 'category' ] = $new_instance[ 'category' ];
      $instance[ 'button_text' ] = strip_tags( $new_instance[ 'button_text' ] );
      $instance[ 'button_url' ] = esc_url_raw( $new_instance[ 'button_url' ] );

      return $instance;
   }

   function widget( $args, $instance ) {
      extract( $args );
      extract( $instance );

      global $post;
      $featured_menu_id = isset( $instance[ 'featured_menu_id' ] ) ? $instance[ 'featured_menu_id' ] : '';
      $background_color = isset( $instance[ 'background_color' ] ) ? $instance[ 'background_color' ] : '';
      $background_image = isset( $instance[ 'background_image' ] ) ? $instance[ 'background_image' ] : '';
      $title = apply_filters( 'widget_title', isset( $instance[ 'title' ] ) ? $instance[ 'title' ] : '');
      $text = apply_filters( 'widget_text', empty( $instance['text'] ) ? '' : $instance['text'], $instance );
      $number = empty( $instance[ 'number' ] ) ? 3 : $instance[ 'number' ];
      $type = isset( $instance[ 'type' ] ) ? $instance[ 'type' ] : 'latest' ;
      $category = isset( $instance[ 'category' ] ) ? $instance[ 'category' ] : '';
      $button_text = isset( $instance[ 'button_text' ] ) ? $instance[ 'button_text' ] : '';
      $button_url = empty( $instance[ 'button_url' ] ) ? '#' : $instance[ 'button_url' ];

      // For Multilingual compatibility
      if ( function_exists( 'icl_register_string' ) ) {
         icl_register_string( 'Himalayas Pro', 'TG: Featured Posts widget image' . $this->id, $background_image );
         icl_register_string( 'Himalayas Pro', 'TG: Featured Posts widget button text' . $this->id, $button_text );
         icl_register_string( 'Himalayas Pro', 'TG: Featured Posts widget button url' . $this->id, $button_url );
      }
      if ( function_exists( 'icl_t' ) ) {
         $background_image = icl_t( 'Himalayas Pro', 'TG: Featured Posts widget image'. $this->id, $background_image );
         $button_text = icl_t( 'Himalayas Pro', 'TG: Featured Posts widget button text'. $this->id, $button_text );
         $button_url = icl_t( 'Himalayas Pro', 'TG: Featured Posts widget button url'. $this->id, $button_url );
      }

      if( $type == 'latest' ) {
         $get_featured_posts = new WP_Query( array(
            'posts_per_page'        => $number,
            'post_type'             => 'post',
            'ignore_sticky_posts'   => true
         ) );
      }
      else {
         $get_featured_posts = new WP_Query( array(
            'posts_per_page'        => $number,
            'post_type'             => 'post',
            'category__in'          => $category
         ) );
      }

      if ( !empty( $background_image ) ) {
         $bg_image_style = 'background-image:url(' . $background_image . ');background-repeat:no-repeat;background-size:cover;background-attachment:fixed;';
         $bg_image_class = 'parallax-section';
      }else {
         $bg_image_style = 'background-color:' . $background_color . ';';
         $bg_image_class = 'no-bg-image';
      }

      $section_id = '';
      if( !empty( $featured_menu_id ) )
         $section_id = 'id="' . $featured_menu_id . '"';

      echo $before_widget; ?>

      <div <?php echo $section_id; ?> class="<?php echo $bg_image_class ?>" style="<?php echo $bg_image_style; ?>">
         <div class="parallax-overlay"> </div>
         <div class="section-wrapper">
            <div class="tg-container">

               <div class="section-title-wrapper">
                  <?php if ( !empty( $title ) ) { echo $before_title . esc_html( $title ) . $after_title; } ?>
                  <?php if ( !empty( $text ) ) { ?>
                     <h4 class="sub-title">
                        <?php echo esc_textarea( $text ); ?>
                     </h4>
                  <?php } ?>
               </div>

               <div class="blog-content-wrapper clearfix">
               <div class="tg-column-wrapper clearfix">

                  <?php
                  $i = 1;
                  while( $get_featured_posts->have_posts() ):$get_featured_posts->the_post();
                     if( $i % 3 == 1 ) {
                        $himalayas_animate = 'fadeInLeft';
                        if ( $i > 1 ) { ?> <div class="clearfix"></div> <?php }
                     }
                     elseif( $i % 3 == 2 ) { $himalayas_animate = 'fadeInDown'; }
                     else { $himalayas_animate = 'fadeInRight'; } ?>

                     <div class="tg-column-3 tg-column-bottom-margin wow <?php echo $himalayas_animate; ?>" data-wow-delay="0.8s">
                     <div class="blog-block">

                        <?php if( has_post_thumbnail() ) { ?>
                           <div class="blog-img">
                              <?php the_post_thumbnail('himalayas-featured-image'); ?>
                           </div>
                         <?php } ?>

                        <div class="blog-content-wrapper">

                           <h5 class="blog-title"> <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute();?>"><?php the_title(); ?></a></h5>

                           <div class="posted-date">

                              <?php if( has_category() ) { ?>
                                 <span>
                                    <?php _e( 'Posted in ', 'himalayas' );
                                    the_category(', '); ?>
                                 </span>
                              <?php } ?>
                              <span>
                                 <?php _e( 'by ', 'himalayas' ); ?>
                                 <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" title="<?php echo get_the_author(); ?>"><?php echo esc_html( get_the_author() ); ?></a>
                              </span>

                              <span>
                                 <?php _e( 'on ', 'himalayas' );

                                 $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';

                                 $time_string = sprintf( $time_string,
                                    esc_attr( get_the_date( 'c' ) ),
                                    esc_html( get_the_date() )
                                 );
                                 printf( __( '<span><a href="%1$s" title="%2$s" rel="bookmark"></i> %3$s</a></span>', 'himalayas' ),
                                    esc_url( get_permalink() ),
                                    esc_attr( get_the_time() ),
                                    $time_string
                                 ); ?>
                              </span>
                           </div>

                           <div class="blog-content">
                              <?php the_excerpt(); ?>
                           </div>

                           <a class="blog-readmore" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"> <?php echo get_theme_mod( 'himalayas_read_more', __( 'Read more', 'himalayas' ) ); ?> <i class="fa fa-angle-double-right"> </i> </a>
                        </div>

                     </div><!-- .blog-block -->
                     </div><!-- .tg-column-3 -->

                  <?php $i++;
                  endwhile;

                  if( !empty( $button_text ) ) { ?>
                     <div class="clearfix"></div>

                     <a class="blog-view" href="<?php echo $button_url; ?>" title="<?php echo esc_attr( $button_text ); ?>"><?php echo esc_html( $button_text ); ?></a>
                  <?php } ?>

               </div><!-- .tg-column-wrapper -->
               </div><!-- .blog-content-wrapper -->

            </div><!-- .tg-container -->
         </div><!-- .section-wrapper -->
      </div>

      <?php
      // Reset Post Data
      wp_reset_query();
      echo $after_widget;
   }
}

/**************************************************************************************/

/**
 * Our Team section.
 */
class himalayas_our_team_widget extends WP_Widget {
   function __construct() {
      $widget_ops = array( 'classname' => 'widget_our_team_block', 'description' => __( 'Show your Team Members.', 'himalayas' ) );
      $control_ops = array( 'width' => 200, 'height' =>250 );
      parent::__construct( false, $name = __( 'TG: Our Team Widget', 'himalayas' ), $widget_ops, $control_ops);
   }

   function form( $instance ) {
      $defaults['team_menu_id'] = '';
      $defaults[ 'background_color' ] = '#575757';
      $defaults[ 'background_image' ] = '';
      $defaults['title'] = '';
      $defaults['text'] = '';
      $defaults['number'] = '3';

      $instance = wp_parse_args( (array) $instance, $defaults );

      $team_menu_id = esc_attr( $instance[ 'team_menu_id' ] );
      $background_color = esc_attr( $instance[ 'background_color' ] );
      $background_image = esc_url_raw( $instance[ 'background_image' ] );
      $title = esc_attr( $instance[ 'title' ] );
      $text = esc_textarea( $instance['text'] );
      $number = absint( $instance[ 'number' ] );
      ?>

      <p><?php _e( 'Note: Enter the Our Team Section ID and use same for Menu item. Only used for One Page Menu.', 'himalayas' ); ?></p>
      <p>
         <label for="<?php echo $this->get_field_id( 'team_menu_id' ); ?>"><?php _e( 'Our Team Section ID:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'team_menu_id' ); ?>" name="<?php echo $this->get_field_name( 'team_menu_id' ); ?>" type="text" value="<?php echo $team_menu_id; ?>" />
      </p>
      <p>
         <strong><?php _e( 'DESIGN SETTINGS :', 'himalayas' ); ?></strong><br />

         <label for="<?php echo $this->get_field_id( 'background_color' ); ?>"><?php _e( 'Background Color:', 'himalayas' ); ?></label><br />
         <input class="my-color-picker" type="text" data-default-color="#575757" id="<?php echo $this->get_field_id( 'background_color' ); ?>" name="<?php echo $this->get_field_name( 'background_color' ); ?>" value="<?php echo  $background_color; ?>" />
      </p>
      <p>
         <label for="<?php echo $this->get_field_id( 'background_image' ); ?>"> <?php _e( 'Background Image:', 'himalayas' ); ?> </label> <br />

         <?php
         if ( $instance[ 'background_image' ]  != '' ) :
            echo '<img id="' . $this->get_field_id( 'background_image' . 'preview') . '"src="' . $instance[ 'background_image' ] . '"style="max-width: 250px;" /><br />';
         endif;
         ?>
         <input type="text" class="widefat custom_media_url" id="<?php echo $this->get_field_id( 'background_image' ); ?>" name="<?php echo $this->get_field_name( 'background_image' ); ?>" value="<?php echo $instance[ 'background_image' ]; ?>" style="margin-top: 5px;"/>

         <input type="button" class="button button-primary custom_media_button" id="custom_media_button_action" name="<?php echo $this->get_field_name( 'background_image' ); ?>" value="<?php _e( 'Upload Image', 'himalayas' ) ?>" style="margin-top: 5px; margin-right: 30px;" onclick="imageWidget.uploader( '<?php echo $this->get_field_id( 'background_image' ); ?>' ); return false;"/>
      </p>

      <strong><?php _e( 'OTHER SETTINGS :', 'himalayas' ); ?></strong><br />

      <p>
         <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
      </p>
      <?php _e( 'Description','himalayas' ); ?>
      <textarea class="widefat" rows="5" cols="20" id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>"><?php echo $text; ?></textarea>

      <p>
         <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of pages to display:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" />
      </p>
      <p><?php _e( 'Note: Create the pages and select Our Team Template to display Our Team pages.', 'himalayas' ); ?></p>
   <?php }

   function update( $new_instance, $old_instance ) {
      $instance = $old_instance;
      $instance[ 'team_menu_id' ] = strip_tags( $new_instance[ 'team_menu_id' ] );
      $instance['background_color'] =  $new_instance['background_color'];
      $instance['background_image'] =  esc_url_raw( $new_instance['background_image'] );
      $instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );

      if ( current_user_can('unfiltered_html') )
         $instance[ 'text' ] =  $new_instance[ 'text' ];
      else
         $instance[ 'text' ] = stripslashes( wp_filter_post_kses( addslashes( $new_instance[ 'text' ] ) ) ); // wp_filter_post_kses() expects slashed

      $instance[ 'number' ] = absint( $new_instance[ 'number' ] );

      return $instance;
   }

   function widget( $args, $instance ) {
      extract( $args );
      extract( $instance );

      global $post;
      $team_menu_id = isset( $instance[ 'team_menu_id' ] ) ? $instance[ 'team_menu_id' ] : '';
      $background_color = isset( $instance[ 'background_color' ] ) ? $instance[ 'background_color' ] : '';
      $background_image = isset( $instance[ 'background_image' ] ) ? $instance[ 'background_image' ] : '';
      $title = apply_filters( 'widget_title', isset( $instance[ 'title' ] ) ? $instance[ 'title' ] : '');
      $text = apply_filters( 'widget_text', empty( $instance[ 'text' ] ) ? '' : $instance[ 'text' ], $instance );
      $number = empty( $instance[ 'number' ] ) ? 3 : $instance[ 'number' ];

      // For Multilingual compatibility
      if ( function_exists( 'icl_register_string' ) ) {
         icl_register_string( 'Himalayas Pro', 'TG: Our Team widget image' . $this->id, $background_image );
      }
      if ( function_exists( 'icl_t' ) ) {
         $background_image = icl_t( 'Himalayas Pro', 'TG: Our Team widget image'. $this->id, $background_image );
      }

      $page_array = array();
      $pages = get_pages();
      // get the pages associated with Our Team Template.
      foreach ( $pages as $page ) {
         $page_id = $page->ID;
         $template_name = get_post_meta( $page_id, '_wp_page_template', true );
         if( $template_name == 'page-templates/template-team.php' ) {
            array_push( $page_array, $page_id );
         }
      }

      $get_featured_pages = new WP_Query( array(
         'posts_per_page'        => $number,
         'post_type'             =>  array( 'page' ),
         'post__in'              => $page_array,
         'orderby'               => 'date'
      ) );

      if ( !empty( $background_image ) ) {
         $bg_image_style = 'background-image:url(' . $background_image . ');background-repeat:no-repeat;background-size:cover;background-attachment:fixed;';
         $bg_image_class = 'parallax-section';
      }else {
         $bg_image_style = 'background-color:' . $background_color . ';';
         $bg_image_class = 'no-bg-image';
      }

      $himalayas_social = array();
      $section_id = '';
      if( !empty( $team_menu_id ) )
         $section_id = 'id="' . $team_menu_id . '"';

      echo $before_widget; ?>
      <div <?php echo $section_id; ?> class="<?php echo $bg_image_class ?> clearfix" style="<?php echo $bg_image_style; ?>">

         <div class="parallax-overlay"></div>
         <div class="section-wrapper">
            <div class="tg-container">

               <div class="section-title-wrapper">
                  <?php if( !empty( $title ) ) echo $before_title . esc_html( $title ) . $after_title;

                  if( !empty( $text ) ) { ?>
                     <h4 class="sub-title"><?php echo esc_textarea( $text ); ?></h4>
                  <?php } ?>
               </div>

               <?php if( !empty ( $page_array ) ) : ?>
               <div class="team-content-wrapper clearfix">
                  <div class="tg-column-wrapper clearfix">
                     <?php while( $get_featured_pages->have_posts() ):$get_featured_pages->the_post();

                        $title_attribute = the_title_attribute( 'echo=0' ); ?>

                        <div class="tg-column-3 tg-column-bottom-margin wow fadeIn" data-wow-delay="0.8s">
                           <div class="team-block">

                              <div class="team-img-wrapper">

                                 <div class="team-img">
                                    <?php if( has_post_thumbnail() ) {
                                       the_post_thumbnail( 'himalayas-portfolio-image' );
                                    } else { echo '<img src="' . get_template_directory_uri() . '/images/placeholder-team.jpg' . '">';
                                    } ?>
                                 </div>

                                 <div class="team-name">
                                    <?php the_title(); ?>
                                 </div>
                              </div>

                              <div class="team-desc-wrapper">
                                 <?php
                                 $output = '';
                                 $himalayas_designation = get_post_meta( $post->ID, 'himalayas_designation', true );
                                 if( !empty( $himalayas_designation ) ) {
                                    $himalayas_designation = isset( $himalayas_designation ) ? esc_attr( $himalayas_designation ) : '';
                                    $output .= '<h5 class="team-deg">' . esc_html( $himalayas_designation ) . '</h5>';
                                 }

                                 $output .= '<div class="team-content">' . '<p>' . get_the_excerpt() . '</p></div>';

                                 $output .= '<div class="team-social-links">';
                                 for( $i = 1; $i <= 3; $i++ ) {
                                    $himalayas_social[$i] = get_post_meta( $post->ID, 'himalayas_team_social'.$i, true );
                                    if( !empty( $himalayas_social[$i] ) ) {
                                       $himalayas_social_link = isset( $himalayas_social[$i] ) ? esc_attr( $himalayas_social[$i] ) : '';
                                       $output .= '<a href="' . esc_html( $himalayas_social_link ) . '"></a>';
                                    }
                                 }
                                 $output .= '</div>';

                                 $output .= '<a class="team-name" href="' . get_permalink() . '" title="' . $title_attribute . '" alt ="' . $title_attribute . '">' . get_the_title() . '</a>';

                                 echo $output; ?>
                              </div>
                           </div><!-- .team-block -->
                        </div><!-- .tg-column-3 -->
                     <?php endwhile;

                     // Reset Post Data
                     wp_reset_query(); ?>
                  </div><!-- .tg-column-wrapper -->
               </div><!-- .team-content-wrapper -->

               <?php endif; ?>

            </div><!-- .tg-container -->
         </div><!-- .section-wrapper -->
      </div>

      <?php echo $after_widget;
   }
}

/**************************************************************************************/

/**
 * Contact us section.
 */
class himalayas_contact_widget extends WP_Widget {
   function __construct() {
      $widget_ops = array( 'classname' => 'widget_contact_block', 'description' => __( 'Show your Contact page.', 'himalayas' ) );
      $control_ops = array( 'width' => 200, 'height' =>250 );
      parent::__construct( false, $name = __( 'TG: Contact Us Widget', 'himalayas' ), $widget_ops, $control_ops);
   }

   function form( $instance ) {
      $defaults[ 'contact_menu_id' ] = '';
      $defaults[ 'title' ] = '';
      $defaults[ 'text' ] = '';
      $defaults[ 'page_id' ] = '';
      $defaults[ 'shortcode' ] = '';

      $instance = wp_parse_args( (array) $instance, $defaults );

      $contact_menu_id = esc_attr( $instance[ 'contact_menu_id' ] );
      $title = esc_attr( $instance[ 'title' ] );
      $text = esc_textarea( $instance['text'] );
      $page_id = absint( $instance[ 'page_id' ] );
      $shortcode = esc_attr( $instance[ 'shortcode' ] );
      ?>
      <p><?php _e( 'Note: Enter the Contact Section ID and use same for Menu item. Only used for One Page Menu.', 'himalayas' ); ?></p>

      <p>
         <label for="<?php echo $this->get_field_id( 'contact_menu_id' ); ?>"><?php _e( 'Contact Section ID:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'contact_menu_id' ); ?>" name="<?php echo $this->get_field_name( 'contact_menu_id' ); ?>" type="text" value="<?php echo $contact_menu_id; ?>" />
      </p>

      <p>
         <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
      </p>

      <?php _e( 'Description','himalayas' ); ?>
      <textarea class="widefat" rows="5" cols="20" id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>"><?php echo $text; ?></textarea>

      <p><?php _e( 'Select a Contact page.', 'himalayas' ) ?></p>
      <label for="<?php echo $this->get_field_id( 'page_id' ); ?>"><?php _e( 'Page', 'himalayas' ); ?>:</label>
      <?php wp_dropdown_pages( array( 'show_option_none' =>' ','name' => $this->get_field_name( 'page_id' ), 'selected' => $instance[ 'page_id' ] ) ); ?>

      <p><?php _e( 'Use Contact Form Plugin and enter the shortcode here:', 'himalayas' ) ?></p>
      <p>
         <label for="<?php echo $this->get_field_id( 'shortcode' ); ?>"><?php _e( 'Shortcode', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'shortcode' ); ?>" name="<?php echo $this->get_field_name( 'shortcode' ); ?>" type="text" value="<?php echo $shortcode; ?>" />
      </p>
   <?php }

   function update( $new_instance, $old_instance ) {
      $instance = $old_instance;
      $instance[ 'contact_menu_id' ] = strip_tags( $new_instance[ 'contact_menu_id' ] );
      $instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );

      if ( current_user_can('unfiltered_html') )
         $instance[ 'text' ] =  $new_instance[ 'text' ];
      else
         $instance[ 'text' ] = stripslashes( wp_filter_post_kses( addslashes( $new_instance[ 'text' ] ) ) ); // wp_filter_post_kses() expects slashed

      $instance[ 'page_id' ] = absint( $new_instance[ 'page_id' ] );
      $instance[ 'shortcode' ] = strip_tags( $new_instance[ 'shortcode' ] );

      return $instance;
   }

   function widget( $args, $instance ) {
      extract( $args );
      extract( $instance );

      global $post;
      $contact_menu_id = isset( $instance[ 'contact_menu_id' ] ) ? $instance[ 'contact_menu_id' ] : '';
      $title = apply_filters( 'widget_title', isset( $instance[ 'title' ] ) ? $instance[ 'title' ] : '');
      $text = apply_filters( 'widget_text', empty( $instance[ 'text' ] ) ? '' : $instance['text'], $instance );
      $page_id = isset( $instance[ 'page_id' ] ) ? $instance[ 'page_id' ] : '';
      $shortcode = isset( $instance[ 'shortcode' ] ) ? $instance[ 'shortcode' ] : '';

      $section_id = '';
      if( !empty( $contact_menu_id ) )
         $section_id = 'id="' . $contact_menu_id . '"';

      echo $before_widget; ?>
      <div <?php echo $section_id; ?> class="section-wrapper">
         <div class="tg-container">

            <div class="section-title-wrapper">
               <?php if( !empty( $title ) ) echo $before_title . esc_html( $title ) . $after_title;

               if( !empty( $text ) ) { ?>
                  <h4 class="sub-title"><?php echo esc_textarea( $text ); ?></h4>
               <?php } ?>
            </div>

            <div class="contact-form-wrapper tg-column-wrapper clearfix">
               <?php if( $page_id ) :
                  $the_query = new WP_Query( 'page_id='.$page_id );
                  while( $the_query->have_posts() ):$the_query->the_post(); ?>

                     <div class="tg-column-2">

                        <h2 class="contact-title"> <?php the_title(); ?> </h2>

                        <div class="contact-content"> <?php the_content(); ?> </div>
                     </div>
                  <?php endwhile;
               endif;
               // Reset Post Data
               wp_reset_query();
               if ( !empty ( $shortcode ) ) { ?>
                  <div class="tg-column-2">
                     <?php echo do_shortcode( $shortcode ); ?>
                  </div>
               <?php } ?>
            </div><!-- .contact-content-wrapper -->
         </div><!-- .tg-container -->
      </div>
   <?php echo $after_widget;
   }
}

/**************************************************************************************/

/**
 * Image Gallery widget.
 */
class himalayas_image_gallery_widget extends WP_Widget {
   function __construct() {
      $widget_ops = array( 'classname' => 'widget_image_gallery_block', 'description' => __( 'Use this widget to display the images.', 'himalayas' ) );
      $control_ops = array( 'width' => 200, 'height' =>250 );
      parent::__construct( false, $name = __( 'TG: Image Gallery Widget', 'himalayas' ), $widget_ops, $control_ops);
   }

   function form( $instance ) {
      $defaults[ 'title' ] = '';
      for( $i=1; $i<=8; $i++ ) {
         $var = 'image'.$i;
         $defaults[ $var ] = '';
      }

      $instance = wp_parse_args( (array) $instance, $defaults );

      $title = esc_attr( $instance[ 'title' ] );
      for( $i=1; $i<=8; $i++ ) {
         $var = 'image'.$i;
         $instance[ $var ] = esc_url_raw( $instance[ $var ] );
      }
      ?>
      <strong><?php _e( 'Upload your Images :', 'himalayas' ); ?></strong><br />
      <p>
         <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
      </p>
      <?php for( $i=1; $i<=8; $i++ ) {
         $var = 'image'.$i; ?>
         <p>
            <label for="<?php echo $this->get_field_id( $var ); ?>"> <?php echo $var; ?> </label> <br />

            <?php
            if ( $instance[ $var ]  != '' ) :
               echo '<img id="' . $this->get_field_id( $var . 'preview') . '"src="' . $instance[ $var ] . '"style="max-width: 250px;" /><br />';
            endif;
            ?>
            <input type="text" class="widefat custom_media_url" id="<?php echo $this->get_field_id( $var ); ?>" name="<?php echo $this->get_field_name( $var ); ?>" value="<?php echo $instance[ $var ]; ?>" style="margin-top: 5px;"/>

            <input type="button" class="button button-primary custom_media_button" id="custom_media_button_action" name="<?php echo $this->get_field_name( $var ); ?>" value="<?php _e( 'Upload Image', 'himalayas' ) ?>" style="margin-top: 5px; margin-right: 30px;" onclick="imageWidget.uploader( '<?php echo $this->get_field_id( 'image' .$i ); ?>' ); return false;"/>
         </p>
      <?php }
   }

   function update( $new_instance, $old_instance ) {
      $instance = $old_instance;

      $instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );
      for( $i=1; $i<=8; $i++ ) {
         $var = 'image'.$i;
         $instance[$var] =  esc_url_raw( $new_instance[$var] );
      }

      return $instance;
   }

   function widget( $args, $instance ) {
      extract( $args );
      extract( $instance );

      $title = apply_filters( 'widget_title', isset( $instance[ 'title' ] ) ? $instance[ 'title' ] : '');

      echo $before_widget;
      if( !empty( $title ) ) echo $before_title . esc_html( $title ) . $after_title; ?>

      <div class="gallery-image-wrapper">
         <ul class="gallery-wrap">
            <?php for( $i=1; $i<=8; $i++ ) {
               $var = 'image'.$i;
               $image = isset( $instance[ $var ] ) ? $instance[ $var ] : '';
               if( !empty($image) ) { ?>
                  <li>
                     <a href="<?php echo $image; ?>">
                        <img src="<?php echo $image; ?>">
                     </a>
                  </li>
               <?php }
            }?>
         </ul>
      </div>

      <?php echo $after_widget;
   }
}

/**************************************************************************************/

/**
 * Testimonial section.
 */
class himalayas_testimonial_widget extends WP_Widget {
   function __construct() {
      $widget_ops = array( 'classname' => 'widget_testimonial_block', 'description' => __( 'Show Testimonials', 'himalayas' ) );
      $control_ops = array( 'width' => 200, 'height' =>250 );
      parent::__construct( false, $name = __( 'TG: Testimonial Widget', 'himalayas' ), $widget_ops, $control_ops);
   }

   function form( $instance ) {
      $defaults['testimonial_menu_id'] = '';
      $defaults[ 'background_color' ] = '#575757';
      $defaults[ 'background_image' ] = '';
      $defaults['number'] = '4';

      $instance = wp_parse_args( (array) $instance, $defaults );

      $testimonial_menu_id = esc_attr( $instance[ 'testimonial_menu_id' ] );
      $background_color = esc_attr( $instance[ 'background_color' ] );
      $background_image = esc_url_raw( $instance[ 'background_image' ] );
      $number = absint( $instance[ 'number' ] );
      ?>

      <p><?php _e( 'Note: Enter the Testimonial Section ID and use same for Menu item. Only used for One Page Menu.', 'himalayas' ); ?></p>
      <p>
         <label for="<?php echo $this->get_field_id( 'testimonial_menu_id' ); ?>"><?php _e( 'Testimonial Section ID:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'testimonial_menu_id' ); ?>" name="<?php echo $this->get_field_name( 'testimonial_menu_id' ); ?>" type="text" value="<?php echo $testimonial_menu_id; ?>" />
      </p>
      <p>
         <label for="<?php echo $this->get_field_id( 'background_color' ); ?>"><?php _e( 'Background Color:', 'himalayas' ); ?></label><br />
         <input class="my-color-picker" type="text" data-default-color="#575757" id="<?php echo $this->get_field_id( 'background_color' ); ?>" name="<?php echo $this->get_field_name( 'background_color' ); ?>" value="<?php echo  $background_color; ?>" />
      </p>
      <p>
         <label for="<?php echo $this->get_field_id( 'background_image' ); ?>"> <?php _e( 'Background Image:', 'himalayas' ); ?> </label> <br />

         <?php
         if ( $instance[ 'background_image' ]  != '' ) :
            echo '<img id="' . $this->get_field_id( 'background_image' . 'preview') . '"src="' . $instance[ 'background_image' ] . '"style="max-width: 250px;" /><br />';
         endif;
         ?>
         <input type="text" class="widefat custom_media_url" id="<?php echo $this->get_field_id( 'background_image' ); ?>" name="<?php echo $this->get_field_name( 'background_image' ); ?>" value="<?php echo $instance[ 'background_image' ]; ?>" style="margin-top: 5px;"/>

         <input type="button" class="button button-primary custom_media_button" id="custom_media_button_action" name="<?php echo $this->get_field_name( 'background_image' ); ?>" value="<?php _e( 'Upload Image', 'himalayas' ) ?>" style="margin-top: 5px; margin-right: 30px;" onclick="imageWidget.uploader( '<?php echo $this->get_field_id( 'background_image' ); ?>' ); return false;"/>
      </p>

      <p>
         <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of testimonials to display:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" />
      </p>
      <p><?php _e( 'Note: Create the pages and select Testimonial Template to display Testimonial pages.', 'himalayas' ); ?></p>
   <?php }

   function update( $new_instance, $old_instance ) {
      $instance = $old_instance;
      $instance[ 'testimonial_menu_id' ] = strip_tags( $new_instance[ 'testimonial_menu_id' ] );
      $instance['background_color'] =  $new_instance['background_color'];
      $instance['background_image'] =  esc_url_raw( $new_instance['background_image'] );
      $instance[ 'number' ] = absint( $new_instance[ 'number' ] );

      return $instance;
   }

   function widget( $args, $instance ) {
      wp_enqueue_script( 'himalayas-bxslider' );
      extract( $args );
      extract( $instance );

      global $post;
      $testimonial_menu_id = isset( $instance[ 'testimonial_menu_id' ] ) ? $instance[ 'testimonial_menu_id' ] : '';
      $background_color = isset( $instance[ 'background_color' ] ) ? $instance[ 'background_color' ] : '';
      $background_image = isset( $instance[ 'background_image' ] ) ? $instance[ 'background_image' ] : '';
      $number = empty( $instance[ 'number' ] ) ? 4 : $instance[ 'number' ];

      // For Multilingual compatibility
      if ( function_exists( 'icl_register_string' ) ) {
         icl_register_string( 'Himalayas Pro', 'TG: Testimonial widget image' . $this->id, $background_image );
      }
      if ( function_exists( 'icl_t' ) ) {
         $background_image = icl_t( 'Himalayas Pro', 'TG: Testimonial widget image'. $this->id, $background_image );
      }

      $page_array = array();
      $pages = get_pages();
      // get the pages associated with Testimonial Template.
      foreach ( $pages as $page ) {
         $page_id = $page->ID;
         $template_name = get_post_meta( $page_id, '_wp_page_template', true );
         if( $template_name == 'page-templates/template-testimonial.php' ) {
            array_push( $page_array, $page_id );
         }
      }
      $page_num = count( $page_array );

      $get_featured_pages = new WP_Query( array(
         'posts_per_page'        => $number,
         'post_type'             =>  array( 'page' ),
         'post__in'              => $page_array,
         'orderby'               => 'date'
      ) );

      if ( !empty( $background_image ) ) {
         $bg_image_style = 'background-image:url(' . $background_image . ');background-repeat:no-repeat;background-size:cover;background-attachment:fixed;';
         $bg_image_class = 'parallax-section';
      }else {
         $bg_image_style = 'background-color:' . $background_color . ';';
         $bg_image_class = 'no-bg-image';
      }

      $section_id = '';
      if( !empty( $testimonial_menu_id ) )
         $section_id = 'id="' . $testimonial_menu_id . '"';

      echo $before_widget; ?>
      <div <?php echo $section_id; ?> class="<?php echo $bg_image_class ?> clearfix" style="<?php echo $bg_image_style; ?>">

         <div class="parallax-overlay"></div>
         <div class="section-wrapper">
            <div class="tg-container">

               <?php
               if( $page_num > 1 && $number > 1 ) {
                  $bx_class = 'testimonial-bxslider';
               }else {
                  $bx_class = 'testimonial-no-bxslider';
               }
               if( !empty ( $page_array ) ) : ?>
               <div class="testimonial-content-wrapper clearfix">
                  <div class="<?php echo $bx_class; ?>">
                     <?php while( $get_featured_pages->have_posts() ):$get_featured_pages->the_post();

                        $title_attribute = the_title_attribute( 'echo=0' ); ?>

                        <div class="testimonial-block">
                           <div class="testimonial-img-name-wrap clearfix">
                              <div class="testimonial-img">
                                 <?php if( has_post_thumbnail() ) {
                                    the_post_thumbnail( array(82, 82) );
                                 } else { echo '<img src="' . get_template_directory_uri() . '/images/placeholder-portfolio.jpg' . '">';
                                 } ?>
                              </div>
                              <div class="tesimonial-name-wrap">
                                 <div class="testimonial-name">
                                    <?php the_title(); ?>
                                 </div>

                                 <div class="testimonial-designation">
                                    <?php
                                    $himalayas_designation = get_post_meta( $post->ID, 'himalayas_testimonial_designation', true );
                                    if( !empty( $himalayas_designation ) ) {
                                       $himalayas_designation = isset( $himalayas_designation ) ? esc_attr( $himalayas_designation ) : '';
                                       echo '<span class="testimonial-deg">' . esc_html( $himalayas_designation ) . '</span>';
                                    } ?>
                                 </div>
                              </div>
                           </div>
                           <div class="testimonial-content">
                              <?php the_content(); ?>
                           </div>
                           </div><!-- .testimonial-block -->
                     <?php endwhile;

                     // Reset Post Data
                     wp_reset_query(); ?>
                  </div><!-- .tg-column-wrapper -->
               </div><!-- .testimonial-content-wrapper -->

               <?php endif; ?>
            </div><!-- .tg-container -->
         </div><!-- .section-wrapper -->
      </div>

      <?php echo $after_widget;
   }
}

/**************************************************************************************/

/**
 * Clients images widget
 */
class himalayas_our_clients_widget extends WP_Widget {

   function __construct() {
      $widget_ops = array( 'classname' => 'widget_our_clients', 'description' => __( 'Widget to show clients/partners images', 'himalayas') );
      $control_ops = array('width' => 200, 'height' => 250);
      parent::__construct( false, $name= __( 'TG: Our Clients', 'himalayas' ), $widget_ops, $control_ops );
   }

   function form( $instance ) {
      $instance = wp_parse_args( (array) $instance, array( 'client_menu_id' => '', 'client_title' => '', 'client_num' => '5', 'image-1' => '', 'image-2' => '', 'image-3' => '', 'image-4' => '', 'image-5' => '', 'link-1' => '', 'link-2' => '', 'link-3' => '', 'link-4' => '', 'link-5' => '', 'hover-text1' => '', 'hover-text2' => '', 'hover-text3' => '', 'hover-text4' => '', 'hover-text5' => '' ) );

      for ( $i=1; $i<=$instance[ 'client_num' ]; $i++ ) {
         $image = 'image-'.$i;
         $link = 'link-'.$i;
         $hover_text = 'hover-text'.$i;
         $instance[ $image ] = esc_url_raw( $instance[ $image ] );
         $instance[ $link ] = esc_url( $instance[ $link ] );
         $instance[ $hover_text ] = strip_tags( $instance[ $hover_text ] );
      } ?>
      <p><?php _e( 'Note: Enter the Client Section ID and use same for Menu item. Only used for One Page Menu.', 'himalayas' ); ?></p>

      <p>
         <label for="<?php echo $this->get_field_id( 'client_menu_id' ); ?>"><?php _e( 'Client Section ID:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id('client_menu_id'); ?>" name="<?php echo $this->get_field_name('client_menu_id'); ?>" type="text" value="<?php echo $instance[ 'client_menu_id' ]; ?>" />
      </p>

      <p>
         <label for="<?php echo $this->get_field_id( 'client_title' ); ?>"><?php _e( 'Title:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'client_title' ); ?>" name="<?php echo $this->get_field_name( 'client_title' ); ?>" type="text" value="<?php echo $instance[ 'client_title' ]; ?>" />
      </p>

      <p><?php _e( 'Note: Enter the Client number (default will show fields for 5 clients) and save it than enter the data in respective field.', 'himalayas' ); ?></p>
      <p>
         <label for="<?php echo $this->get_field_id( 'client_num' ); ?>"><?php _e( 'Client number:', 'himalayas' ); ?></label>
         <input class="widefat" id="<?php echo $this->get_field_id( 'client_num' ); ?>" name="<?php echo $this->get_field_name( 'client_num' ); ?>" type="text" value="<?php echo $instance[ 'client_num' ]; ?>" />
      </p>

      <?php for ( $i=1; $i<=$instance[ 'client_num' ]; $i++ ) {
         $image = 'image-'.$i;
         $link = 'link-'.$i;
         $hover_text = 'hover-text'.$i;
         ?>
         <p>
            <label for="<?php echo $this->get_field_id( $image ); ?>"><?php _e( 'Image ', 'himalayas' ); echo $i; ?></label><br />
            <?php
            if ( $instance[ $image ]  != '' ) :
               echo '<img id="' . $this->get_field_id( $image . 'preview' ) . '"src="' . $instance[ $image ] . '"style="max-width: 150px;" /><br />';
            endif;
            ?>
            <input style="width:100%;" id="<?php echo $this->get_field_id( $image ); ?>" name="<?php echo $this->get_field_name( $image ); ?>" type="text" value="<?php echo $instance[ $image ]; ?>" style="margin-top: 5px;"/>

            <input type="button" class="button button-primary custom_media_button" id="custom_media_client" name="<?php echo $this->get_field_name( $image ); ?>" value="<?php _e( 'Upload Image', 'himalayas' ) ?>" style="margin-top: 5px; margin-right: 30px;" onclick="imageWidget.uploader( '<?php echo $this->get_field_id( $image ); ?>' ); return false;"/>
         </p>
         <p><?php _e( 'Redirect Link ', 'himalayas' ); echo $i;?>
         <input style="width: 100%;" name="<?php echo $this->get_field_name( $link ); ?>" type="text" value="<?php if( isset ( $instance[ $link ] ) ) echo esc_url( $instance[ $link ] ); ?>" /></p>
         <p>
            <label for="<?php echo $this->get_field_id( $hover_text ); ?>"><?php _e( 'Hover Title Text ', 'himalayas' ); echo $i; ?></label>
            <input style="width: 100%;" id="<?php echo $this->get_field_id( $hover_text ); ?>" name="<?php echo $this->get_field_name( $hover_text ); ?>" type="text" value="<?php echo $instance[ $hover_text ]; ?>" />
         </p>
         <hr/>
      <?php } ?>
   <?php
   }

   function update( $new_instance, $old_instance ) {
      $instance = $old_instance;
      $instance[ 'client_menu_id' ] = strip_tags( $new_instance[ 'client_menu_id' ] );
      $instance[ 'client_title' ] = strip_tags( $new_instance[ 'client_title' ] );

      $instance[ 'client_num' ] = absint( $new_instance[ 'client_num' ] );

      for( $i=1; $i<=$instance[ 'client_num' ]; $i++ ) {
         $image = 'image-'.$i;
         $link = 'link-'.$i;
         $hover_text = 'hover-text'.$i;
         $instance[ $image ] = esc_url_raw( $new_instance[ $image ] );
         $instance[ $link ] = esc_url( $new_instance[ $link ] );
         $instance[ $hover_text ] = strip_tags( $new_instance[ $hover_text ] );
      }
      return $instance;
   }

   function widget( $args, $instance ) {
      wp_enqueue_script( 'himalayas-bxslider' );
      extract( $args );
      extract( $instance );

      $title = apply_filters( 'widget_title', isset( $instance[ 'client_title' ] ) ? $instance[ 'client_title' ] : '');

      $image_array = array();
      $link_array = array();
      $hover_text_array = array();

      for( $i=1; $i<=$instance[ 'client_num' ]; $i++ ) {
         $j = $i -1;
         $image = 'image-'.$i;
         $link = 'link-'.$i;
         $hover_text = 'hover-text'.$i;
         $image = isset( $instance[ $image ] ) ? $instance[ $image ] : '';
         $link = isset( $instance[ $link ] ) ? $instance[ $link ] : '';
         $hover_text = isset( $instance[ $hover_text ] ) ? $instance[ $hover_text ] : '';
         array_push( $image_array, $image );
         array_push( $link_array, $link );
         array_push( $hover_text_array, $hover_text );
         // For Multilingual compatibility
         if ( function_exists( 'icl_register_string' ) ) {
            if( !empty( $image ) ) {
               icl_register_string( 'Himalayas Pro', 'TG: Our Clients image' . $this->id . $j , $image_array[$j] );

               if( !empty( $link ) ) {
                  icl_register_string( 'Himalayas Pro', 'TG: Our Clients link' . $this->id . $j , $link_array[$j] );
               }
               if( !empty( $hover_text ) ) {
                  icl_register_string( 'Himalayas Pro', 'TG: Our Clients hover' . $this->id . $j , $hover_text_array[$j] );
               }
            }
         }
      }

      echo $before_widget;

      $section_id = '';
      if( !empty( $instance[ 'client_menu_id' ] ) )
         $section_id = 'id="' . $instance[ 'client_menu_id' ] . '"';
      ?>
      <div class="section-wrapper">
         <div class="tg-container">
            <div <?php echo $section_id ?> class="client-list">
               <div class="clients-all-content clearfix">
                  <div class="section-title-wrapper">
                     <div class="clients-header">
                        <?php if ( !empty( $title ) ) { ?>
                           <?php echo $before_title . esc_html( $title ) . $after_title; ?>
                        <?php } ?>
                     </div>
                  </div>

                  <?php
                  if ( !empty( $image_array ) ) {
                     $output = '';
                     $output .= '<ul class="client-slider">';
                     for( $i=1; $i<=$instance[ 'client_num' ]; $i++ ) {
                        $j = $i - 1;
                        if( !empty( $image_array[$j] ) ) {
                           $output .= '<li>';
                           // For Multilingual compatibility
                           if ( function_exists( 'icl_t' ) ) {
                              $image_array[$j] = icl_t( 'Himalayas Pro', 'TG: Our Clients image' . $this->id . $j , $image_array[$j] );
                           }
                           if( !empty( $hover_text_array[$j] ) ) {
                              if ( function_exists( 'icl_t' ) ) {
                                 $hover_text_array[$j] = icl_t( 'Himalayas Pro', 'TG: Our Clients hover' . $this->id . $j , $hover_text_array[$j] );
                              }
                           }

                           if( !empty( $link_array[$j] ) ) {
                              if ( function_exists( 'icl_t' ) ) {
                                 $link_array[$j] = icl_t( 'Himalayas Pro', 'TG: Our Clients link' . $this->id . $j , $link_array[$j] );
                              }

                              $output .= '<a href="'.$link_array[$j].'" title="'.$hover_text_array[$j].'" target="_blank"><img src="'.$image_array[$j].'" alt="'.$hover_text_array[$j].'"></a>';
                           }
                           else {
                              $output .= '<img src="'.$image_array[$j].'" alt="'.$hover_text_array[$j].'">';
                           }
                           $output .=  '</li>';
                        }
                     }
                     $output .= '</ul>';
                     echo $output;
                  }
                  ?>
               </div>
            </div>
         </div>
      </div>
      <?php
      echo $after_widget;
   }
}

/**************************************************************************************/

/**
 * Fun Facts widget
 */
class himalayas_fun_facts_widget extends WP_Widget {

   function __construct() {
      $widget_ops = array( 'classname' => 'widget_fun_facts', 'description' => __( 'Widget to show Fun Facts', 'himalayas') );
      $control_ops = array('width' => 200, 'height' => 250);
      parent::__construct( false, $name= __( 'TG: Fun Facts', 'himalayas' ), $widget_ops, $control_ops );
   }

   function form( $instance ) {
      $instance = wp_parse_args( (array) $instance, array( 'background_image' => '', 'background_color' => '', 'facts_title' => '', 'facts_desc' => '', 'fact_num-1' => '', 'fact_num-2' => '', 'fact_num-3' => '', 'fact_num-4' => '', 'fact-1' => '', 'fact-2' => '', 'fact-3' => '', 'fact-4' => '', 'icon-1' => '', 'icon-2' => '', 'icon-3' => '', 'icon-4' => '') );

      $background_color = esc_attr( $instance[ 'background_color' ] );
      $background_image = esc_url_raw( $instance[ 'background_image' ] );
      $instance[ 'facts_title' ] = strip_tags( $instance[ 'facts_title' ] );
      $instance[ 'facts_desc' ] = strip_tags( $instance[ 'facts_desc' ] );

      for ( $i=1; $i<=4; $i++ ) {
         $fact_num = 'fact_num-'.$i;
         $fact = 'fact-'.$i;
         $icon = 'icon-'.$i;
         $instance[ $fact_num ] = absint( $instance[ $fact_num ] );
         $instance[ $fact ] = strip_tags( $instance[ $fact ] );
         $instance[ $icon ] = esc_attr( $instance[ $icon ] );
      } ?>

      <p>
         <strong><?php _e( 'DESIGN SETTINGS :', 'himalayas' ); ?></strong><br />
         <label for="<?php echo $this->get_field_id( 'background_color' ); ?>"><?php _e( 'Background Color:', 'himalayas' ); ?></label><br />
         <input class="my-color-picker" type="text" data-default-color="#333333" id="<?php echo $this->get_field_id( 'background_color' ); ?>" name="<?php echo $this->get_field_name( 'background_color' ); ?>" value="<?php echo  $background_color; ?>" />
      </p>

      <p>
         <label for="<?php echo $this->get_field_id( 'background_image' ); ?>"> <?php _e( 'Image:', 'himalayas' ); ?> </label> <br />

         <?php
         if ( $instance[ 'background_image' ]  != '' ) :
            echo '<img id="' . $this->get_field_id( 'background_image' . 'preview') . '"src="' . $instance[ 'background_image' ] . '"style="max-width: 250px;" /><br />';
         endif;
         ?>
         <input type="text" class="widefat custom_media_url" id="<?php echo $this->get_field_id( 'background_image' ); ?>" name="<?php echo $this->get_field_name( 'background_image' ); ?>" value="<?php echo $instance[ 'background_image' ]; ?>" style="margin-top: 5px;"/>

         <input type="button" class="button button-primary custom_media_button" id="custom_media_button_action" name="<?php echo $this->get_field_name( 'background_image' ); ?>" value="<?php _e( 'Upload Image', 'himalayas' ) ?>" style="margin-top: 5px; margin-right: 30px;" onclick="imageWidget.uploader( '<?php echo $this->get_field_id( 'background_image' ); ?>' ); return false;"/>
      </p>

      <strong><?php _e( 'OTHER SETTINGS :', 'himalayas' ); ?></strong><br />

      <p>
         <label for="<?php echo $this->get_field_id( 'facts_title' ); ?>"><?php _e( 'Title:', 'himalayas' ); ?></label>
         <input id="<?php echo $this->get_field_id( 'facts_title' ); ?>" name="<?php echo $this->get_field_name( 'facts_title' ); ?>" type="text" value="<?php echo $instance[ 'facts_title' ]; ?>" />
      </p>

      <?php _e( 'Description','himalayas' ); ?>
      <textarea class="widefat" rows="5" cols="20" id="<?php echo $this->get_field_id( 'facts_desc' ); ?>" name="<?php echo $this->get_field_name( 'facts_desc' ); ?>"><?php echo $instance[ 'facts_desc' ];?></textarea>

      <?php for ( $i=1; $i<=4; $i++ ) {
         $fact_num = 'fact_num-'.$i;
         $fact = 'fact-'.$i;
         $icon = 'icon-'.$i;
         ?>
         <p>
            <label for="<?php echo $this->get_field_id( $fact_num ); ?>"><?php _e( 'Fact number: ', 'himalayas' ); echo $i; ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( $fact_num ); ?>" name="<?php echo $this->get_field_name( $fact_num ); ?>" type="text" value="<?php echo $instance[ $fact_num ]; ?>" />
         </p>

         <p>
            <label for="<?php echo $this->get_field_id( $fact ); ?>"><?php _e( 'Fact Detail:', 'himalayas' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( $fact ); ?>" name="<?php echo $this->get_field_name( $fact ); ?>" type="text" value="<?php echo $instance[ $fact ]; ?>" />
         </p>
         <p>
            <label for="<?php echo $this->get_field_id( $icon ); ?>"><?php _e( 'Icon Class:', 'himalayas' ); ?></label>
            <input id="<?php echo $this->get_field_id( $icon ); ?>" name="<?php echo $this->get_field_name( $icon ); ?>" placeholder="fa-trophy" type="text" value="<?php echo $instance[ $icon ]; ?>" />
         </p>
         <hr/>
      <?php } ?>
      <p>
         <?php
         $url = 'http://fontawesome.io/icons/';
         $link = sprintf( __( '<a href="%s" target="_blank">Refer here</a> For Icon Class', 'himalayas' ), esc_url( $url ) );
         echo $link;
         ?>
      </p>
   <?php
   }

   function update( $new_instance, $old_instance ) {
      $instance = $old_instance;

      $instance['background_color'] =  $new_instance['background_color'];
      $instance['background_image'] =  esc_url_raw( $new_instance['background_image'] );
      $instance[ 'facts_title' ] = strip_tags( $new_instance[ 'facts_title' ] );

      if ( current_user_can('unfiltered_html') ) {
         $instance[ 'facts_desc' ] =  $new_instance[ 'facts_desc' ];
      }
      else {
         $instance[ 'facts_desc' ] = stripslashes( wp_filter_post_kses( addslashes( $new_instance[ 'facts_desc' ] ) ) ); // wp_filter_post_kses() expects slashed
      }

      for( $i=1; $i<=4; $i++ ) {
         $fact_num = 'fact_num-'.$i;
         $fact = 'fact-'.$i;
         $icon = 'icon-'.$i;
         $instance[ $fact_num ] = absint( $new_instance[ $fact_num ] );
         $instance[ $fact ] = strip_tags( $new_instance[ $fact ] );
         $instance[ $icon ] = strip_tags( $new_instance[ $icon ] );
      }
      return $instance;
   }

   function widget( $args, $instance ) {
      extract( $args );
      extract( $instance );

      $background_color = isset( $instance[ 'background_color' ] ) ? $instance[ 'background_color' ] : '';
      $background_image = isset( $instance[ 'background_image' ] ) ? $instance[ 'background_image' ] : '';
      $facts_title = apply_filters( 'widget_title', isset( $instance[ 'facts_title' ] ) ? $instance[ 'facts_title' ] : '');
      $facts_desc = apply_filters( 'widget_text', empty( $instance['facts_desc'] ) ? '' : $instance['facts_desc'], $instance );

      echo $before_widget;
      $bg_image_style = '';
      if ( !empty( $background_image ) ) {
         $bg_image_style .= 'background-image:url(' . $background_image . ');background-repeat:no-repeat;background-size:cover;background-attachment:fixed;';
         $bg_image_class = 'parallax-section';
      }else {
         $bg_image_style .= 'background-color:' . $background_color . ';';
         $bg_image_class = 'no-bg-image';
      } ?>
      <div class="parallax-overlay"> </div>
      <div class="section-wrapper  <?php echo $bg_image_class; ?>" style="<?php echo $bg_image_style; ?>">
         <div class="tg-container">
            <div class="fact clearfix">

               <div class="section-title-wrapper">
                  <?php if( !empty( $facts_title ) ) echo $before_title . esc_html( $facts_title ) . $after_title;
                  if( !empty( $facts_desc ) ) { ?>
                     <h4 class="sub-title"><?php echo esc_textarea( $facts_desc ); ?></h4>
                  <?php } ?>
               </div>

               <div class="counter-wrapper">
	               <?php
	               for( $i=1; $i<=4; $i++ ) {
	                  $fact_num = 'fact_num-'.$i;
	                  $fact = 'fact-'.$i;
                     $icon = 'icon-'.$i;

	                  $fact_num = isset( $instance[ $fact_num ] ) ? $instance[ $fact_num ] : '';
	                  $fact = isset( $instance[ $fact ] ) ? $instance[ $fact ] : '';
                     $icon = isset( $instance[ $icon ] ) ? $instance[ $icon ] : '';

	                  // For Multilingual compatibility
	                  if( !empty( $fact ) ) {
	                     if ( function_exists( 'icl_register_string' ) ) {
	                        icl_register_string( 'Himalayas Pro', 'TG: Fun Facts' . $this->id . $i , $fact );
	                     }

	                     if ( function_exists( 'icl_t' ) ) {
	                        $fact = icl_t( 'Himalayas Pro', 'TG: Fun Facts' . $this->id . $i , $fact );
	                     }
	                  }

	                  if( !empty( $fact_num  ) ) : ?>
                        <div class="counter-block-wrapper clearfix">
                           <?php
                           echo '<span class="counter-icon"> <i class="fa ' . esc_html( $icon ) . '"></i> </span>';
                           echo '<span class="counter">' . $fact_num . '</span>';
                           echo '<span class="counter-text">' . esc_html( $fact ) . '</span>';
                          ?>
	                 	   </div>
	                  <?php endif;
	               }
	               ?>
               </div>
            </div>
         </div>
      </div>
      <?php
      echo $after_widget;
   }
}