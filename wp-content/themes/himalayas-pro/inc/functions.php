<?php

/**
 * Himalayas Pro functions and definitions
 *
 * @package ThemeGrill
 * @subpackage Himalayas Pro
 * @since Himalayas Pro 1.0
 */

add_action( 'wp_enqueue_scripts', 'himalayas_scripts' );
/**
 * Enqueue scripts and styles.
 */
 
 
function himalayas_scripts() {

   $suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

   // Load Google fonts
   $himalayas_googlefonts = array();
   array_push( $himalayas_googlefonts, get_theme_mod( 'himalayas_site_title_font', 'Roboto:300,400,700,900' ) );
   array_push( $himalayas_googlefonts, get_theme_mod( 'himalayas_site_tagline_font', 'Roboto:300,400,700,900' ) );
   array_push( $himalayas_googlefonts, get_theme_mod( 'himalayas_primary_menu_font', 'Roboto:300,400,700,900' ) );
   array_push( $himalayas_googlefonts, get_theme_mod( 'himalayas_widget_titles_font', 'Crimson Text:700' ) );
   array_push( $himalayas_googlefonts, get_theme_mod( 'himalayas_other_titles_font', 'Roboto:300,400,700,900' ) );
   array_push( $himalayas_googlefonts, get_theme_mod( 'himalayas_content_font', 'Roboto:300,400,700,900' ) );

   $himalayas_googlefonts = array_unique( $himalayas_googlefonts );
   $himalayas_googlefonts = implode("|", $himalayas_googlefonts);
   
   wp_register_style( 'custom-styles', get_template_directory_uri() . '/css/custom-styles.css');

   wp_register_style( 'himalayas-google-fonts', '//fonts.googleapis.com/css?family='.$himalayas_googlefonts );
   wp_enqueue_style( 'himalayas-google-fonts' );
   


   // Load fontawesome
   wp_enqueue_style( 'himalayas-fontawesome', get_template_directory_uri() . '/font-awesome/css/font-awesome' . $suffix . '.css', array(), '4.3.0' );

   // Register magnific popup script
   wp_enqueue_script( 'himalayas-featured-image-popup', HIMALAYAS_JS_URL. '/magnific-popup/jquery.magnific-popup' . $suffix . '.js', array( 'jquery' ), '1.0.0', true );

   wp_enqueue_style( 'himalayas-featured-image-popup-css', HIMALAYAS_JS_URL.'/magnific-popup/magnific-popup' . $suffix . '.css', array(), '1.0.0' );

   // Loads our main stylesheet
   wp_enqueue_style( 'himalayas-style', get_stylesheet_uri() );
   
   wp_enqueue_style( 'custom-styles', get_template_directory_uri() . '/css/custom-styles.css', array(), '4.3.0' );

   if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
      wp_enqueue_script( 'comment-reply' );
   }

   wp_register_script( 'himalayas-bxslider', HIMALAYAS_JS_URL . '/jquery.bxslider/jquery.bxslider' . $suffix . '.js', array( 'jquery' ), false, true );

   if ( is_front_page() ) {
      $slider = 0;
      for( $i=1; $i<=4; $i++ ) {
         $page_id = get_theme_mod( 'himalayas_slide'.$i );
         if ( !empty ( $page_id ) )  $slider++;
      }

      if( ( $slider > 1 ) && get_theme_mod( 'himalayas_slide_on_off', 0 ) == 1 ) {
         wp_enqueue_script( 'himalayas-bxslider' );
      }

   	// Onepage
   	wp_enqueue_script( 'himalayas-onepagenav', HIMALAYAS_JS_URL . '/jquery.nav' . $suffix . '.js', array( 'jquery' ), '3.0.0', true );

      // Parallax
      wp_enqueue_script( 'himalayas-parallax', HIMALAYAS_JS_URL . '/jquery.parallax-1.1.3' . $suffix . '.js', array( 'jquery' ), '1.1.3', true );

      // Waypoints Script
      wp_enqueue_script( 'himalayas-waypoints', HIMALAYAS_JS_URL . '/jquery.waypoints' . $suffix . '.js', array( 'jquery' ), '2.0.3', true );

      // CounterUp Script
      wp_enqueue_script( 'himalayas-counterup', HIMALAYAS_JS_URL . '/jquery.counterup' . $suffix . '.js', array( 'jquery' ), false, true );

      $animate = get_theme_mod( 'himalayas_animate_on', '0' );
      if( $animate == true ) {
         // Animate CSS
         wp_enqueue_style( 'himalayas-animate', get_template_directory_uri() . '/css/animate' . $suffix . '.css' );
         // WOW Js
         wp_enqueue_script( 'himalayas-wow', HIMALAYAS_JS_URL . '/wow' . $suffix . '.js', array( 'jquery' ), '1.1.2', true );
      }
      
      wp_enqueue_style( 'custom-styles');
   }

   $himalayas_user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);
   if(preg_match('/(?i)msie [1-8]/',$himalayas_user_agent)) {
      wp_enqueue_script( 'html5', HIMALAYAS_JS_URL . '/html5shiv' . $suffix . '.js', true );
   }

   //Load custom JS
   wp_enqueue_script( 'scripts', HIMALAYAS_JS_URL . '/scripts.js', array( 'jquery' ), '1.1.2', true );
}

add_action( 'wp_footer', 'himalayas_custom_scripts' );
/**
 * Enqueue scripts just before </body>.
 */
function himalayas_custom_scripts() {

   $suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

   wp_enqueue_script( 'himalayas-custom', HIMALAYAS_JS_URL . '/himalayas-custom' . $suffix . '.js', array( 'jquery' ), false, true );
}

/**************************************************************************************/

/**
 * Add admin scripts
 */

add_action('admin_enqueue_scripts', 'himalayas_image_uploader');

function himalayas_image_uploader( $hook ) {
   global $post_type;
   if( $hook == 'widgets.php' || $hook == 'customize.php' ) {
	   //For image uploader
	   wp_enqueue_media();
	   wp_enqueue_script( 'himalayas-script', HIMALAYAS_JS_URL . '/image-uploader.js', false, '1.0', true );

	   //For Color Picker
	   wp_enqueue_style( 'wp-color-picker' );
	   wp_enqueue_script( 'himalayas-color-picker', HIMALAYAS_JS_URL . '/color-picker.js', array( 'wp-color-picker' ), false);
	}
   if( $post_type == 'page' ) {
      wp_enqueue_script( 'himalayas-meta-toggle', HIMALAYAS_JS_URL . '/metabox-toggle.js', false, '1.0', true );
   }
}

/****************************************************************************************/

add_filter( 'excerpt_length', 'himalayas_excerpt_length' );
/**
 * Sets the post excerpt length to 40 words.
 *
 * function tied to the excerpt_length filter hook.
 *
 * @uses filter excerpt_length
 */
function himalayas_excerpt_length( $length ) {
   return 25;
}

add_filter( 'excerpt_more', 'himalayas_continue_reading' );
/**
 * Returns a "Continue Reading" link for excerpts
 */
function himalayas_continue_reading() {
   return '';
}

/**************************************************************************************/

if ( ! function_exists( 'himalayas_excerpt' ) ) :
/**
 * Function to show the footer info, copyright information
 */
function himalayas_excerpt( $limit ) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }
  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
  return $excerpt;
}
endif;

/****************************************************************************************/

/**
 * Removing the default style of wordpress gallery
 */
add_filter( 'use_default_gallery_style', '__return_false' );

/**
 * Filtering the size to be medium from thumbnail to be used in WordPress gallery as a default size
 */
function himalayas_gallery_atts( $out, $pairs, $atts ) {
   $atts = shortcode_atts( array(
   'size' => 'medium',
   ), $atts );

   $out['size'] = $atts['size'];

   return $out;
}
add_filter( 'shortcode_atts_gallery', 'himalayas_gallery_atts', 10, 3 );

/****************************************************************************************/

if ( ! function_exists( 'himalayas_entry_meta' ) ) :
/**
 * Shows meta information of post.
 */
function himalayas_entry_meta() {
   if ( 'post' == get_post_type() ) :
      echo '<div class="entry-meta">';

      $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
      if (  ( 'U' ) !== get_the_modified_time( 'U' ) ) {
         $time_string .= '<time class="updated" datetime="%3$s">%4$s</time>';
      }
      $time_string = sprintf( $time_string,
         esc_attr( get_the_date( 'c' ) ),
         esc_html( get_the_date() ),
         esc_attr( get_the_modified_date( 'c' ) ),
         esc_html( get_the_modified_date() )
      );
      printf( __( '<span class="posted-on"><a href="%1$s" title="%2$s" rel="bookmark"> %3$s</a></span>', 'himalayas' ),
         esc_url( get_permalink() ),
         esc_attr( get_the_time() ),
         $time_string
      ); ?>

      <span class="byline author vcard"><a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" title="<?php echo get_the_author(); ?>"><?php echo esc_html( get_the_author() ); ?></a></span>

      <?php
      if ( ! post_password_required() && comments_open() ) { ?>
         <span class="comments-link"><?php comments_popup_link( __( '0 Comment', 'himalayas' ), __( '1 Comment', 'himalayas' ), __( ' % Comments', 'himalayas' ) ); ?></span>
      <?php }

      if( has_category() ) { ?>
         <span class="cat-links"><?php the_category(', '); ?></span>
       <?php }

      $tags_list = get_the_tag_list( '<span class="tag-links">', ', ', '</span>' );
      if ( $tags_list ) echo $tags_list;

      edit_post_link( __( 'Edit', 'himalayas' ), '<span class="edit-link">', '</span>' );

      echo '</div>';
   endif;
}
endif;

/****************************************************************************************/

if ( ! function_exists( 'himalayas_layout_class' ) ) :
/**
 * Return the layout as selected by user
 */
function himalayas_layout_class() {
   global $post;
   $classes = '';

   if( $post ) { $layout_meta = get_post_meta( $post->ID, 'himalayas_page_layout', true ); }

   if( is_home() ) {
      $queried_id = get_option( 'page_for_posts' );
      $layout_meta = get_post_meta( $queried_id, 'himalayas_page_layout', true );
   }
   if( empty( $layout_meta ) || is_archive() || is_search() ) { $layout_meta = 'default_layout'; }

   $himalayas_default_layout = get_theme_mod( 'himalayas_default_layout', 'right_sidebar' );
   $himalayas_default_page_layout = get_theme_mod( 'himalayas_default_page_layout', 'right_sidebar' );
   $himalayas_default_post_layout = get_theme_mod( 'himalayas_default_single_posts_layout', 'right_sidebar' );
   $himalayas_woo_product_layout = get_theme_mod( 'himalayas_woo_product_layout', 'no_sidebar_full_width' );
   $himalayas_woo_shop_layout = get_theme_mod( 'himalayas_woo_shop_layout', 'no_sidebar_full_width' );

   if( $layout_meta == 'default_layout' ) {
      if( is_page() ) {
         if( $himalayas_default_page_layout == 'right_sidebar' ) { $classes = 'right_sidebar'; }
         elseif( $himalayas_default_page_layout == 'left_sidebar' ) { $classes = 'left-sidebar'; }
         elseif( $himalayas_default_page_layout == 'no_sidebar_full_width' ) { $classes = 'no-sidebar-full-width'; }
         elseif( $himalayas_default_page_layout == 'no_sidebar_content_centered' ) { $classes = 'no-sidebar'; }
      }
      elseif( function_exists( 'himalayas_is_in_woocommerce_page' ) && is_product() ) {
         if( $himalayas_woo_product_layout == 'right_sidebar' ) { $classes = ''; }
         elseif( $himalayas_woo_product_layout == 'left_sidebar' ) { $classes = 'left-sidebar'; }
         elseif( $himalayas_woo_product_layout == 'no_sidebar_full_width' ) { $classes = 'no-sidebar-full-width'; }
         elseif( $himalayas_woo_product_layout == 'no_sidebar_content_centered' ) { $classes = 'no-sidebar'; }
      }
      elseif( is_single() ) {
         if( $himalayas_default_post_layout == 'right_sidebar' ) { $classes = 'right_sidebar'; }
         elseif( $himalayas_default_post_layout == 'left_sidebar' ) { $classes = 'left-sidebar'; }
         elseif( $himalayas_default_post_layout == 'no_sidebar_full_width' ) { $classes = 'no-sidebar-full-width'; }
         elseif( $himalayas_default_post_layout == 'no_sidebar_content_centered' ) { $classes = 'no-sidebar'; }
      }
      elseif( function_exists( 'himalayas_is_in_woocommerce_page' ) && himalayas_is_in_woocommerce_page() ) {
         if( $himalayas_woo_shop_layout == 'right_sidebar' ) { $classes = ''; }
         elseif( $himalayas_woo_shop_layout == 'left_sidebar' ) { $classes = 'left-sidebar'; }
         elseif( $himalayas_woo_shop_layout == 'no_sidebar_full_width' ) { $classes = 'no-sidebar-full-width'; }
         elseif( $himalayas_woo_shop_layout == 'no_sidebar_content_centered' ) { $classes = 'no-sidebar'; }
      }
      elseif( $himalayas_default_layout == 'right_sidebar' ) { $classes = 'right_sidebar'; }
      elseif( $himalayas_default_layout == 'left_sidebar' ) { $classes = 'left-sidebar'; }
      elseif( $himalayas_default_layout == 'no_sidebar_full_width' ) { $classes = 'no-sidebar-full-width'; }
      elseif( $himalayas_default_layout == 'no_sidebar_content_centered' ) { $classes = 'no-sidebar'; }
   }
   elseif( $layout_meta == 'right_sidebar' ) { $classes = 'right_sidebar'; }
   elseif( $layout_meta == 'left_sidebar' ) { $classes = 'left-sidebar'; }
   elseif( $layout_meta == 'no_sidebar_full_width' ) { $classes = 'no-sidebar-full-width'; }
   elseif( $layout_meta == 'no_sidebar_content_centered' ) { $classes = 'no-sidebar'; }

   return $classes;
}
endif;

/****************************************************************************************/

if ( ! function_exists( 'himalayas_sidebar_select' ) ) :
/**
 * Function to select the sidebar
 */
function himalayas_sidebar_select() {
   global $post;

   if( $post ) { $layout_meta = get_post_meta( $post->ID, 'himalayas_page_layout', true ); }

   if( is_home() ) {
      $queried_id = get_option( 'page_for_posts' );
      $layout_meta = get_post_meta( $queried_id, 'himalayas_page_layout', true );
   }

   if( empty( $layout_meta ) || is_archive() || is_search() ) { $layout_meta = 'default_layout'; }

   $himalayas_default_layout = get_theme_mod( 'himalayas_default_layout', 'right_sidebar' );
   $himalayas_default_page_layout = get_theme_mod( 'himalayas_default_page_layout', 'right_sidebar' );
   $himalayas_default_post_layout = get_theme_mod( 'himalayas_default_single_posts_layout', 'right_sidebar' );
   $himalayas_woo_product_layout = get_theme_mod( 'himalayas_woo_product_layout', 'no_sidebar_full_width' );
   $himalayas_woo_shop_layout = get_theme_mod( 'himalayas_woo_shop_layout', 'no_sidebar_full_width' );

   if( $layout_meta == 'default_layout' ) {
      if( is_page() ) {
         if( $himalayas_default_page_layout == 'right_sidebar' ) { get_sidebar(); }
         elseif ( $himalayas_default_page_layout == 'left_sidebar' ) { get_sidebar( 'left' ); }
      }
      if( function_exists( 'himalayas_is_in_woocommerce_page' ) && is_product() ) {
         if( $himalayas_woo_product_layout == 'right_sidebar' || $himalayas_woo_product_layout == 'left_sidebar' ) { get_sidebar(); }
      }
      elseif( is_single() ) {
         if( $himalayas_default_post_layout == 'right_sidebar' ) { get_sidebar(); }
         elseif ( $himalayas_default_post_layout == 'left_sidebar' ) { get_sidebar( 'left' ); }
      }
      elseif( function_exists( 'himalayas_is_in_woocommerce_page' ) && himalayas_is_in_woocommerce_page() ) {
         if( $himalayas_woo_shop_layout == 'right_sidebar' || $himalayas_woo_shop_layout == 'left_sidebar' ) { get_sidebar(); }

      }
      elseif( $himalayas_default_layout == 'right_sidebar' ) { get_sidebar(); }
      elseif ( $himalayas_default_layout == 'left_sidebar' ) { get_sidebar( 'left' ); }
   }
   elseif( $layout_meta == 'right_sidebar' ) { get_sidebar(); }
   elseif( $layout_meta == 'left_sidebar' ) { get_sidebar( 'left' ); }
}
endif;

/**************************************************************************************/

if ( ! function_exists( 'himalayas_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 */
function himalayas_comment( $comment, $args, $depth ) {
   $GLOBALS['comment'] = $comment;
   switch ( $comment->comment_type ) :
      case 'pingback' :
      case 'trackback' :
      // Display trackbacks differently than normal comments.
   ?>
   <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
      <p><?php _e( 'Pingback:', 'himalayas' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'himalayas' ), '<span class="edit-link">', '</span>' ); ?></p>
   <?php
         break;
      default :
      // Proceed with normal comments.
      global $post;
   ?>
   <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
      <article id="comment-<?php comment_ID(); ?>" class="comment">
         <header class="comment-meta comment-author vcard">
            <?php
               echo get_avatar( $comment, 74 );
               printf( '<div class="comment-author-link"><i class="fa fa-user"></i>%1$s%2$s</div>',
                  get_comment_author_link(),
                  // If current post author is also comment author, make it known visually.
                  ( $comment->user_id === $post->post_author ) ? '<span>' . __( 'Post author', 'himalayas' ) . '</span>' : ''
               );
               printf( '<div class="comment-date-time"><i class="fa fa-calendar-o"></i>%1$s</div>',
                  sprintf( __( '%1$s at %2$s', 'himalayas' ), get_comment_date(), get_comment_time() )
               );
               printf( '<a class="comment-permalink" href="%1$s"><i class="fa fa-link"></i>Permalink</a>', esc_url( get_comment_link( $comment->comment_ID ) ) );
               edit_comment_link();
            ?>
         </header><!-- .comment-meta -->

         <?php if ( '0' == $comment->comment_approved ) : ?>
            <p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'himalayas' ); ?></p>
         <?php endif; ?>

         <section class="comment-content comment">
            <?php comment_text(); ?>
            <?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply', 'himalayas' ), 'after' => '', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
         </section><!-- .comment-content -->

      </article><!-- #comment-## -->
   <?php
      break;
   endswitch; // end comment_type check
}
endif;

/**************************************************************************************/

/* Register shortcodes. */
add_action( 'init', 'himalayas_add_shortcodes' );
/**
 * Creates new shortcodes for use in any shortcode-ready area.  This function uses the add_shortcode()
 * function to register new shortcodes with WordPress.
 *
 * @uses add_shortcode() to create new shortcodes.
 */
function himalayas_add_shortcodes() {
   /* Add theme-specific shortcodes. */
   add_shortcode( 'the-year', 'himalayas_the_year_shortcode' );
   add_shortcode( 'site-link', 'himalayas_site_link_shortcode' );
   add_shortcode( 'wp-link', 'himalayas_wp_link_shortcode' );
   add_shortcode( 'tg-link', 'himalayas_themegrill_link_shortcode' );
}

/**
 * Shortcode to display the current year.
 *
 * @uses date() Gets the current year.
 * @return string
 */
function himalayas_the_year_shortcode() {
   return date( 'Y' );
}

/**
 * Shortcode to display a link back to the site.
 *
 * @uses get_bloginfo() Gets the site link
 * @return string
 */
function himalayas_site_link_shortcode() {
   return '<a href="' . esc_url( home_url( '/' ) ) . '" title="' . esc_attr( get_bloginfo( 'name', 'display' ) ) . '" ><span>' . get_bloginfo( 'name', 'display' ) . '</span></a>';
}

/**
 * Shortcode to display a link to WordPress.org.
 *
 * @return string
 */
function himalayas_wp_link_shortcode() {
   return '<a href="' .esc_url( 'http://wordpress.org' ). '" target="_blank" title="' . esc_attr__( 'WordPress', 'himalayas' ) . '"><span>' . __( 'WordPress', 'himalayas' ) . '</span></a>';
}

/**
 * Shortcode to display a link to himalayas.com.
 *
 * @return string
 */
function himalayas_themegrill_link_shortcode() {
   return '<a href="' .esc_url( 'http://themegrill.com/themes/himalayas-pro' ) .'" target="_blank" title="'.esc_attr__( 'ThemeGrill', 'himalayas' ).'" rel="designer"><span>'.__( 'ThemeGrill', 'himalayas') .'</span></a>';
}

add_action( 'himalayas_footer_copyright', 'himalayas_footer_copyright', 10 );
/**
 * Function to show the footer info, copyright information
 */
if ( ! function_exists( 'himalayas_footer_copyright' ) ) :
function himalayas_footer_copyright() {

   $default_footer_value = '<span class="copyright-text">' . get_theme_mod( 'himalayas_footer_editor', __( 'Copyright &copy; ', 'himalayas' ).'[the-year] [site-link]. All rights reserved. '.__( 'Theme: Himalayas by ', 'himalayas' ).'[tg-link]. '.__( 'Powered by ', 'himalayas' ).'[wp-link].' ) . '</span>';

   $himalayas_footer_copyright = '<div class="copyright">'.$default_footer_value.'</div>';
   echo do_shortcode( $himalayas_footer_copyright );
}
endif;

/**************************************************************************************/

add_action( 'wp_head', 'himalayas_custom_css' );
/**
 * Hooks the Custom Internal CSS to head section
 */
function himalayas_custom_css() {
   $primary_color = get_theme_mod( 'himalayas_primary_color', '#32c4d1' );;
   $himalayas_internal_css = '';
   if( $primary_color != '#32c4d1' ) {
      $himalayas_internal_css = ' .about-btn a:hover,.bttn:hover,.icon-img-wrap:hover,.navigation .nav-links a:hover,.service_icon_class .image-wrap:hover i,.slider-readmore:before,.subscribe-form .subscribe-submit .subscribe-btn,button,input[type=button]:hover,input[type=reset]:hover,input[type=submit]:hover{background:'.$primary_color.'}a{color:'.$primary_color.'}#site-navigation .menu li.current-one-page-item a,#site-navigation .menu li:hover a,.about-title a:hover,.caption-title a:hover,.header-wrapper.no-slider #site-navigation .menu li.current-one-page-item a,.header-wrapper.no-slider #site-navigation .menu li:hover a,.header-wrapper.no-slider .search-icon:hover,.header-wrapper.stick #site-navigation .menu li.current-one-page-item a,.header-wrapper.stick #site-navigation .menu li:hover a,.header-wrapper.stick .search-icon:hover,.scroll-down,.search-icon:hover,.service-title a:hover{color:'.$primary_color.'}.comments-area .comment-author-link span{background-color:'.$primary_color.'}.slider-readmore:hover{border:1px solid '.$primary_color.'}.icon-wrap:hover,.image-wrap:hover,.port-link a:hover,.counter-block-wrapper{border-color:'.$primary_color.'}.main-title:after,.main-title:before{border-top:2px solid '.$primary_color.'}.blog-view,.port-link a:hover{background:'.$primary_color.'}.port-title-wrapper .port-desc{color:'.$primary_color.'}#top-footer a:hover,.blog-title a:hover,.entry-title a:hover,.footer-nav li a:hover,.footer-social a:hover,.widget ul li a:hover,.widget ul li:hover:before{color:'.$primary_color.'}.contact-form-wrapper input[type=submit],.default-wp-page a:hover,.team-desc-wrapper{background:'.$primary_color.'}.scrollup{background-color:'.$primary_color.'}#stick-navigation li.current-one-page-item a,#stick-navigation li:hover a,.blog-hover-link a:hover,.entry-btn .btn:hover{background:'.$primary_color.'}#secondary .widget-title:after,#top-footer .widget-title:after{background:'.$primary_color.'}.widget-tags a:hover{background:'.$primary_color.';border:1px solid '.$primary_color.'}.num-404,.counter-block-wrapper .counter{color:'.$primary_color.'}.error,.testimonial-content-wrapper .bx-pager-item a:hover, .testimonial-content-wrapper .bx-pager-item a.active{background:'.$primary_color.'}';
   }

   // Google Fonts Custom CSS
   if( get_theme_mod( 'himalayas_site_title_font', 'Roboto:300,400,700,900' ) != 'Roboto:300,400,700,900' ) {
      $himalayas_internal_css .= ' #site-title a { font-family: "'.get_theme_mod( 'himalayas_site_title_font', 'Roboto:300,400,700,900' ).'"; }';
   }
   if( get_theme_mod( 'himalayas_site_tagline_font', 'Roboto:300,400,700,900' ) != 'Roboto:300,400,700,900' ) {
      $himalayas_internal_css .= ' #site-description { font-family: "'.get_theme_mod( 'himalayas_site_tagline_font', 'Roboto:300,400,700,900' ).'"; }';
   }
   if( get_theme_mod( 'himalayas_primary_menu_font', 'Roboto:300,400,700,900' ) != 'Roboto:300,400,700,900' ) {
      $himalayas_internal_css .= ' .main-navigation li, .site-header .menu-toggle { font-family: "'.get_theme_mod( 'himalayas_primary_menu_font', 'Roboto:300,400,700,900' ).'"; }';
   }
   if( get_theme_mod( 'himalayas_widget_titles_font', 'Crimson Text:700' ) != 'Crimson Text:700' ) {
      $himalayas_internal_css .= ' .main-title { font-family: "'.get_theme_mod( 'himalayas_widget_titles_font', 'Crimson Text:700' ).'"; }';
   }
   if( get_theme_mod( 'himalayas_other_titles_font', 'Roboto:300,400,700,900' ) != 'Roboto:300,400,700,900' ) {
      $himalayas_internal_css .= ' h1, h2, h3, h4, h5, h6 { font-family: "'.get_theme_mod( 'himalayas_other_titles_font', 'Roboto:300,400,700,900' ).'"; }';
   }
   if( get_theme_mod( 'himalayas_content_font', 'Roboto:300,400,700,900' ) != 'Roboto:300,400,700,900' ) {
      $himalayas_internal_css .= ' body, button, input, select, textarea, p, blockquote p, .entry-meta, .more-link { font-family: "'.get_theme_mod( 'himalayas_content_font', 'Roboto:300,400,700,900' ).'"; }';
   } // End of Google Fonts Custom CSS

   // Font Size Options
   $himalayas_fonts_size_custom = himalayas_font_size_func();

   foreach ($himalayas_fonts_size_custom as $himalayas_font) {

      $himalayas_font_size_setting = $himalayas_font['himalayas_font_size_setting'];
      foreach ($himalayas_font_size_setting as $himalayas_font_size_css) {

         if( get_theme_mod( $himalayas_font_size_css['id'], $himalayas_font_size_css['default'] ) != $himalayas_font_size_css['default'] ) {
            $himalayas_internal_css .= $himalayas_font_size_css['custom_css'] . '{ font-size: '.get_theme_mod( $himalayas_font_size_css['id'], $himalayas_font_size_css['default'] ).'px; }';
         }
      }
   }// End of Font Size Options

   // Color Options
   $himalayas_color_custom = himalayas_color_func();

   foreach ($himalayas_color_custom as $himalayas_color) {

      $himalayas_color_settings = $himalayas_color['himalayas_color_settings'];
      foreach ($himalayas_color_settings as $himalayas_color_css) {

         if( get_theme_mod( $himalayas_color_css['id'], $himalayas_color_css['default'] ) != $himalayas_color_css['default'] ) {
            $color_code = array_key_exists( 'color_location', $himalayas_color_css ) ? 'background-color: ' : 'color: ';
            $himalayas_internal_css .= $himalayas_color_css['color_custom_css'] . '{' . $color_code .get_theme_mod( $himalayas_color_css['id'], $himalayas_color_css['default'] ).'; }';
         }
      }
   } // End of Color Options

   if( !empty( $himalayas_internal_css ) ) {
      echo '<!-- '.get_bloginfo('name').' Internal Styles -->'; ?>
      <style type="text/css"><?php echo $himalayas_internal_css; ?></style>
      <?php
   }

   $himalayas_custom_css = get_theme_mod( 'himalayas_custom_css', '' );
   if( !empty( $himalayas_custom_css ) ) {
      echo '<!-- '.get_bloginfo('name').' Custom Styles -->';
      ?><style type="text/css"><?php echo esc_html( $himalayas_custom_css ); ?></style><?php
   }
}

/**************************************************************************************/

if ( ! function_exists( 'himalayas_archive_title' ) ) :
/**
 * Shim for `the_archive_title()`.
 *
 * Display the archive title based on the queried object.
 *
 * @todo Remove this function when WordPress 4.3 is released.
 *
 * @param string $before Optional. Content to prepend to the title. Default empty.
 * @param string $after  Optional. Content to append to the title. Default empty.
 */
function himalayas_archive_title( $before = '', $after = '' ) {
   if ( is_category() ) {
      $title = sprintf( esc_html__( 'Category: %s', 'himalayas' ), single_cat_title( '', false ) );
   } elseif ( is_tag() ) {
      $title = sprintf( esc_html__( 'Tag: %s', 'himalayas' ), single_tag_title( '', false ) );
   } elseif ( is_author() ) {
      $title = sprintf( esc_html__( 'Author: %s', 'himalayas' ), '<span class="vcard">' . get_the_author() . '</span>' );
   } elseif ( is_year() ) {
      $title = sprintf( esc_html__( 'Year: %s', 'himalayas' ), get_the_date( esc_html_x( 'Y', 'yearly archives date format', 'himalayas' ) ) );
   } elseif ( is_month() ) {
      $title = sprintf( esc_html__( 'Month: %s', 'himalayas' ), get_the_date( esc_html_x( 'F Y', 'monthly archives date format', 'himalayas' ) ) );
   } elseif ( is_day() ) {
      $title = sprintf( esc_html__( 'Day: %s', 'himalayas' ), get_the_date( esc_html_x( 'F j, Y', 'daily archives date format', 'himalayas' ) ) );
   } elseif ( is_tax( 'post_format' ) ) {
      if ( is_tax( 'post_format', 'post-format-aside' ) ) {
         $title = esc_html_x( 'Asides', 'post format archive title', 'himalayas' );
      } elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
         $title = esc_html_x( 'Galleries', 'post format archive title', 'himalayas' );
      } elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
         $title = esc_html_x( 'Images', 'post format archive title', 'himalayas' );
      } elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
         $title = esc_html_x( 'Videos', 'post format archive title', 'himalayas' );
      } elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
         $title = esc_html_x( 'Quotes', 'post format archive title', 'himalayas' );
      } elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
         $title = esc_html_x( 'Links', 'post format archive title', 'himalayas' );
      } elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
         $title = esc_html_x( 'Statuses', 'post format archive title', 'himalayas' );
      } elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
         $title = esc_html_x( 'Audio', 'post format archive title', 'himalayas' );
      } elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
         $title = esc_html_x( 'Chats', 'post format archive title', 'himalayas' );
      }
   } elseif ( is_post_type_archive() ) {
      $title = sprintf( esc_html__( 'Archives: %s', 'himalayas' ), post_type_archive_title( '', false ) );
   } elseif ( is_tax() ) {
      $tax = get_taxonomy( get_queried_object()->taxonomy );
      /* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
      $title = sprintf( esc_html__( '%1$s: %2$s', 'himalayas' ), $tax->labels->singular_name, single_term_title( '', false ) );
   } else {
      $title = esc_html__( 'Archives', 'himalayas' );
   }

   /**
    * Filter the archive title.
    *
    * @param string $title Archive title to be displayed.
    */
   $title = apply_filters( 'get_the_archive_title', $title );

   if ( ! empty( $title ) ) {
      echo $before . $title . $after;  // WPCS: XSS OK.
   }
}
endif;

if ( ! function_exists( 'himalayas_archive_description' ) ) :
/**
 * Shim for `the_archive_description()`.
 *
 * Display category, tag, or term description.
 *
 * @param string $before Optional. Content to prepend to the description. Default empty.
 * @param string $after  Optional. Content to append to the description. Default empty.
 */
function himalayas_archive_description( $before = '', $after = '' ) {
   $description = apply_filters( 'get_the_archive_description', term_description() );

   if ( ! empty( $description ) ) {
      /**
       * Filter the archive description.
       *
       * @see term_description()
       *
       * @param string $description Archive description to be displayed.
       */
      echo $before . $description . $after;  // WPCS: XSS OK.
   }
}
endif;

/**************************************************************************************/

if ( !function_exists('himalayas_responsive_video') ) :
/*
 * Creating responsive video for posts/pages
 */
function himalayas_responsive_video( $html, $url, $attr, $post_ID ) {
   return '<div class="fitvids-video">'.$html.'</div>';
}
add_filter( 'embed_oembed_html', 'himalayas_responsive_video', 10, 4 );
endif;

/**************************************************************************************/

/**
 * Making the theme Woocommrece compatible
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10);

add_action('woocommerce_before_main_content', 'himalayas_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'himalayas_wrapper_end', 10);

add_action( 'woocommerce_sidebar', 'himalayas_get_sidebar', 10 );

function himalayas_wrapper_start() {
   $himalayas_woo_layout = himalayas_layout_class();

  echo '<div id="content" class="site-content"><main id="main" class="clearfix ' . $himalayas_woo_layout . '"><div class="tg-container"><div id="primary">';
}

function himalayas_wrapper_end() {
  echo '</div>';
}
function himalayas_get_sidebar() {
   himalayas_sidebar_select();
   echo '</div></main></div>';
}

add_theme_support( 'woocommerce' );

if ( ! function_exists( 'himalayas_woo_related_products_limit' ) ):

/**
 * WooCommerce Extra Feature
 * --------------------------
 *
 * Change number of related products on product page
 * Set your own value for 'posts_per_page'
 *
 */
function himalayas_woo_related_products_limit() {
   global $product;
   $args = array(
      'posts_per_page' => 4,
      'columns' => 4,
      'orderby' => 'rand'
   );
   return $args;
}
endif;
add_filter( 'woocommerce_output_related_products_args', 'himalayas_woo_related_products_limit' );

if ( class_exists('woocommerce') && !function_exists( 'himalayas_is_in_woocommerce_page' ) ):
/*
 * WooCommerce - conditional to check if WooCommerce related page showed
 */
function himalayas_is_in_woocommerce_page() {
return ( is_shop() || is_product_category() || is_product_tag() || is_product() || is_cart() || is_checkout() || is_account_page() ) ? true : false;
}
endif;