<?php
/**
 * Himalayas Pro Theme Customizer
 *
 * @package ThemeGrill
 * @subpackage Himalayas Pro
 * @since Himalayas Pro 1.0
 */
function himalayas_customize_register($wp_customize) {

	/* Start of the Header Options */
   $wp_customize->add_panel('himalayas_header_options', array(
      'capabitity' => 'edit_theme_options',
      'description' => __('Contain all the Header related options', 'himalayas'),
      'priority' => 300,
      'title' => __('Himalayas Header Options', 'himalayas')
   ));

   // Sticky Option
	$wp_customize->add_section('himalayas_sticky_section', array(
		'priority' => 10,
      'title' => __('Header Sticky/non-sticky', 'himalayas'),
      'description' => __('Header is sticky by default.', 'himalayas'),
      'panel' => 'himalayas_header_options'
   ));

	$wp_customize->add_setting('himalayas_sticky_on_off',	array(
		'default' => 0,
      'capability' => 'edit_theme_options',
		'sanitize_callback' => 'himalayas_sanitize_checkbox'
	));
	$wp_customize->add_control('himalayas_sticky_on_off',	array(
		'type' => 'checkbox',
		'label' => __('Check to make header non-sticky', 'himalayas' ),
		'section' => 'himalayas_sticky_section'
	));

	// Header Non-Transparent Option
	$wp_customize->add_section('himalayas_transparent_section', array(
		'priority' => 20,
      'title' => __('Header Transparency', 'himalayas'),
      'description' => __('By default Header is transparent when slider is used.', 'himalayas'),
      'panel' => 'himalayas_header_options'
   ));

	$wp_customize->add_setting('himalayas_trans_off',	array(
		'default' => 0,
      'capability' => 'edit_theme_options',
		'sanitize_callback' => 'himalayas_sanitize_checkbox'
	));
	$wp_customize->add_control('himalayas_trans_off',	array(
		'type' => 'checkbox',
		'label' => __('Make header non transparent.', 'himalayas' ),
		'section' => 'himalayas_transparent_section'
	));

   // Logo Option
	$wp_customize->add_section('himalayas_header_title_logo', array(
		'title'     => __( 'Header Title/Tagline and Logo', 'himalayas' ),
		'priority'  => 30,
		'description' => __( '<strong>Note:</strong> The recommended height for header logo image is 68px.', 'himalayas' ),
  		'panel' => 'himalayas_header_options'
	));

	$wp_customize->add_setting('himalayas_logo', array(
		'default' => '',
      'capability' => 'edit_theme_options',
      'sanitize_callback' => 'himalayas_sanitize_url',
      'sanitize_js_callback' => 'himalayas_sanitize_js_url'
	));
	$wp_customize->add_control(
		new WP_Customize_Image_Control($wp_customize, 'himalayas_logo', array(
			'label' 		=> __( 'Header Logo Image Upload', 'himalayas' ),
			'section' 	=> 'himalayas_header_title_logo',
			'settings' 	=> 'himalayas_logo'
		))
	);
	// Header Logo and Title/Tagline Display Option
	$wp_customize->add_setting('himalayas_header_logo_placement', array(
      'default' => 'header_text_only',
      'capability' => 'edit_theme_options',
      'sanitize_callback' => 'himalayas_radio_sanitize'
   ));
   $wp_customize->add_control('himalayas_header_logo_placement', array(
      'type' => 'radio',
      'label' => __('Choose the required option', 'himalayas'),
      'section' => 'himalayas_header_title_logo',
      'choices' => array(
         'header_logo_only' => __('Header Logo Only', 'himalayas'),
         'header_text_only' => __('Header Text Only', 'himalayas'),
         'show_both' => __('Show Both', 'himalayas'),
         'disable' => __('Disable', 'himalayas')
      )
   ));
	// End of the Header Options

 /**************************************************************************************/

	/* Start of the Slider Options */
   $wp_customize->add_panel('himalayas_slider_options', array(
   	'priority'  => 310,
      'capabitity' => 'edit_theme_options',
      'description' => __('Contain all the slider related options', 'himalayas'),
      'title' => __('Himalayas Slider Options', 'himalayas')
   ));

   // Slider Section
	$wp_customize->add_section( 'himalayas_slider', array(
		'title'     => __( 'Slider Settings', 'himalayas' ),
		'priority'  => 10,
		'description' => '<strong>'.__( 'Note', 'himalayas').'</strong><br/>'.__( '1. To display the Slider first check Enable the slider below. Now create the page for each slider and enter title, text and featured image. Choose that pages in the dropdown options.', 'himalayas').'<br/>'.__( '2. The recommended size for the slider image is 1600 x 780 pixels. For better functioning of slider use equal size images for each slide.', 'himalayas' ).'<br/>'.__( '3. If page do not have featured Image than that page will not included in slider show.', 'himalayas' ).'<br/>'.__( '4. If you need more or less number of slides, input the new number below, click on Save and Publish button and refresh the browser.', 'himalayas' ),
      'panel' => 'himalayas_slider_options'
	));

	// Enable or Disable the Slider
	$wp_customize->add_setting('himalayas_slide_on_off', array(
		'default' => '',
      'capability' => 'edit_theme_options',
      'sanitize_callback' => 'himalayas_sanitize_checkbox'
	));
	$wp_customize->add_control('himalayas_slide_on_off', array(
		'label' => __( 'Enable the slider', 'himalayas' ),
		'section' => 'himalayas_slider',
		'type'	=> 'checkbox',
		'priority' => 10
	));

	// Choose Slider Layout
	$wp_customize->add_setting('himalayas_slider_layout', array(
		'default' => 'slider-style-1',
      'capability' => 'edit_theme_options',
      'sanitize_callback' => 'himalayas_radio_sanitize'
	));
	$wp_customize->add_control('himalayas_slider_layout', array(
		'label' => __( 'Select Slider Layout', 'himalayas' ),
		'type' => 'select',
		'section' => 'himalayas_slider',
		'priority' => 11,
		'choices' => array(
			'slider-style-1' => 'Layout One',
         'slider-style-2' => 'Layout Two'
      )
	));

	// Number of Slider
	$wp_customize->add_setting('himalayas_slider_number', array(
		'default' => '4',
      'capability' => 'edit_theme_options',
      'sanitize_callback' => 'himalayas_sanitize_integer'
	));
	$wp_customize->add_control('himalayas_slider_number', array(
		'label' => __( 'Enter the slider number', 'himalayas' ),
		'section' => 'himalayas_slider',
		'priority' => 12
	));

	// Type of Transition between Slides
	$wp_customize->add_setting('himalayas_slider_mode', array(
		'default' => 'fade',
      'capability' => 'edit_theme_options',
      'sanitize_callback' => 'himalayas_radio_sanitize'
	));
	$wp_customize->add_control('himalayas_slider_mode', array(
		'label' => __( 'Choose the Type of transition', 'himalayas' ),
		'type' => 'select',
		'section' => 'himalayas_slider',
		'priority' => 14,
		'choices' => array(
			'fade' => 'Fade',
         'horizontal' => 'Horizontal',
         'vertical' => 'Vertical'
      )
	));

	// Slider Delay Time
	$wp_customize->add_setting('himalayas_slider_delay', array(
		'default' => '5',
      'capability' => 'edit_theme_options',
      'sanitize_callback' => 'himalayas_sanitize_integer'
	));
	$wp_customize->add_control('himalayas_slider_delay', array(
		'label' => __( 'Slides delay time in Second', 'himalayas' ),
		'section' => 'himalayas_slider',
		'priority' => 16
	));

	// Slider Transition Time
	$wp_customize->add_setting('himalayas_slider_duration', array(
		'default' => '1',
      'capability' => 'edit_theme_options',
      'sanitize_callback' => 'himalayas_sanitize_integer'
	));
	$wp_customize->add_control('himalayas_slider_duration', array(
		'label' => __( 'Slides duration time in Second', 'himalayas' ),
		'section' => 'himalayas_slider',
		'priority' => 18
	));

	$slider_num = intval( get_theme_mod('himalayas_slider_number', 4) );

   // Slider Page Select
   for( $i = 1; $i <= $slider_num; $i++ ) {
		$wp_customize->add_setting('himalayas_slide'.$i, array(
			'default' => '',
	      'capability' => 'edit_theme_options',
	      'sanitize_callback' => 'himalayas_sanitize_integer'
		));
		$wp_customize->add_control('himalayas_slide'.$i, array(
			'label' => __( 'Slider: ', 'himalayas' ).$i,
			'section' => 'himalayas_slider',
			'type'	=> 'dropdown-pages',
			'priority'  => $i*20+1
		));
	}
	// End of the Slider Options

 /**************************************************************************************/

	/* Start of the Design Options */
   $wp_customize->add_panel('himalayas_design_options', array(
   	'priority'  => 320,
      'capabitity' => 'edit_theme_options',
      'description' => __('Contain all the Design related options', 'himalayas'),
      'title' => __('Himalayas Design Options', 'himalayas')
   ));

   class HIMALAYAS_Image_Radio_Control extends WP_Customize_Control {

 		public function render_content() {

			if ( empty( $this->choices ) )
				return;

			$name = '_customize-radio-' . $this->id;

			?>
			<style>
				#himalayas-img-container .himalayas-radio-img-img {
					border: 3px solid #DEDEDE;
					margin: 0 5px 5px 0;
					cursor: pointer;
					border-radius: 3px;
					-moz-border-radius: 3px;
					-webkit-border-radius: 3px;
				}
				#himalayas-img-container .himalayas-radio-img-selected {
					border: 3px solid #AAA;
					border-radius: 3px;
					-moz-border-radius: 3px;
					-webkit-border-radius: 3px;
				}
				input[type=checkbox]:before {
					content: '';
					margin: -3px 0 0 -4px;
				}
			</style>
			<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			<ul class="controls" id = 'himalayas-img-container'>
			<?php
				foreach ( $this->choices as $value => $label ) :
					$class = ($this->value() == $value)?'himalayas-radio-img-selected himalayas-radio-img-img':'himalayas-radio-img-img';
					?>
					<li style="display: inline;">
					<label>
						<input <?php $this->link(); ?>style = 'display:none' type="radio" value="<?php echo esc_attr( $value ); ?>" name="<?php echo esc_attr( $name ); ?>" <?php $this->link(); checked( $this->value(), $value ); ?> />
						<img src = '<?php echo esc_html( $label ); ?>' class = '<?php echo $class; ?>' />
					</label>
					</li>
					<?php
				endforeach;
			?>
			</ul>
			<script type="text/javascript">

				jQuery(document).ready(function($) {
					$('.controls#himalayas-img-container li img').click(function(){
						$('.controls#himalayas-img-container li').each(function(){
							$(this).find('img').removeClass ('himalayas-radio-img-selected') ;
						});
						$(this).addClass ('himalayas-radio-img-selected') ;
					});
				});

			</script>
			<?php
		}
	}

	// Default Layout Setting
	$wp_customize->add_section('himalayas_default_layout_setting', array(
		'priority' => 10,
		'title' => __('Default layout', 'himalayas'),
		'panel'=> 'himalayas_design_options'
	));

	$wp_customize->add_setting('himalayas_default_layout', array(
		'default' => 'right_sidebar',
      'capability' => 'edit_theme_options',
		'sanitize_callback' => 'himalayas_radio_sanitize'
	));
	$wp_customize->add_control(
		new HIMALAYAS_Image_Radio_Control($wp_customize, 'himalayas_default_layout', array(
			'type' => 'radio',
			'label' => __('Select default layout. This layout will be reflected in whole site archives, categories, search page etc. The layout for a single post and page can be controlled from below options', 'himalayas'),
			'section' => 'himalayas_default_layout_setting',
			'settings' => 'himalayas_default_layout',
			'choices' => array(
				'right_sidebar' => HIMALAYAS_ADMIN_IMAGES_URL . '/right-sidebar.png',
				'left_sidebar' => HIMALAYAS_ADMIN_IMAGES_URL . '/left-sidebar.png',
				'no_sidebar_full_width'	=> HIMALAYAS_ADMIN_IMAGES_URL . '/no-sidebar-full-width-layout.png',
				'no_sidebar_content_centered'	=> HIMALAYAS_ADMIN_IMAGES_URL . '/no-sidebar-content-centered-layout.png'
			)
		))
	);

	// Default Layout for Pages
	$wp_customize->add_section('himalayas_default_page_layout_setting', array(
		'priority' => 20,
		'title' => __('Default layout for pages only', 'himalayas'),
		'panel'=> 'himalayas_design_options'
	));

	$wp_customize->add_setting('himalayas_default_page_layout', array(
		'default' => 'right_sidebar',
      'capability' => 'edit_theme_options',
		'sanitize_callback' => 'himalayas_radio_sanitize'
	));
	$wp_customize->add_control(
		new HIMALAYAS_Image_Radio_Control($wp_customize, 'himalayas_default_page_layout', array(
			'type' => 'radio',
			'label' => __('Select default layout for pages. This layout will be reflected in all pages unless unique layout is set for specific page', 'himalayas'),
			'section' => 'himalayas_default_page_layout_setting',
			'settings' => 'himalayas_default_page_layout',
			'choices' => array(
				'right_sidebar' => HIMALAYAS_ADMIN_IMAGES_URL . '/right-sidebar.png',
				'left_sidebar' => HIMALAYAS_ADMIN_IMAGES_URL . '/left-sidebar.png',
				'no_sidebar_full_width'	=> HIMALAYAS_ADMIN_IMAGES_URL . '/no-sidebar-full-width-layout.png',
				'no_sidebar_content_centered'	=> HIMALAYAS_ADMIN_IMAGES_URL . '/no-sidebar-content-centered-layout.png'
			)
		))
	);

	// Default Layout for Single Posts
	$wp_customize->add_section('himalayas_default_single_posts_layout_setting', array(
		'priority' => 30,
		'title' => __('Default layout for single posts only', 'himalayas'),
		'panel'=> 'himalayas_design_options'
	));

	$wp_customize->add_setting('himalayas_default_single_posts_layout', array(
		'default' => 'right_sidebar',
      'capability' => 'edit_theme_options',
		'sanitize_callback' => 'himalayas_radio_sanitize'
	));
	$wp_customize->add_control(
		new HIMALAYAS_Image_Radio_Control($wp_customize, 'himalayas_default_single_posts_layout', array(
			'type' => 'radio',
			'label' => __('Select default layout for single posts. This layout will be reflected in all single posts unless unique layout is set for specific post', 'himalayas'),
			'section' => 'himalayas_default_single_posts_layout_setting',
			'settings' => 'himalayas_default_single_posts_layout',
			'choices' => array(
				'right_sidebar' => HIMALAYAS_ADMIN_IMAGES_URL . '/right-sidebar.png',
				'left_sidebar' => HIMALAYAS_ADMIN_IMAGES_URL . '/left-sidebar.png',
				'no_sidebar_full_width'	=> HIMALAYAS_ADMIN_IMAGES_URL . '/no-sidebar-full-width-layout.png',
				'no_sidebar_content_centered'	=> HIMALAYAS_ADMIN_IMAGES_URL . '/no-sidebar-content-centered-layout.png'
			)
		))
	);

	/* Display the WooCommerce Options only if WooCommerce plugin is active. */
	if ( class_exists('woocommerce') ) {
		// WooCommerce Shop Page Layout
		$wp_customize->add_section('himalayas_woo_shop_layout_setting', array(
			'priority' => 40,
			'title' => __('WooCommerce Shop Page Layout', 'himalayas'),
			'panel'=> 'himalayas_design_options'
		));

		$wp_customize->add_setting('himalayas_woo_shop_layout', array(
			'default' => 'no_sidebar_full_width',
	      'capability' => 'edit_theme_options',
			'sanitize_callback' => 'himalayas_radio_sanitize'
		));
		$wp_customize->add_control(
			new HIMALAYAS_Image_Radio_Control($wp_customize, 'himalayas_woo_shop_layout', array(
				'type' => 'radio',
				'label' => __('Select Layout for WooCommerce Shop page.', 'himalayas'),
				'section' => 'himalayas_woo_shop_layout_setting',
				'settings' => 'himalayas_woo_shop_layout',
				'choices' => array(
					'right_sidebar' => HIMALAYAS_ADMIN_IMAGES_URL . '/right-sidebar.png',
					'left_sidebar' => HIMALAYAS_ADMIN_IMAGES_URL . '/left-sidebar.png',
					'no_sidebar_full_width'	=> HIMALAYAS_ADMIN_IMAGES_URL . '/no-sidebar-full-width-layout.png',
					'no_sidebar_content_centered'	=> HIMALAYAS_ADMIN_IMAGES_URL . '/no-sidebar-content-centered-layout.png'
				)
			))
		);

		// WooCommerce Product Page Layout
		$wp_customize->add_section('himalayas_woo_product_layout_setting', array(
			'priority' => 50,
			'title' => __('WooCommerce Product Page Layout', 'himalayas'),
			'panel'=> 'himalayas_design_options'
		));

		$wp_customize->add_setting('himalayas_woo_product_layout', array(
			'default' => 'no_sidebar_full_width',
	      'capability' => 'edit_theme_options',
			'sanitize_callback' => 'himalayas_radio_sanitize'
		));
		$wp_customize->add_control(
			new HIMALAYAS_Image_Radio_Control($wp_customize, 'himalayas_woo_product_layout', array(
				'type' => 'radio',
				'label' => __('Select Layout for WooCommerce Product page.', 'himalayas'),
				'section' => 'himalayas_woo_product_layout_setting',
				'settings' => 'himalayas_woo_product_layout',
				'choices' => array(
					'right_sidebar' => HIMALAYAS_ADMIN_IMAGES_URL . '/right-sidebar.png',
					'left_sidebar' => HIMALAYAS_ADMIN_IMAGES_URL . '/left-sidebar.png',
					'no_sidebar_full_width'	=> HIMALAYAS_ADMIN_IMAGES_URL . '/no-sidebar-full-width-layout.png',
					'no_sidebar_content_centered'	=> HIMALAYAS_ADMIN_IMAGES_URL . '/no-sidebar-content-centered-layout.png'
				)
			))
		);
	}

	// Primary Color Options
   $wp_customize->add_section('himalayas_primary_color_setting', array(
      'panel' => 'himalayas_design_options',
      'priority' => 60,
      'title' => __('Primary color option', 'himalayas')
   ));

   $wp_customize->add_setting('himalayas_primary_color', array(
      'default' => '#32c4d1',
      'capability' => 'edit_theme_options',
      'sanitize_callback' => 'himalayas_color_option_hex_sanitize',
      'sanitize_js_callback' => 'himalayas_color_escaping_option_sanitize'
   ));
   $wp_customize->add_control(
   	new WP_Customize_Color_Control($wp_customize, 'himalayas_primary_color', array(
	      'label' => __('This will reflect in links, buttons and many others. Choose a color to match your site', 'himalayas'),
	      'section' => 'himalayas_primary_color_setting',
	      'settings' => 'himalayas_primary_color'
   	))
   );

   // Footer Layout
	$wp_customize->add_section('himalayas_footer_layout_setting', array(
		'priority' => 70,
		'title' => __('Footer Layout Options', 'himalayas'),
		'panel'=> 'himalayas_design_options'
	));

	$wp_customize->add_setting('himalayas_footer_layout', array(
		'default' => 'footer-layout-one',
      'capability' => 'edit_theme_options',
		'sanitize_callback' => 'himalayas_radio_sanitize'
	));
	$wp_customize->add_control('himalayas_footer_layout', array(
		'type' => 'radio',
		'label' => __('Select the Footer Layout', 'himalayas'),
		'section' => 'himalayas_footer_layout_setting',
		'settings' => 'himalayas_footer_layout',
		'choices' => array(
			'footer-layout-one' => __('Choose Footer Layout One', 'himalayas'),
			'footer-layout-two' => __('Choose Footer Layout Two', 'himalayas')
		)
	));

   // Custom CSS setting
   class HIMALAYAS_Custom_CSS_Control extends WP_Customize_Control {

      public $type = 'custom_css';

      public function render_content() {
      ?>
         <label>
            <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
            <textarea rows="5" style="width:100%;" <?php $this->link(); ?>><?php echo esc_textarea( $this->value() ); ?></textarea>
         </label>
      <?php
      }
   }

   $wp_customize->add_section('himalayas_custom_css_setting', array(
      'priority' => 80,
      'title' => __('Custom CSS', 'himalayas'),
      'panel' => 'himalayas_design_options'
   ));

   $wp_customize->add_setting('himalayas_custom_css', array(
      'default' => '',
      'capability' => 'edit_theme_options',
      'sanitize_callback' => 'wp_filter_nohtml_kses',
      'sanitize_js_callback' => 'wp_filter_nohtml_kses'
   ));
   $wp_customize->add_control(
   	new HIMALAYAS_Custom_CSS_Control($wp_customize, 'himalayas_custom_css', array(
	      'label' => __('Write your custom css', 'himalayas'),
	      'section' => 'himalayas_custom_css_setting',
	      'settings' => 'himalayas_custom_css'
   	))
   );
   // End of the Design Options

 /**************************************************************************************/

 	// Start of the Additional Options
   $wp_customize->add_panel('himalayas_additional_options', array(
      'capabitity' => 'edit_theme_options',
      'description' => __('Contain additional options', 'himalayas'),
      'priority' => 330,
      'title' => __('Himalayas Additional Options', 'himalayas')
   ));

   // Front Page setting
	$wp_customize->add_section('himalayas_blog_on_front', array(
		'priority'  => 10,
      'title' => __('Hide Blog posts from the front page', 'himalayas'),
      'panel' => 'himalayas_additional_options'
   ));

	$wp_customize->add_setting('himalayas_hide_blog_front',	array(
		'default' => 0,
      'capability' => 'edit_theme_options',
		'sanitize_callback' => 'himalayas_sanitize_checkbox'
	));
	$wp_customize->add_control('himalayas_hide_blog_front',	array(
		'type' => 'checkbox',
		'label' => __('Check to hide blog posts/static page on front page', 'himalayas' ),
		'section' => 'himalayas_blog_on_front'
	));

   // Excerpts or Full Posts setting
	$wp_customize->add_section('himalayas_content_setting', array(
		'priority' => 20,
		'title' => __('Excerpts or Full Posts option', 'himalayas'),
		'panel'=> 'himalayas_additional_options'
	));

	$wp_customize->add_setting('himalayas_content_show', array(
		'default' => 'show_full_post_content',
      'capability' => 'edit_theme_options',
		'sanitize_callback' => 'himalayas_radio_sanitize'
	));
	$wp_customize->add_control('himalayas_content_show', array(
		'type' => 'radio',
		'label' => __('Toggle between displaying excerpts and full posts on your blog and archives.', 'himalayas'),
		'section' => 'himalayas_content_setting',
		'settings' => 'himalayas_content_show',
		'choices' => array(
			'show_full_post_content' => __('Show full post content', 'himalayas'),
			'show_excerpt' => __('Show excerpt', 'himalayas')
		)
	));

	// Change Read more
   $wp_customize->add_section('himalayas_read_more_setting', array(
      'priority' => 30,
      'title' => __('Change Read more', 'himalayas'),
      'panel' => 'himalayas_additional_options'
   ));

	$wp_customize->add_setting('himalayas_read_more', array(
		'default' => __( 'Read more', 'himalayas' ),
      'capability' => 'edit_theme_options',
      'sanitize_callback' => 'himalayas_text_sanitize'
	));
	$wp_customize->add_control('himalayas_read_more', array(
		'label' => __( 'Replace the default Read more text with your own words.', 'himalayas' ),
		'section' => 'himalayas_read_more_setting'
	));

	// Animate CSS on/off setting
	$wp_customize->add_section('himalayas_animate_on_off_setting', array(
		'priority'  => 40,
      'title' => __('Animate On/Off', 'himalayas'),
      'panel' => 'himalayas_additional_options'
   ));

	$wp_customize->add_setting('himalayas_animate_on',	array(
		'default' => 0,
      'capability' => 'edit_theme_options',
		'sanitize_callback' => 'himalayas_sanitize_checkbox'
	));
	$wp_customize->add_control('himalayas_animate_on',	array(
		'type' => 'checkbox',
		'label' => __('Check to enable Scroll Animations.', 'himalayas' ),
		'section' => 'himalayas_animate_on_off_setting'
	));

	// Footer Editor section
   class HIMALAYAS_Footer_Control extends WP_Customize_Control {

      public $type = 'footer_control';

      public function render_content() {
      ?>
         <label>
            <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
            <textarea rows="5" style="width:100%;" <?php $this->link(); ?>><?php echo esc_textarea( $this->value() ); ?></textarea>
         </label>
      <?php
      }
   }

   $wp_customize->add_section('himalayas_footer_editor_setting', array(
      'priority' => 50,
      'title' => __('Footer Copyright Editor', 'himalayas'),
      'panel' => 'himalayas_additional_options'
   ));

   $default_footer_value = __( 'Copyright &copy; ', 'himalayas' ).' '.'[the-year] [site-link]. All rights reserved. '.'<br>'.__( 'Theme: Himalayas by ', 'himalayas' ).' '.'[tg-link]. '.__( 'Powered by ', 'himalayas' ).' '.'[wp-link].';

   $wp_customize->add_setting('himalayas_footer_editor', array(
      'default' => $default_footer_value,
      'capability' => 'edit_theme_options',
      'sanitize_callback' => 'himalayas_footer_editor_sanitize'
   ));
   $wp_customize->add_control(
   	new HIMALAYAS_Footer_Control($wp_customize, 'himalayas_footer_editor', array(
	      'label' => __('Edit the Copyright information in your footer. You can also use shortcodes: [the-year] for current year, [site-link] for your site link, [wp-link] for WordPress site link and [tg-link] for ThemeGrill site link.', 'himalayas'),
	      'section' => 'himalayas_footer_editor_setting',
	      'settings'=> 'himalayas_footer_editor'
   	))
   );
	// End of the Additional Options

 /**************************************************************************************/

	// Start of the Typography Option
   $wp_customize->add_panel('himalayas_typography_options', array(
      'priority' => 340,
      'title' => __('Himalayas Typography Options', 'himalayas'),
      'description' => __('Change the Typography Settings from here as you want', 'himalayas'),
      'capability' => 'edit_theme_options'
   ));

   // Google Font Options
   $wp_customize->add_section('himalayas_google_fonts_settings', array(
      'priority' => 1,
      'title' => __('Google Font Options', 'himalayas'),
      'panel' => 'himalayas_typography_options'
   ));

   $himalayas_fonts = array(
      'himalayas_site_title_font' => array(
         'id' => 'himalayas_site_title_font',
         'default' => 'Roboto:300,400,700,900',
         'title'=> __('Site title font. Default: "Roboto"', 'himalayas')
      ),
      'himalayas_site_tagline_font' => array(
         'id' => 'himalayas_site_tagline_font',
         'default' => 'Roboto:300,400,700,900',
         'title'=> __('Site tagline font. Default: "Roboto"', 'himalayas')
      ),
      'himalayas_primary_menu_font' => array(
         'id' => 'himalayas_primary_menu_font',
         'default' => 'Roboto:300,400,700,900',
         'title'=> __('Primary menu font. Default: "Roboto"', 'himalayas')
      ),
      'himalayas_widget_titles_font' => array(
         'id' => 'himalayas_widget_titles_font',
         'default' => 'Crimson Text:700',
         'title'=> __('Widget Titles font. Default: "Crimson Text"', 'himalayas')
      ),
      'himalayas_other_titles_font' => array(
         'id' => 'himalayas_other_titles_font',
         'default' => 'Roboto:300,400,700,900',
         'title'=> __('Other Titles font. Default: "Roboto"', 'himalayas')
      ),
      'himalayas_content_font' => array(
         'id' => 'himalayas_content_font',
         'default' => 'Roboto:300,400,700,900',
         'title'=> __('Content font and for others. Default: "Roboto"', 'himalayas')
      )
   );

   foreach ($himalayas_fonts as $himalayas_font) {

      $wp_customize->add_setting($himalayas_font['id'], array(
         'default' => $himalayas_font['default'],
         'capability' => 'edit_theme_options',
         'sanitize_callback' => 'himalayas_font_sanitize'
      ));

      $himalayas_google_fonts = himalayas_google_font_option();

      $wp_customize->add_control($himalayas_font['id'], array(
         'label' => $himalayas_font['title'],
         'type' => 'select',
         'settings' => $himalayas_font['id'],
         'section' => 'himalayas_google_fonts_settings',
         'choices' => $himalayas_google_fonts
      ));
   }

   // Font Size Option
	$himalayas_fonts_size_options = himalayas_font_size_func();

   foreach ($himalayas_fonts_size_options as $himalayas_section) {

      $wp_customize->add_section($himalayas_section['section_id'], array(
	      'title' => $himalayas_section['title'],
	      'panel' => 'himalayas_typography_options'
      ));

      $himalayas_section_id = $himalayas_section['section_id'];
      $himalayas_font_size_setting = $himalayas_section['himalayas_font_size_setting'];

	   foreach ($himalayas_font_size_setting as $himalayas_font_setting) {

	      $wp_customize->add_setting($himalayas_font_setting['id'], array(
            'default' => $himalayas_font_setting['default'],
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'himalayas_radio_sanitize'
         ));

	      $wp_customize->add_control($himalayas_font_setting['id'], array(
            'label' => $himalayas_font_setting['label'],
            'type' => 'select',
            'settings' => $himalayas_font_setting['id'],
            'section' => $himalayas_section_id,
            'choices' => $himalayas_font_setting['choice']
         ));
	   }
	}
   // End of the Typography Options

 /**************************************************************************************/

   /* Start of the Color Options */
	$wp_customize->add_panel('himalayas_color_options', array(
      'priority' => 350,
      'title' => __('Himalayas Color Options', 'himalayas'),
      'description' => __('Make you site Colorful', 'himalayas'),
      'capability' => 'edit_theme_options'
   ));

	$himalayas_colors_options = himalayas_color_func();

   foreach ($himalayas_colors_options as $himalayas_color_section) {

      $wp_customize->add_section($himalayas_color_section['section_id'], array(
	      'title' => $himalayas_color_section['title'],
	      'panel' => 'himalayas_color_options'
      ));

      $himalayas_color_section_id = $himalayas_color_section['section_id'];
      $himalayas_color_settings = $himalayas_color_section['himalayas_color_settings'];

	   foreach ($himalayas_color_settings as $himalayas_color_setting) {

	      $wp_customize->add_setting($himalayas_color_setting['id'], array(
            'default' => $himalayas_color_setting['default'],
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'himalayas_color_option_hex_sanitize',
            'sanitize_js_callback' => 'himalayas_color_escaping_option_sanitize'
         ));

	      $wp_customize->add_control(
	      	new WP_Customize_Color_Control($wp_customize, $himalayas_color_setting['id'], array(
	            'label' => $himalayas_color_setting['label'],
	            'settings' => $himalayas_color_setting['id'],
	            'section' => $himalayas_color_section_id
	         ))
	      );
	   }
	}
   // End of the Color Options

 /**************************************************************************************/

   // Theme important links started
   class HIMALAYAS_Important_Links extends WP_Customize_Control {

      public $type = "himalayas-important-links";

      public function render_content() {
         //Add Theme instruction, Support Forum, Demo Link, Rating Link
         $important_links = array(
            'documentation' => array(
               'link' => esc_url('http://themegrill.com/theme-instruction/himalayas-pro/'),
               'text' => __('Documentation', 'himalayas')
            ),
            'support' => array(
               'link' => esc_url('http://themegrill.com/support-forum/'),
               'text' => __('Support', 'himalayas')
            ),
            'demo' => array(
               'link' => esc_url('http://demo.themegrill.com/himalayas-pro/'),
               'text' => __('View Demo', 'himalayas')
            ),
            'rating' => array(
               'link' => esc_url('https://wordpress.org/support/view/theme-reviews/himalayas'),
               'text' => __('Rate This Theme', 'himalayas')
            )
         );
         foreach ($important_links as $important_link) {
            echo '<p><a target="_blank" href="' . $important_link['link'] . '" >' . esc_attr($important_link['text']) . ' </a></p>';
         }
      }

   }

   $wp_customize->add_section('himalayas_important_links', array(
      'priority' => 360,
      'title' => __('Himalayas Theme Important Links', 'himalayas'),
   ));

   /**
    * This setting has the dummy Sanitizaition function as it contains no value to be sanitized
    */
   $wp_customize->add_setting('himalayas_important_links', array(
      'capability' => 'edit_theme_options',
      'sanitize_callback' => 'himalayas_links_sanitize'
   ));

   $wp_customize->add_control(
   	new HIMALAYAS_Important_Links($wp_customize, 'important_links', array(
	      'label' => __('Important Links', 'himalayas'),
	      'section' => 'himalayas_important_links',
	      'settings' => 'himalayas_important_links'
	   ))
	);
   // Theme Important Links Ended

 /**************************************************************************************/

	function himalayas_sanitize_checkbox($input) {
      if ( $input == 1 ) {
         return 1;
      } else {
         return '';
      }
   }
   function himalayas_sanitize_url( $input ) {
		$input = esc_url_raw( $input );
		return $input;
	}
	function himalayas_sanitize_js_url ( $input ) {
		$input = esc_url( $input );
		return $input;
	}
	function himalayas_sanitize_integer( $input ) {
    	if( is_numeric( $input ) ) {
        return intval( $input );
   	}
	}
   // Color sanitization
   function himalayas_color_option_hex_sanitize($color) {
      if ($unhashed = sanitize_hex_color_no_hash($color))
         return '#' . $unhashed;

      return $color;
   }

   function himalayas_color_escaping_option_sanitize($input) {
      $input = esc_attr($input);
      return $input;
   }
   // Radio and Select Sanitization
   function himalayas_radio_sanitize( $input, $setting ) {

		// Ensure input is a slug.
		$input = sanitize_key( $input );

		// Get list of choices from the control associated with the setting.
		$choices = $setting->manager->get_control( $setting->id )->choices;

		// If the input is a valid key, return it; otherwise, return the default.
		return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
	}
	// Google Font Sanitization
   function himalayas_font_sanitize( $input, $setting ) {

		// Get list of choices from the control associated with the setting.
		$choices = $setting->manager->get_control( $setting->id )->choices;

		// If the input is a valid key, return it; otherwise, return the default.
		return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
	}
	// Text sanitization
	function himalayas_text_sanitize( $input ) {
	    return wp_kses_post( force_balance_tags( $input ) );
	}
	// Footer section sanitization
   function himalayas_footer_editor_sanitize( $input) {
      if( isset( $input ) ) {
         $input =  stripslashes( wp_filter_post_kses( addslashes ( $input ) ) );
      }
      return $input;
   }
   // Sanitization of links
   function himalayas_links_sanitize() {
      return false;
   }
}
add_action('customize_register', 'himalayas_customize_register');

/**************************************************************************************/

if ( ! function_exists( 'himalayas_font_size_range_generator' ) ) :
/**
 * Function to generate font size range for font size options.
 */
function himalayas_font_size_range_generator( $start_range, $end_range ) {
   $range_string = array();
   for( $i = $start_range; $i <= $end_range; $i++ ) {
      $range_string[$i] = $i;
   }
   return $range_string;
}
endif;

/**************************************************************************************/

if ( ! function_exists( 'himalayas_font_size_func' ) ) :
/**
 * Function that contain Font Size customze setting
 */
function himalayas_font_size_func() {
	$himalayas_fonts_size_options = array(
      'himalayas_header_font_size_setting' => array(
         'section_id' => 'himalayas_header_font_size_setting',
         'title'=> __('Header font size Options', 'himalayas'),
         'himalayas_font_size_setting' => array(
	      	'himalayas_site_title_font_size' => array(
		         'id' => 'himalayas_site_title_font_size',
		         'default' => '23',
		         'label'=> __('Site title font size. Default: 23', 'himalayas'),
		         'choice' => himalayas_font_size_range_generator( 15, 50 ),
		         'custom_css' => ' #site-title a'
			   ),
		      'himalayas_site_tagline_font_size' => array(
		         'id' => 'himalayas_site_tagline_font_size',
		         'default' => '14',
		         'label'=> __('Site tagline font size. Default: 14', 'himalayas'),
		         'choice' => himalayas_font_size_range_generator( 8, 40 ),
		         'custom_css' => ' .header-wrapper #site-description'
		   	),
		      'himalayas_primary_menu_font_size' => array(
		         'id' => 'himalayas_primary_menu_font_size',
		         'default' => '15',
		         'label'=> __('Primary menu font size. Default: 15', 'himalayas'),
		         'choice' => himalayas_font_size_range_generator( 8, 40 ),
		         'custom_css' => ' #site-navigation .menu li a'
			   )
   		)
   	),
      'himalayas_title_font_size_setting' => array(
         'section_id' => 'himalayas_title_font_size_setting',
         'title'=> __('Titles related font size option', 'himalayas'),
		   'himalayas_font_size_setting' => array(
		      'himalayas_heading_h1_font_size' => array(
		         'id' => 'himalayas_heading_h1_font_size',
		         'default' => '36',
		         'label'=> __('Heading H1 tag. Default: 36', 'himalayas'),
		         'choice' => himalayas_font_size_range_generator( 20, 50 ),
		         'custom_css' => 'h1'
		      ),
		      'himalayas_heading_h2_font_size' => array(
		         'id' => 'himalayas_heading_h2_font_size',
		         'default' => '30',
		         'label'=> __('Heading H2 tag. Default: 30', 'himalayas'),
		         'choice' => himalayas_font_size_range_generator( 18, 44 ),
		         'custom_css' => 'h2'
		      ),
		      'himalayas_heading_h3_font_size' => array(
		         'id' => 'himalayas_heading_h3_font_size',
		         'default' => '28',
		         'label'=> __('Heading H3 tag. Default: 28', 'himalayas'),
		         'choice' => himalayas_font_size_range_generator( 15, 42 ),
		         'custom_css' => 'h3'
		      ),
		      'himalayas_heading_h4_font_size' => array(
		         'id' => 'himalayas_heading_h4_font_size',
		         'default' => '20',
		         'label'=> __('Heading H4 tag. Default: 20', 'himalayas'),
		         'choice' => himalayas_font_size_range_generator( 10, 34 ),
		         'custom_css' => 'h4'
		      ),
		      'himalayas_heading_h5_font_size' => array(
		         'id' => 'himalayas_heading_h5_font_size',
		         'default' => '18',
		         'label'=> __('Heading H5 tag. Default: 18', 'himalayas'),
		         'choice' => himalayas_font_size_range_generator( 10, 32 ),
		         'custom_css' => 'h5'
		      ),
		      'himalayas_heading_h6_font_size' => array(
		         'id' => 'himalayas_heading_h6_font_size',
		         'default' => '16',
		         'label'=> __('Heading H6 tag. Default: 16', 'himalayas'),
		         'choice' => himalayas_font_size_range_generator( 8, 30 ),
		         'custom_css' => 'h6'
		      ),
		      'himalayas_comment_title_font_size' => array(
		         'id' => 'himalayas_comment_title_font_size',
		         'default' => '24',
		         'label'=> __('Comment Title. Default: 24', 'himalayas'),
		         'choice' => himalayas_font_size_range_generator( 10, 35 ),
		         'custom_css' => '#site-title a'
		      )
		   )
      ),
		'himalayas_content_font_size_setting' => array(
         'section_id' => 'himalayas_content_font_size_setting',
         'title'=> __('Content Font Size Option', 'himalayas'),
		   'himalayas_font_size_setting' => array(
		   	'himalayas_content_fonts_size' => array(
		         'id' => 'himalayas_content_fonts_size',
		         'default' => '15',
		         'label'=> __('Content font size, also applies to other text like in search fields, post comment button etc. Default: 15', 'himalayas'),
		         'choice' => himalayas_font_size_range_generator( 8, 30 ),
		         'custom_css' => 'body, button, input, select, textarea'
		      ),
		      'himalayas_post_meta_font_size' => array(
		         'id' => 'himalayas_post_meta_font_size',
		         'default' => '12',
		         'label'=> __('Post meta font size. Default: 12', 'himalayas'),
		         'choice' => himalayas_font_size_range_generator( 5, 25 ),
		         'custom_css' => '.entry-meta a,.single .byline, .group-blog .byline, .posted-on, .blog-author, .blog-cat,.entry-meta > span::before'
		      ),
		      'himalayas_button_text_font_size' => array(
		         'id' => 'himalayas_button_text_font_size',
		         'default' => '14',
		         'label'=> __('Button text font size (Buttons like Read more, submit, post comment etc). Default: 14', 'himalayas'),
		         'choice' => himalayas_font_size_range_generator( 10, 30 ),
		         'custom_css' => '.contact-form-wrapper input[type="submit"],.navigation .nav-links a, .bttn, button, input[type="button"], input[type="reset"], input[type="submit"]'
		      )
   		)
   	),
		'himalayas_footer_font_size_setting' => array(
         'section_id' => 'himalayas_footer_font_size_setting',
         'title'=> __('Footer Size Option', 'himalayas'),
		   'himalayas_font_size_setting' => array(
		   	'himalayas_footer_widget_title_fonts_size' => array(
		         'id' => 'himalayas_footer_widget_title_fonts_size',
		         'default' => '20',
		         'label'=> __('Footer widget Titles. Default: 20', 'himalayas'),
		         'choice' => himalayas_font_size_range_generator( 10, 35 ),
		         'custom_css' => '#top-footer .widget-title'
		      ),
		      'himalayas_footer_widget_content_font_size' => array(
		         'id' => 'himalayas_footer_widget_content_font_size',
		         'default' => '15',
		         'label'=> __('Footer widget Content. Default: 15', 'himalayas'),
		         'choice' => himalayas_font_size_range_generator( 10, 30 ),
		         'custom_css' => '#top-footer'
		      ),
		      'himalayas_footer_copyright_textt_font_size' => array(
		         'id' => 'himalayas_footer_copyright_textt_font_size',
		         'default' => '12',
		         'label'=> __('Footer Copyright Text. Default: 12', 'himalayas'),
		         'choice' => himalayas_font_size_range_generator( 5, 25 ),
		         'custom_css' => '#bottom-footer .copyright'
		      )
		   )
		)
	);
	return $himalayas_fonts_size_options;
}
endif;

/**************************************************************************************/

if ( ! function_exists( 'himalayas_color_func' ) ) :
/**
 * Function that contain Color customze setting
 */
function himalayas_color_func() {
	$himalayas_color_options = array(
      'himalayas_header_color_settings' => array(
         'section_id' => 'himalayas_header_color_settings',
         'title'=> __('Header Color Options', 'himalayas'),
         'himalayas_color_settings' => array(
	      	'himalayas_site_title_color' => array(
		         'id' => 'himalayas_site_title_color',
		         'default' => '#333333',
		         'label'=> __('Site Title. Default: #333333', 'himalayas'),
		         'color_custom_css' => ' #site-title a'
			   ),
		      'himalayas_site_tagline_color' => array(
		         'id' => 'himalayas_site_tagline_color',
		         'default' => '#333333',
		         'label'=> __('Site Tagline. Default: #333333', 'himalayas'),
		         'color_custom_css' => ' .header-wrapper #site-description'
			   ),
		      'himalayas_primary_menu_text_color' => array(
		         'id' => 'himalayas_primary_menu_text_color',
		         'default' => '#ffffff',
		         'label'=> __('Primary menu text color. Default: #ffffff', 'himalayas'),
		         'color_custom_css' => ' #site-navigation .menu li a'
			   ),
			   'himalayas_primary_menu_selected_hovered_text_color' => array(
		         'id' => 'himalayas_primary_menu_selected_hovered_text_color',
		         'default' => '#32c4d1',
		         'label'=> __('Primary menu selected/hovered item color. Default: #32c4d1', 'himalayas'),
		         'color_custom_css' => '.service-read-more:hover,.cta-text-btn:hover,.blog-readmore:hover, #site-navigation .menu li:hover a, #site-navigation .menu li.current-one-page-item a, .header-wrapper.stick #site-navigation .menu li:hover a, .header-wrapper.stick #site-navigation .menu li.current-one-page-item a, .header-wrapper.no-slider #site-navigation .menu li:hover a, .header-wrapper.no-slider #site-navigation .menu li.current-one-page-item a'
			   ),
			   'himalayas_header_background_color' => array(
		         'id' => 'himalayas_header_background_color',
		         'default' => '#333333',
		         'label'=> __('Header background color. Default: #333333', 'himalayas'),
		         'color_custom_css' => ' .header-wrapper.non-transparent, .header-wrapper.transparent, .header-wrapper.non-stick,.home .header-wrapper.transparent.stick',
		         'color_location' => 'background'
			   )
   		)
   	),
		'himalayas_content_part_color_settings' => array(
         'section_id' => 'himalayas_content_part_color_settings',
         'title'=> __('Content part color options', 'himalayas'),
         'himalayas_color_settings' => array(
	      	'himalayas_post_title_color' => array(
		         'id' => 'himalayas_post_title_color',
		         'default' => '#333333',
		         'label'=> __('Posts title color. Default: #333333', 'himalayas'),
		         'color_custom_css' => ' .entry-title a,.single-post .entry-title'
			   ),
			   'himalayas_page_title_color' => array(
		         'id' => 'himalayas_page_title_color',
		         'default' => '#333333',
		         'label'=> __('Page title color. Default: #333333', 'himalayas'),
		         'color_custom_css' => ' .entry-title'
			   ),
			   'himalayas_content_text_color' => array(
		         'id' => 'himalayas_content_text_color',
		         'default' => '#333333',
		         'label'=> __('Content text color. Default: #333333', 'himalayas'),
		         'color_custom_css' => ' body, button, input, select, textarea,.about-content, .contact-content,.service-content'
			   ),
			   'himalayas_post_meta_color' => array(
		         'id' => 'himalayas_post_meta_color',
		         'default' => '#333333',
		         'label'=> __('Post meta color. Default: #333333', 'himalayas'),
		         'color_custom_css' => ' .entry-meta a,.entry-meta > span::before'
			   ),
			   'himalayas_button_text_color' => array(
		         'id' => 'himalayas_button_text_color',
		         'default' => '#ffffff',
		         'label'=> __('Button text color. Default: #ffffff', 'himalayas'),
		         'color_custom_css' => ' .contact-form-wrapper input[type="submit"], .navigation .nav-links a, .bttn, button, input[type="button"], input[type="reset"], input[type="submit"]'
			   ),
			   'himalayas_button_background_color' => array(
		         'id' => 'himalayas_button_background_color',
		         'default' => '#333333',
		         'label'=> __('Button background color. Default: #333333', 'himalayas'),
		         'color_custom_css' => ' .navigation .nav-links a, .bttn, button, input[type="button"], input[type="reset"], input[type="submit"],.about-btn a,.blog-view, .port-link a:hover,.contact-form-wrapper input[type="submit"], .default-wp-page a:hover',
		         'color_location' => 'background'
			   ),
			   'himalayas_sidebar_widget_title_color' => array(
		         'id' => 'himalayas_sidebar_widget_title_color',
		         'default' => '#333333',
		         'label'=> __('Sidebar widget title color. Default: #333333', 'himalayas'),
		         'color_custom_css' => ' #secondary .widget-title'
			   )
			)
		),
		'himalayas_footer_part_color_settings' => array(
         'section_id' => 'himalayas_footer_part_color_settings',
         'title'=> __('Footer part color options', 'himalayas'),
         'himalayas_color_settings' => array(
	      	'himalayas_footer_widget_title_color' => array(
		         'id' => 'himalayas_footer_widget_title_color',
		         'default' => '#ffffff',
		         'label'=> __('Footer Widget title color. Default: #ffffff', 'himalayas'),
		         'color_custom_css' => ' #top-footer .widget-title'
			   ),
			   'himalayas_footer_widget_content_color' => array(
		         'id' => 'himalayas_footer_widget_content_color',
		         'default' => '#333333',
		         'label'=> __('Footer widget content color. Default: #333333', 'himalayas'),
		         'color_custom_css' => ' #top-footer'
			   ),
			   'himalayas_footer_widget_content_link_text_color' => array(
		         'id' => 'himalayas_footer_widget_content_link_text_color',
		         'default' => '#333333',
		         'label'=> __('Footer widget content link text color. Default: #333333', 'himalayas'),
		         'color_custom_css' => ' #top-footer a'
			   ),
			   'himalayas_footer_widget_background_color' => array(
		         'id' => 'himalayas_footer_widget_background_color',
		         'default' => '#333333',
		         'label'=> __('Footer widget background color. Default: #333333', 'himalayas'),
		         'color_custom_css' => ' #top-footer',
		         'color_location' => 'background'
			   ),
			   'himalayas_footer_copyright_text_color' => array(
		         'id' => 'himalayas_footer_copyright_text_color',
		         'default' => '#ffffff',
		         'label'=> __('Footer copyright text color. Default: #ffffff', 'himalayas'),
		         'color_custom_css' => ' .copyright'
			   ),
			   'himalayas_footer_copyright_link_text_color' => array(
		         'id' => 'himalayas_footer_copyright_link_text_color',
		         'default' => '#32c4d1',
		         'label'=> __('Footer copyright link text color. Default: #32c4d1', 'himalayas'),
		         'color_custom_css' => ' .copyright a'
			   ),
			   'himalayas_footer_copyright_part_background_color' => array(
		         'id' => 'himalayas_footer_copyright_part_background_color',
		         'default' => '#2c2c2c',
		         'label'=> __('Footer copyright part background color. Default: #2c2c2c', 'himalayas'),
		         'color_custom_css' => ' #bottom-footer',
		         'color_location' => 'background'
			   )
			)
		)
	);
	return $himalayas_color_options;
}
endif;

/**************************************************************************************/

if ( ! function_exists( 'himalayas_google_font_option' ) ) :
/**
 * Google Font addition
 */
function himalayas_google_font_option() {

   $himalayas_google_fonts = array(
      "ABeeZee" => "ABeeZee",
      "Abel" => "Abel",
      "Abril Fatface" => "Abril+Fatface",
      "Aclonica" => "Aclonica",
      "Acme" => "Acme",
      "Actor" => "Actor",
      "Adamina" => "Adamina",
      "Advent Pro" => "Advent+Pro",
      "Aguafina Script" => "Aguafina+Script",
      "Akronim" => "Akronim",
      "Aladin" => "Aladin",
      "Aldrich" => "Aldrich",
      "Alegreya" => "Alegreya",
      "Alegreya SC" => "Alegreya+SC",
      "Alex Brush" => "Alex+Brush",
      "Alfa Slab One" => "Alfa+Slab+One",
      "Alice" => "Alice",
      "Alike" => "Alike",
      "Alike Angular" => "Alike+Angular",
      "Allan" => "Allan",
      "Allerta" => "Allerta",
      "Allerta Stencil" => "Allerta+Stencil",
      "Allura" => "Allura",
      "Almendra" => "Almendra",
      "Almendra Display" => "Almendra+Display",
      "Almendra SC" => "Almendra+SC",
      "Amarante" => "Amarante",
      "Amaranth" => "Amaranth",
      "Amatic SC" => "Amatic+SC",
      "Amethysta" => "Amethysta",
      "Anaheim" => "Anaheim",
      "Andada" => "Andada",
      "Andika" => "Andika",
      "Angkor" => "Angkor",
      "Annie Use Your Telescope" => "Annie+Use+Your+Telescope",
      "Anonymous Pro" => "Anonymous+Pro",
      "Antic" => "Antic",
      "Antic Didone" => "Antic+Didone",
      "Antic Slab" => "Antic+Slab",
      "Anton" => "Anton",
      "Arapey" => "Arapey",
      "Arbutus" => "Arbutus",
      "Arbutus Slab" => "Arbutus+Slab",
      "Architects Daughter" => "Architects+Daughter",
      "Archivo Black" => "Archivo+Black",
      "Archivo Narrow" => "Archivo+Narrow",
      "Arimo" => "Arimo",
      "Arizonia" => "Arizonia",
      "Armata" => "Armata",
      "Artifika" => "Artifika",
      "Arvo" => "Arvo",
      "Asap" => "Asap",
      "Asset" => "Asset",
      "Astloch" => "Astloch",
      "Asul" => "Asul",
      "Atomic Age" => "Atomic+Age",
      "Aubrey" => "Aubrey",
      "Audiowide" => "Audiowide",
      "Autour One" => "Autour+One",
      "Average" => "Average",
      "Average Sans" => "Average+Sans",
      "Averia Gruesa Libre" => "Averia+Gruesa+Libre",
      "Averia Libre" => "Averia+Libre",
      "Averia Sans Libre" => "Averia+Sans+Libre",
      "Averia Serif Libre" => "Averia+Serif+Libre",
      "Bad Script" => "Bad+Script",
      "Balthazar" => "Balthazar",
      "Bangers" => "Bangers",
      "Basic" => "Basic",
      "Battambang" => "Battambang",
      "Baumans" => "Baumans",
      "Bayon" => "Bayon",
      "Belgrano" => "Belgrano",
      "Belleza" => "Belleza",
      "BenchNine" => "BenchNine",
      "Bentham" => "Bentham",
      "Berkshire Swash" => "Berkshire+Swash",
      "Bevan" => "Bevan",
      "Bigelow Rules" => "Bigelow+Rules",
      "Bigshot One" => "Bigshot+One",
      "Bilbo" => "Bilbo",
      "Bilbo Swash Caps" => "Bilbo+Swash+Caps",
      "Bitter" => "Bitter",
      "Black Ops One" => "Black+Ops+One",
      "Bokor" => "Bokor",
      "Bonbon" => "Bonbon",
      "Boogaloo" => "Boogaloo",
      "Bowlby One" => "Bowlby+One",
      "Bowlby One SC" => "Bowlby+One+SC",
      "Brawler" => "Brawler",
      "Bree Serif" => "Bree+Serif",
      "Bubblegum Sans" => "Bubblegum+Sans",
      "Bubbler One" => "Bubbler+One",
      "Buda" => "Buda",
      "Buenard" => "Buenard",
      "Butcherman" => "Butcherman",
      "Butterfly Kids" => "Butterfly+Kids",
      "Cabin" => "Cabin",
      "Cabin Condensed" => "Cabin+Condensed",
      "Cabin Sketch" => "Cabin+Sketch",
      "Caesar Dressing" => "Caesar+Dressing",
      "Cagliostro" => "Cagliostro",
      "Calligraffitti" => "Calligraffitti",
      "Cambo" => "Cambo",
      "Candal" => "Candal",
      "Cantarell" => "Cantarell",
      "Cantata One" => "Cantata+One",
      "Cantora One" => "Cantora+One",
      "Capriola" => "Capriola",
      "Cardo" => "Cardo",
      "Carme" => "Carme",
      "Carrois Gothic" => "Carrois+Gothic",
      "Carrois Gothic SC" => "Carrois+Gothic+SC",
      "Carter One" => "Carter+One",
      "Caudex" => "Caudex",
      "Cedarville Cursive" => "Cedarville+Cursive",
      "Ceviche One" => "Ceviche+One",
      "Changa One" => "Changa+One",
      "Chango" => "Chango",
      "Chau Philomene One" => "Chau+Philomene+One",
      "Chela One" => "Chela+One",
      "Chelsea Market" => "Chelsea+Market",
      "Chenla" => "Chenla",
      "Cherry Cream Soda" => "Cherry+Cream+Soda",
      "Cherry Swash" => "Cherry+Swash",
      "Chewy" => "Chewy",
      "Chicle" => "Chicle",
      "Chivo" => "Chivo",
      "Cinzel" => "Cinzel",
      "Cinzel Decorative" => "Cinzel+Decorative",
      "Clicker Script" => "Clicker+Script",
      "Coda" => "Coda",
      "Coda Caption" => "Coda+Caption",
      "Codystar" => "Codystar",
      "Combo" => "Combo",
      "Comfortaa" => "Comfortaa",
      "Coming Soon" => "Coming+Soon",
      "Concert One" => "Concert+One",
      "Condiment" => "Condiment",
      "Content" => "Content",
      "Contrail One" => "Contrail+One",
      "Convergence" => "Convergence",
      "Cookie" => "Cookie",
      "Copse" => "Copse",
      "Corben" => "Corben",
      "Courgette" => "Courgette",
      "Cousine" => "Cousine",
      "Coustard" => "Coustard",
      "Covered By Your Grace" => "Covered+By+Your+Grace",
      "Crafty Girls" => "Crafty+Girls",
      "Creepster" => "Creepster",
      "Crete Round" => "Crete+Round",
      "Crimson Text:700" => "Crimson+Text",
      "Croissant One" => "Croissant+One",
      "Crushed" => "Crushed",
      "Cuprum" => "Cuprum",
      "Cutive" => "Cutive",
      "Cutive Mono" => "Cutive+Mono",
      "Damion" => "Damion",
      "Dancing Script" => "Dancing+Script",
      "Dangrek" => "Dangrek",
      "Dawning of a New Day" => "Dawning+of+a+New+Day",
      "Days One" => "Days+One",
      "Delius" => "Delius",
      "Delius Swash Caps" => "Delius+Swash+Caps",
      "Delius Unicase" => "Delius+Unicase",
      "Della Respira" => "Della+Respira",
      "Denk One" => "Denk+One",
      "Devonshire" => "Devonshire",
      "Didact Gothic" => "Didact+Gothic",
      "Diplomata" => "Diplomata",
      "Diplomata SC" => "Diplomata+SC",
      "Domine" => "Domine",
      "Donegal One" => "Donegal+One",
      "Doppio One" => "Doppio+One",
      "Dorsa" => "Dorsa",
      "Dosis" => "Dosis",
      "Dr Sugiyama" => "Dr+Sugiyama",
      "Droid Sans" => "Droid+Sans",
      "Droid Sans Mono" => "Droid+Sans+Mono",
      "Droid Serif" => "Droid+Serif",
      "Duru Sans" => "Duru+Sans",
      "Dynalight" => "Dynalight",
      "EB Garamond" => "EB+Garamond",
      "Eagle Lake" => "Eagle+Lake",
      "Eater" => "Eater",
      "Economica" => "Economica",
      "Electrolize" => "Electrolize",
      "Elsie" => "Elsie",
      "Elsie Swash Caps" => "Elsie+Swash+Caps",
      "Emblema One" => "Emblema+One",
      "Emilys Candy" => "Emilys+Candy",
      "Engagement" => "Engagement",
      "Englebert" => "Englebert",
      "Enriqueta" => "Enriqueta",
      "Erica One" => "Erica+One",
      "Esteban" => "Esteban",
      "Euphoria Script" => "Euphoria+Script",
      "Ewert" => "Ewert",
      "Exo" => "Exo",
      "Expletus Sans" => "Expletus+Sans",
      "Fanwood Text" => "Fanwood+Text",
      "Fascinate" => "Fascinate",
      "Fascinate Inline" => "Fascinate+Inline",
      "Faster One" => "Faster+One",
      "Fasthand" => "Fasthand",
      "Federant" => "Federant",
      "Federo" => "Federo",
      "Felipa" => "Felipa",
      "Fenix" => "Fenix",
      "Finger Paint" => "Finger+Paint",
      "Fira Sans" => "Fira+Sans",
      "Fjalla One" => "Fjalla+One",
      "Fjord One" => "Fjord+One",
      "Flamenco" => "Flamenco",
      "Flavors" => "Flavors",
      "Fondamento" => "Fondamento",
      "Fontdiner Swanky" => "Fontdiner+Swanky",
      "Forum" => "Forum",
      "Francois One" => "Francois+One",
      "Freckle Face" => "Freckle+Face",
      "Fredericka the Great" => "Fredericka+the+Great",
      "Fredoka One" => "Fredoka+One",
      "Freehand" => "Freehand",
      "Fresca" => "Fresca",
      "Frijole" => "Frijole",
      "Fruktur" => "Fruktur",
      "Fugaz One" => "Fugaz+One",
      "GFS Didot" => "GFS+Didot",
      "GFS Neohellenic" => "GFS+Neohellenic",
      "Gabriela" => "Gabriela",
      "Gafata" => "Gafata",
      "Galdeano" => "Galdeano",
      "Galindo" => "Galindo",
      "Gentium Basic" => "Gentium+Basic",
      "Gentium Book Basic" => "Gentium+Book+Basic",
      "Geo" => "Geo",
      "Geostar" => "Geostar",
      "Geostar Fill" => "Geostar+Fill",
      "Germania One" => "Germania+One",
      "Gilda Display" => "Gilda+Display",
      "Give You Glory" => "Give+You+Glory",
      "Glass Antiqua" => "Glass+Antiqua",
      "Glegoo" => "Glegoo",
      "Gloria Hallelujah" => "Gloria+Hallelujah",
      "Goblin One" => "Goblin+One",
      "Gochi Hand" => "Gochi+Hand",
      "Gorditas" => "Gorditas",
      "Goudy Bookletter 1911" => "Goudy+Bookletter+1911",
      "Graduate" => "Graduate",
      "Grand Hotel" => "Grand+Hotel",
      "Gravitas One" => "Gravitas+One",
      "Great Vibes" => "Great+Vibes",
      "Griffy" => "Griffy",
      "Gruppo" => "Gruppo",
      "Gudea" => "Gudea",
      "Habibi" => "Habibi",
      "Hammersmith One" => "Hammersmith+One",
      "Hanalei" => "Hanalei",
      "Hanalei Fill" => "Hanalei+Fill",
      "Handlee" => "Handlee",
      "Hanuman" => "Hanuman",
      "Happy Monkey" => "Happy+Monkey",
      "Headland One" => "Headland+One",
      "Henny Penny" => "Henny+Penny",
      "Herr Von Muellerhoff" => "Herr+Von+Muellerhoff",
      "Holtwood One SC" => "Holtwood+One+SC",
      "Homemade Apple" => "Homemade+Apple",
      "Homenaje" => "Homenaje",
      "IM Fell DW Pica" => "IM+Fell+DW+Pica",
      "IM Fell DW Pica SC" => "IM+Fell+DW+Pica+SC",
      "IM Fell Double Pica" => "IM+Fell+Double+Pica",
      "IM Fell Double Pica SC" => "IM+Fell+Double+Pica+SC",
      "IM Fell English" => "IM+Fell+English",
      "IM Fell English SC" => "IM+Fell+English+SC",
      "IM Fell French Canon" => "IM+Fell+French+Canon",
      "IM Fell French Canon SC" => "IM+Fell+French+Canon+SC",
      "IM Fell Great Primer" => "IM+Fell+Great+Primer",
      "IM Fell Great Primer SC" => "IM+Fell+Great+Primer+SC",
      "Iceberg" => "Iceberg",
      "Iceland" => "Iceland",
      "Imprima" => "Imprima",
      "Inconsolata" => "Inconsolata",
      "Inder" => "Inder",
      "Indie Flower" => "Indie+Flower",
      "Inika" => "Inika",
      "Irish Grover" => "Irish+Grover",
      "Istok Web" => "Istok+Web",
      "Italiana" => "Italiana",
      "Italianno" => "Italianno",
      "Jacques Francois" => "Jacques+Francois",
      "Jacques Francois Shadow" => "Jacques+Francois+Shadow",
      "Jim Nightshade" => "Jim+Nightshade",
      "Jockey One" => "Jockey+One",
      "Jolly Lodger" => "Jolly+Lodger",
      "Josefin Sans" => "Josefin+Sans",
      "Josefin Slab" => "Josefin+Slab",
      "Joti One" => "Joti+One",
      "Judson" => "Judson",
      "Julee" => "Julee",
      "Julius Sans One" => "Julius+Sans+One",
      "Junge" => "Junge",
      "Jura" => "Jura",
      "Just Another Hand" => "Just+Another+Hand",
      "Just Me Again Down Here" => "Just+Me+Again+Down+Here",
      "Kameron" => "Kameron",
      "Karla" => "Karla",
      "Kaushan Script" => "Kaushan+Script",
      "Kavoon" => "Kavoon",
      "Keania One" => "Keania+One",
      "Kelly Slab" => "Kelly+Slab",
      "Kenia" => "Kenia",
      "Khmer" => "Khmer",
      "Kite One" => "Kite+One",
      "Knewave" => "Knewave",
      "Kotta One" => "Kotta+One",
      "Koulen" => "Koulen",
      "Kranky" => "Kranky",
      "Kreon" => "Kreon",
      "Kristi" => "Kristi",
      "Krona One" => "Krona+One",
      "La Belle Aurore" => "La+Belle+Aurore",
      "Lancelot" => "Lancelot",
      "Lato" => "Lato",
      "League Script" => "League+Script",
      "Leckerli One" => "Leckerli+One",
      "Ledger" => "Ledger",
      "Lekton" => "Lekton",
      "Lemon" => "Lemon",
      "Libre Baskerville" => "Libre+Baskerville",
      "Life Savers" => "Life+Savers",
      "Lilita One" => "Lilita+One",
      "Limelight" => "Limelight",
      "Linden Hill" => "Linden+Hill",
      "Lobster" => "Lobster",
      "Lobster Two" => "Lobster+Two",
      "Londrina Outline" => "Londrina+Outline",
      "Londrina Shadow" => "Londrina+Shadow",
      "Londrina Sketch" => "Londrina+Sketch",
      "Londrina Solid" => "Londrina+Solid",
      "Lora" => "Lora",
      "Love Ya Like A Sister" => "Love+Ya+Like+A+Sister",
      "Loved by the King" => "Loved+by+the+King",
      "Lovers Quarrel" => "Lovers+Quarrel",
      "Luckiest Guy" => "Luckiest+Guy",
      "Lusitana" => "Lusitana",
      "Lustria" => "Lustria",
      "Macondo" => "Macondo",
      "Macondo Swash Caps" => "Macondo+Swash+Caps",
      "Magra" => "Magra",
      "Maiden Orange" => "Maiden+Orange",
      "Mako" => "Mako",
      "Marcellus" => "Marcellus",
      "Marcellus SC" => "Marcellus+SC",
      "Marck Script" => "Marck+Script",
      "Margarine" => "Margarine",
      "Marko One" => "Marko+One",
      "Marmelad" => "Marmelad",
      "Marvel" => "Marvel",
      "Mate" => "Mate",
      "Mate SC" => "Mate+SC",
      "Maven Pro" => "Maven+Pro",
      "McLaren" => "McLaren",
      "Meddon" => "Meddon",
      "MedievalSharp" => "MedievalSharp",
      "Medula One" => "Medula+One",
      "Megrim" => "Megrim",
      "Meie Script" => "Meie+Script",
      "Merienda" => "Merienda",
      "Merienda One" => "Merienda+One",
      "Merriweather" => "Merriweather",
      "Merriweather Sans" => "Merriweather+Sans",
      "Metal" => "Metal",
      "Metal Mania" => "Metal+Mania",
      "Metamorphous" => "Metamorphous",
      "Metrophobic" => "Metrophobic",
      "Michroma" => "Michroma",
      "Milonga" => "Milonga",
      "Miltonian" => "Miltonian",
      "Miltonian Tattoo" => "Miltonian+Tattoo",
      "Miniver" => "Miniver",
      "Miss Fajardose" => "Miss+Fajardose",
      "Modern Antiqua" => "Modern+Antiqua",
      "Molengo" => "Molengo",
      "Molle" => "Molle",
      "Monda" => "Monda",
      "Monofett" => "Monofett",
      "Monoton" => "Monoton",
      "Monsieur La Doulaise" => "Monsieur+La+Doulaise",
      "Montaga" => "Montaga",
      "Montez" => "Montez",
      "Montserrat" => "Montserrat",
      "Montserrat Alternates" => "Montserrat+Alternates",
      "Montserrat Subrayada" => "Montserrat+Subrayada",
      "Moul" => "Moul",
      "Moulpali" => "Moulpali",
      "Mountains of Christmas" => "Mountains+of+Christmas",
      "Mouse Memoirs" => "Mouse+Memoirs",
      "Mr Bedfort" => "Mr+Bedfort",
      "Mr Dafoe" => "Mr+Dafoe",
      "Mr De Haviland" => "Mr+De+Haviland",
      "Mrs Saint Delafield" => "Mrs+Saint+Delafield",
      "Mrs Sheppards" => "Mrs+Sheppards",
      "Muli" => "Muli",
      "Mystery Quest" => "Mystery+Quest",
      "Neucha" => "Neucha",
      "Neuton" => "Neuton",
      "New Rocker" => "New+Rocker",
      "News Cycle" => "News+Cycle",
      "Niconne" => "Niconne",
      "Nixie One" => "Nixie+One",
      "Nobile" => "Nobile",
      "Nokora" => "Nokora",
      "Norican" => "Norican",
      "Nosifer" => "Nosifer",
      "Nothing You Could Do" => "Nothing+You+Could+Do",
      "Noticia Text" => "Noticia+Text",
      "Nova Cut" => "Nova+Cut",
      "Nova Flat" => "Nova+Flat",
      "Nova Mono" => "Nova+Mono",
      "Nova Oval" => "Nova+Oval",
      "Nova Round" => "Nova+Round",
      "Nova Script" => "Nova+Script",
      "Nova Slim" => "Nova+Slim",
      "Nova Square" => "Nova+Square",
      "Numans" => "Numans",
      "Nunito" => "Nunito",
      "Odor Mean Chey" => "Odor+Mean+Chey",
      "Offside" => "Offside",
      "Old Standard TT" => "Old+Standard+TT",
      "Oldenburg" => "Oldenburg",
      "Oleo Script" => "Oleo+Script",
      "Oleo Script Swash Caps" => "Oleo+Script+Swash+Caps",
      "Open Sans" => "Open+Sans",
      "Open Sans Condensed" => "Open+Sans+Condensed",
      "Oranienbaum" => "Oranienbaum",
      "Orbitron" => "Orbitron",
      "Oregano" => "Oregano",
      "Orienta" => "Orienta",
      "Original Surfer" => "Original+Surfer",
      "Oswald" => "Oswald",
      "Over the Rainbow" => "Over+the+Rainbow",
      "Overlock" => "Overlock",
      "Overlock SC" => "Overlock+SC",
      "Ovo" => "Ovo",
      "Oxygen" => "Oxygen",
      "Oxygen Mono" => "Oxygen+Mono",
      "PT Mono" => "PT+Mono",
      "PT Sans" => "PT+Sans",
      "PT Sans Caption" => "PT+Sans+Caption",
      "PT Sans Narrow" => "PT+Sans+Narrow",
      "PT Serif" => "PT+Serif",
      "PT Serif Caption" => "PT+Serif+Caption",
      "Pacifico" => "Pacifico",
      "Paprika" => "Paprika",
      "Parisienne" => "Parisienne",
      "Passero One" => "Passero+One",
      "Passion One" => "Passion+One",
      "Patrick Hand" => "Patrick+Hand",
      "Patrick Hand SC" => "Patrick+Hand+SC",
      "Patua One" => "Patua+One",
      "Paytone One" => "Paytone+One",
      "Peralta" => "Peralta",
      "Permanent Marker" => "Permanent+Marker",
      "Petit Formal Script" => "Petit+Formal+Script",
      "Petrona" => "Petrona",
      "Philosopher" => "Philosopher",
      "Piedra" => "Piedra",
      "Pinyon Script" => "Pinyon+Script",
      "Pirata One" => "Pirata+One",
      "Plaster" => "Plaster",
      "Play" => "Play",
      "Playball" => "Playball",
      "Playfair Display" => "Playfair+Display",
      "Playfair Display SC" => "Playfair+Display+SC",
      "Podkova" => "Podkova",
      "Poiret One" => "Poiret+One",
      "Poller One" => "Poller+One",
      "Poly" => "Poly",
      "Pompiere" => "Pompiere",
      "Pontano Sans" => "Pontano+Sans",
      "Port Lligat Sans" => "Port+Lligat+Sans",
      "Port Lligat Slab" => "Port+Lligat+Slab",
      "Prata" => "Prata",
      "Preahvihear" => "Preahvihear",
      "Press Start 2P" => "Press+Start+2P",
      "Princess Sofia" => "Princess+Sofia",
      "Prociono" => "Prociono",
      "Prosto One" => "Prosto+One",
      "Puritan" => "Puritan",
      "Purple Purse" => "Purple+Purse",
      "Quando" => "Quando",
      "Quantico" => "Quantico",
      "Quattrocento" => "Quattrocento",
      "Quattrocento Sans" => "Quattrocento+Sans",
      "Questrial" => "Questrial",
      "Quicksand" => "Quicksand",
      "Quintessential" => "Quintessential",
      "Qwigley" => "Qwigley",
      "Racing Sans One" => "Racing+Sans+One",
      "Radley" => "Radley",
      "Raleway" => "Raleway",
      "Raleway Dots" => "Raleway+Dots",
      "Rambla" => "Rambla",
      "Rammetto One" => "Rammetto+One",
      "Ranchers" => "Ranchers",
      "Rancho" => "Rancho",
      "Rationale" => "Rationale",
      "Redressed" => "Redressed",
      "Reenie Beanie" => "Reenie+Beanie",
      "Revalia" => "Revalia",
      "Ribeye" => "Ribeye",
      "Ribeye Marrow" => "Ribeye+Marrow",
      "Righteous" => "Righteous",
      "Risque" => "Risque",
      "Roboto:300,400,700,900" => "Roboto",
      "Roboto+Slab:" => "Roboto+Slab",
      "Roboto Condensed" => "Roboto+Condensed",
      "Rochester" => "Rochester",
      "Rock Salt" => "Rock+Salt",
      "Rokkitt" => "Rokkitt",
      "Romanesco" => "Romanesco",
      "Ropa Sans" => "Ropa+Sans",
      "Rosario" => "Rosario",
      "Rosarivo" => "Rosarivo",
      "Rouge Script" => "Rouge+Script",
      "Ruda" => "Ruda",
      "Rufina" => "Rufina",
      "Ruge Boogie" => "Ruge+Boogie",
      "Ruluko" => "Ruluko",
      "Rum Raisin" => "Rum+Raisin",
      "Ruslan Display" => "Ruslan+Display",
      "Russo One" => "Russo+One",
      "Ruthie" => "Ruthie",
      "Rye" => "Rye",
      "Sacramento" => "Sacramento",
      "Sail" => "Sail",
      "Salsa" => "Salsa",
      "Sanchez" => "Sanchez",
      "Sancreek" => "Sancreek",
      "Sansita One" => "Sansita+One",
      "Sarina" => "Sarina",
      "Satisfy" => "Satisfy",
      "Scada" => "Scada",
      "Schoolbell" => "Schoolbell",
      "Seaweed Script" => "Seaweed+Script",
      "Sevillana" => "Sevillana",
      "Seymour One" => "Seymour+One",
      "Shadows Into Light" => "Shadows+Into+Light",
      "Shadows Into Light Two" => "Shadows+Into+Light+Two",
      "Shanti" => "Shanti",
      "Share" => "Share",
      "Share Tech" => "Share+Tech",
      "Share Tech Mono" => "Share+Tech+Mono",
      "Shojumaru" => "Shojumaru",
      "Short Stack" => "Short+Stack",
      "Siemreap" => "Siemreap",
      "Sigmar One" => "Sigmar+One",
      "Signika" => "Signika",
      "Signika Negative" => "Signika+Negative",
      "Simonetta" => "Simonetta",
      "Sintony" => "Sintony",
      "Sirin Stencil" => "Sirin+Stencil",
      "Six Caps" => "Six+Caps",
      "Skranji" => "Skranji",
      "Slackey" => "Slackey",
      "Smokum" => "Smokum",
      "Smythe" => "Smythe",
      "Sniglet" => "Sniglet",
      "Snippet" => "Snippet",
      "Snowburst One" => "Snowburst+One",
      "Sofadi One" => "Sofadi+One",
      "Sofia" => "Sofia",
      "Sonsie One" => "Sonsie+One",
      "Sorts Mill Goudy" => "Sorts+Mill+Goudy",
      "Source Code Pro" => "Source+Code+Pro",
      "Source Sans Pro" => "Source+Sans+Pro",
      "Special Elite" => "Special+Elite",
      "Spicy Rice" => "Spicy+Rice",
      "Spinnaker" => "Spinnaker",
      "Spirax" => "Spirax",
      "Squada One" => "Squada+One",
      "Stalemate" => "Stalemate",
      "Stalinist One" => "Stalinist+One",
      "Stardos Stencil" => "Stardos+Stencil",
      "Stint Ultra Condensed" => "Stint+Ultra+Condensed",
      "Stint Ultra Expanded" => "Stint+Ultra+Expanded",
      "Stoke" => "Stoke",
      "Strait" => "Strait",
      "Sue Ellen Francisco" => "Sue+Ellen+Francisco",
      "Sunshiney" => "Sunshiney",
      "Supermercado One" => "Supermercado+One",
      "Suwannaphum" => "Suwannaphum",
      "Swanky and Moo Moo" => "Swanky+and+Moo+Moo",
      "Syncopate" => "Syncopate",
      "Tangerine" => "Tangerine",
      "Taprom" => "Taprom",
      "Tauri" => "Tauri",
      "Telex" => "Telex",
      "Tenor Sans" => "Tenor+Sans",
      "Text Me One" => "Text+Me+One",
      "The Girl Next Door" => "The+Girl+Next+Door",
      "Tienne" => "Tienne",
      "Tinos" => "Tinos",
      "Titan One" => "Titan+One",
      "Titillium Web" => "Titillium+Web",
      "Trade Winds" => "Trade+Winds",
      "Trocchi" => "Trocchi",
      "Trochut" => "Trochut",
      "Trykker" => "Trykker",
      "Tulpen One" => "Tulpen+One",
      "Ubuntu" => "Ubuntu",
      "Ubuntu Condensed" => "Ubuntu+Condensed",
      "Ubuntu Mono" => "Ubuntu+Mono",
      "Ultra" => "Ultra",
      "Uncial Antiqua" => "Uncial+Antiqua",
      "Underdog" => "Underdog",
      "Unica One" => "Unica+One",
      "UnifrakturCook" => "UnifrakturCook",
      "UnifrakturMaguntia" => "UnifrakturMaguntia",
      "Unkempt" => "Unkempt",
      "Unlock" => "Unlock",
      "Unna" => "Unna",
      "VT323" => "VT323",
      "Vampiro One" => "Vampiro+One",
      "Varela" => "Varela",
      "Varela Round" => "Varela+Round",
      "Vast Shadow" => "Vast+Shadow",
      "Vibur" => "Vibur",
      "Vidaloka" => "Vidaloka",
      "Viga" => "Viga",
      "Voces" => "Voces",
      "Volkhov" => "Volkhov",
      "Vollkorn" => "Vollkorn",
      "Voltaire" => "Voltaire",
      "Waiting for the Sunrise" => "Waiting+for+the+Sunrise",
      "Wallpoet" => "Wallpoet",
      "Walter Turncoat" => "Walter+Turncoat",
      "Warnes" => "Warnes",
      "Wellfleet" => "Wellfleet",
      "Wendy One" => "Wendy+One",
      "Wire One" => "Wire+One",
      "Yanone Kaffeesatz" => "Yanone+Kaffeesatz",
      "Yellowtail" => "Yellowtail",
      "Yeseva One" => "Yeseva+One",
      "Yesteryear" => "Yesteryear",
      "Zeyada" => "Zeyada",
   );
   return $himalayas_google_fonts;
}
endif;