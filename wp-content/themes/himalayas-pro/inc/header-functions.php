<?php
/**
 * Contains all the fucntions and components related to header part.
 *
 * @package ThemeGrill
 * @subpackage Himalayas Pro
 * @since Himalayas Pro 1.0
 */

if ( ! function_exists( 'himalayas_featured_image_slider') ):
/**
 * Display Featured Image as Header Background
 */
function himalayas_featured_image_slider() {
   $himalayas_slider_layout = get_theme_mod( 'himalayas_slider_layout', 'slider-style-1' ); ?>
   <div id="home" class="slider-wrapper <?php echo $himalayas_slider_layout; ?>">
      <div class="slider-container">
         <div class="bxslider">
            <?php
            $page_array = array();
            $slider_num = intval( get_theme_mod('himalayas_slider_number', 4) );
            for ( $i=1; $i<=$slider_num; $i++ ) {
               $page_id = get_theme_mod( 'himalayas_slide'.$i );
               if ( !empty ($page_id ) )
                  array_push( $page_array, $page_id );
            }

            $get_featured_posts = new WP_Query( array(
               'posts_per_page'        => -1,
               'post_type'             =>  array( 'page' ),
               'post__in'              => $page_array,
               'orderby'               => 'post__in'
            ) );

            while( $get_featured_posts->have_posts() ):$get_featured_posts->the_post();
               $himalayas_slider_title = get_the_title();
               $himalayas_slider_description = get_the_excerpt();
               $himalayas_slider_image = get_the_post_thumbnail();

               if( !empty( $himalayas_slider_image ) ): ?>
               <div class="slides">
                  <div class="parallax-overlay"> </div>

                  <?php echo '<figure>' . $himalayas_slider_image . '</figure>';

                  if( !empty( $himalayas_slider_title ) || !empty( $himalayas_slider_description ) ) { ?>
                     <div class="tg-container">

                        <?php if( !empty( $himalayas_slider_title ) ) { ?>
                           <h3 class="caption-title">
                              <span><?php echo $himalayas_slider_title; ?></span>
                           </h3>
                        <?php  }

                        if( !empty( $himalayas_slider_description ) ) { ?>
                           <div class="caption-sub">
                              <?php echo $himalayas_slider_description; ?>
                           </div>

                           <a class="slider-readmore" href="<?php echo get_permalink(); ?>"> <?php echo get_theme_mod( 'himalayas_read_more', __( 'Read more', 'himalayas' ) ); ?></a>
                        <?php  } ?>
                     </div>
                  <?php  } ?>
               </div>
               <?php  endif;
            endwhile;
            // Reset Post Data
            wp_reset_query(); ?>
         </div><!-- bxslider -->
      </div><!-- slider-container -->
   </div>
<?php }
endif;

/****************************************************************************************/

if ( ! function_exists( 'himalayas_pass_slider_parameters' ) ) :
/**
 * Function to pass the slider effect parameters from php file to js file.
 */
function himalayas_pass_slider_parameters() {
   $transition_effect = get_theme_mod( 'himalayas_slider_mode', 'fade' );
   $transition_delay = get_theme_mod( 'himalayas_slider_delay', 5 );
   $transition_duration = get_theme_mod( 'himalayas_slider_duration', 1 );

   $transition_delay = intval($transition_delay);
   $transition_duration = intval($transition_duration);

   if ( $transition_delay != 0 ) { $transition_delay = $transition_delay * 1000; }
   else { $transition_delay = 5000; }
   if ( $transition_duration != 0 ) { $transition_duration = $transition_duration * 1000; }
   else { $transition_duration = 1000; }

   wp_localize_script(
      'himalayas-bxslider',
      'himalayas_slider_value',
      array(
         'transition_effect'     => $transition_effect,
         'transition_delay'      => $transition_delay,
         'transition_duration'   => $transition_duration
      )
   );
}
endif;