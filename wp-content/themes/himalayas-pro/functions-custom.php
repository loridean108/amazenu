<?php 
/**
 * Adds the custom fields to the registration form and profile editor
 *
 */
function pw_rcp_add_user_fields() {
	
	$telephone = get_user_meta( get_current_user_id(), 'rcp_telephone', true );
	$grade_level   = get_user_meta( get_current_user_id(), 'rcp_grade_level', true );
	$comments   = get_user_meta( get_current_user_id(), 'rcp_comments', true );
	$address1   = get_user_meta( get_current_user_id(), 'rcp_address1', true );
	$address2   = get_user_meta( get_current_user_id(), 'rcp_address2', true );
	$city   = get_user_meta( get_current_user_id(), 'rcp_city', true );
	$state   = get_user_meta( get_current_user_id(), 'rcp_state', true );
	$country   = get_user_meta( get_current_user_id(), 'rcp_country', true );
	$zip   = get_user_meta( get_current_user_id(), 'rcp_zip', true );
	$role   = get_user_meta( get_current_user_id(), 'rcp_member_role', true );

	$grade_level_terms = get_grade_level_terms();
  
   /*SMW add optional to the label if user not logged in*/
  $optionalReq = '';
  if(!is_user_logged_in()){
    $optionalReq = '(Optional)';
  }
	?>
	<p class="col-sm-6">
		<label for="rcp_telephone"><?php _e( 'Telephone (Optional)', 'rcp' ); ?></label>
		<input name="rcp_telephone" id="rcp_telephone" type="text" value="<?php echo esc_attr( $telephone ); ?>"/>
	</p>
	<p class="col-sm-6">
		<label for="rcp_grade_level"><?php _e( 'Specify Grade Level', 'rcp' ); ?></label>
		<select name="rcp_grade_level" id="rcp_grade_level">
			<option disabled="disabled" selected="selected">Select One</option>
			<?php foreach ($grade_level_terms as $glvl) { ?>
				<option value="<?php echo $glvl->term_id; ?>" <?php echo ($grade_level == $glvl->term_id) ? 'selected="selected"' : '';?>><?php echo $glvl->name; ?></option>
			<?php } ?>
		</select>
	</p>
	<!--<p class="col-sm-12">
		<label for="rcp_comments"><?php _e( 'Comments', 'rcp' ); ?></label>
		<textarea name="rcp_comments" id="rcp_comments"><?php echo esc_attr( $comments ); ?></textarea>
	</p>-->


	<br/>


	<!-- <h3>Billing Information</h3> -->
	<p class="col-sm-12">
		<label for="rcp_address1"><?php _e( 'Street Address '.$optionalReq, 'rcp' ); ?></label>
		<input name="rcp_address1" id="rcp_address1" type="text" placeholder="Street Name, Address, P.O. Box, etc." value="<?php echo esc_attr( $address1 ); ?>"/>
		<input name="rcp_address2" id="rcp_address2" type="text" placeholder="Suit, Apartment, Unit, Building, etc." value="<?php echo esc_attr( $address2 ); ?>" style="margin-top: 10px;"/>
	</p>
	<p class="col-sm-6">
		<label for="rcp_city"><?php _e( 'City '.$optionalReq, 'rcp' ); ?></label>
		<input name="rcp_city" id="rcp_city" type="text" value="<?php echo esc_attr( $city ); ?>"/>
	</p>
	<p class="col-sm-6">
		<label for="rcp_state"><?php _e( 'State '.$optionalReq, 'rcp' ); ?></label>
		<?php 
			$statesArray = array(
								("Alabama"),
								("Alaska"),
								("Arizona"),
								("Arkansas"),
								("California"),
								("Colorado"),
								("Connecticut"),
								("Delaware"),
								("District Of Columbia"),
								("Florida"),
								("Georgia"),
								("Hawaii"),
								("Idaho"),
								("Illinois"),
								("Indiana"),
								("Iowa"),
								("Kansas"),
								("Kentucky"),
								("Louisiana"),
								("Maine"),
								("Maryland"),
								("Massachusetts"),
								("Michigan"),
								("Minnesota"),
								("Mississippi"),
								("Missouri"),
								("Montana"),
								("Nebraska"),
								("Nevada"),
								("New Hampshire"),
								("New Jersey"),
								("New Mexico"),
								("New York"),
								("North Carolina"),
								("North Dakota"),
								("Ohio"),
								("Oklahoma"),
								("Oregon"),
								("Pennsylvania"),
								("Rhode Island"),
								("South Carolina"),
								("South Dakota"),
								("Tennessee"),
								("Texas"),
								("Utah"),
								("Vermont"),
								("Virginia"),
								("Washington"),
								("West Virginia"),
								("Wisconsin"),
								("Wyoming")
							);
		 ?>
		<select name="rcp_state" id="rcp_state">
			<option disabled="disabled" selected="selected">Select One</option>
			<?php foreach ($statesArray as $estado) { ?>
				<option <?php echo ($state == $estado) ? 'selected' : ''; ?>><?php echo $estado; ?></option>
			<?php } ?>
		</select>
	</p>
	<p class="col-sm-6" style="float: none;">
		<label for="rcp_zip"><?php _e( 'Zip', 'rcp' ); ?></label>
		<input name="rcp_zip" id="rcp_zip" type="text" value="<?php echo esc_attr( $zip ); ?>"/>
	</p>

	<p id="rcp_member_role_wrap" class="col-sm-12">
		<ul>
			<li class="rcp_subscription_level">
				<input name="rcp_member_role" class="rcp_level" id="rcp_member_role_parent" type="radio" value="parent" <?php if( esc_attr($role) == 'parent' ) { echo 'checked="checked"'; } ?>  />
				<label for="rcp_member_role_parent">I am a parent</label>
			</li>
			<li class="rcp_subscription_level">
				<input name="rcp_member_role" class="rcp_level" id="rcp_member_role_teacher" type="radio" value="teacher" <?php if( esc_attr($role) == 'teacher' ) { echo 'checked="checked"'; } ?>  />
				<label for="rcp_member_role_teacher">I am a teacher</label>					
			</li>
		</ul>
	</p>

	<?php
}
add_action( 'rcp_before_subscription_form_fields', 'pw_rcp_add_user_fields' );
add_action( 'rcp_profile_editor_after', 'pw_rcp_add_user_fields' );
/**
 * Adds the custom fields to the member edit screen
 *
 */
function pw_rcp_add_member_edit_fields( $user_id = 0 ) {
	
		$telephone = get_user_meta( $user_id, 'rcp_telephone', true );
		$grade_level   = get_user_meta( $user_id, 'rcp_grade_level', true );
		$comments   = get_user_meta( $user_id, 'rcp_comments', true );
		$address1   = get_user_meta( $user_id, 'rcp_address1', true );
		$address2   = get_user_meta( $user_id, 'rcp_address2', true );
		$city   = get_user_meta( $user_id, 'rcp_city', true );
		$state   = get_user_meta( $user_id, 'rcp_state', true );
		$country   = get_user_meta( $user_id, 'rcp_country', true );
		$zip   = get_user_meta( $user_id, 'rcp_zip', true );
		$role   = get_user_meta( $user_id, 'rcp_member_role', true );

		$grade_level_terms = get_grade_level_terms();
	?>
	<tr valign="top">
		<th scope="row" valign="top">
			<label for="rcp_telephone"><?php _e( 'Telephone', 'rcp' ); ?></label>
		</th>
		<td>
			<input name="rcp_telephone" id="rcp_telephone" type="text" value="<?php echo $telephone; ?>"/>
			<p class="description"><?php _e( 'The member\'s telephone', 'rcp' ); ?></p>
		</td>
	</tr>
	<tr valign="top">
		<th scope="row" valign="top">
			<label for="rcp_grade_level"><?php _e( 'Grade Level', 'rcp' ); ?></label>
		</th>
		<td>
			<select name="rcp_grade_level" id="rcp_grade_level">
				<option disabled="disabled" selected="selected">Select One</option>
				<?php foreach ($grade_level_terms as $glvl) { ?>
					<option value="<?php echo $glvl->term_id; ?>" <?php echo ($grade_level == $glvl->term_id) ? 'selected="selected"' : '';?>><?php echo $glvl->name; ?></option>
				<?php } ?>
			</select>
			<p class="description"><?php _e( 'The member\'s Grade Level', 'rcp' ); ?></p>
		</td>
	</tr>
	<tr valign="top">
		<th scope="row" valign="top">
			<label for="rcp_comments"><?php _e( 'Comments', 'rcp' ); ?></label>
		</th>
		<td>
			<textarea name="rcp_comments" id="rcp_comments"><?php echo esc_attr( $comments ); ?></textarea>
			<p class="description"><?php _e( 'The member\'s Comments', 'rcp' ); ?></p>
		</td>
	</tr>
	<tr valign="top">
		<th scope="row" valign="top">
			<label for="rcp_address1"><?php _e( 'Street Address', 'rcp' ); ?></label>
		</th>
		<td>
			<input name="rcp_address1" id="rcp_address1" type="text" placeholder="Street Name, Address, P.O. Box, etc." value="<?php echo esc_attr( $address1 ); ?>"/>
			<input name="rcp_address2" id="rcp_address2" type="text" placeholder="Suit, Apartament, Unit, Building, etc." value="<?php echo esc_attr( $address2 ); ?>" style="margin-top: 10px;"/>
			<p class="description"><?php _e( 'The member\'s Street Address', 'rcp' ); ?></p>
		</td>
	</tr>
	<tr valign="top">
		<th scope="row" valign="top">
			<label for="rcp_city"><?php _e( 'City', 'rcp' ); ?></label>
		</th>
		<td>
			<input name="rcp_city" id="rcp_city" type="text" value="<?php echo esc_attr( $city ); ?>"/>
			<p class="description"><?php _e( 'The member\'s City', 'rcp' ); ?></p>
		</td>
	</tr>
	<tr valign="top">
		<th scope="row" valign="top">
			<label for="rcp_state"><?php _e( 'State', 'rcp' ); ?></label>
		</th>
		<td>
			<?php 
			$statesArray = array(
								("Alabama"),
								("Alaska"),
								("Arizona"),
								("Arkansas"),
								("California"),
								("Colorado"),
								("Connecticut"),
								("Delaware"),
								("District Of Columbia"),
								("Florida"),
								("Georgia"),
								("Hawaii"),
								("Idaho"),
								("Illinois"),
								("Indiana"),
								("Iowa"),
								("Kansas"),
								("Kentucky"),
								("Louisiana"),
								("Maine"),
								("Maryland"),
								("Massachusetts"),
								("Michigan"),
								("Minnesota"),
								("Mississippi"),
								("Missouri"),
								("Montana"),
								("Nebraska"),
								("Nevada"),
								("New Hampshire"),
								("New Jersey"),
								("New Mexico"),
								("New York"),
								("North Carolina"),
								("North Dakota"),
								("Ohio"),
								("Oklahoma"),
								("Oregon"),
								("Pennsylvania"),
								("Rhode Island"),
								("South Carolina"),
								("South Dakota"),
								("Tennessee"),
								("Texas"),
								("Utah"),
								("Vermont"),
								("Virginia"),
								("Washington"),
								("West Virginia"),
								("Wisconsin"),
								("Wyoming")
							);
		 ?>
		<select name="rcp_state" id="rcp_state">
			<option disabled="disabled" selected="selected">Select One</option>
			<?php foreach ($statesArray as $estado) { ?>
				<option <?php echo ($state == $estado) ? 'selected' : ''; ?>><?php echo $estado; ?></option>
			<?php } ?>
		</select>
			<p class="description"><?php _e( 'The member\'s State', 'rcp' ); ?></p>
		</td>
	</tr>
	<tr valign="top">
		<th scope="row" valign="top">
			<label for="rcp_zip"><?php _e( 'Zip', 'rcp' ); ?></label>
		</th>
		<td>
			<input name="rcp_zip" id="rcp_zip" type="text" value="<?php echo esc_attr( $zip ); ?>"/>
			<p class="description"><?php _e( 'The member\'s Zip', 'rcp' ); ?></p>
		</td>
	</tr>

	<tr valign="top">
		<th scope="row" valign="top">
			<label for="rcp_member_role"><?php _e( 'Role', 'rcp' ); ?></label>
		</th>
		<td>
			<ul>
				<li class="rcp_subscription_level">
					<input name="rcp_member_role" class="rcp_level" id="rcp_member_role_parent" type="radio" value="parent" <?php if( esc_attr($role) == 'parent' ) { echo 'checked="checked"'; } ?>  />
					<label for="rcp_member_role_parent">I am a parent</label>
				</li>
				<li class="rcp_subscription_level">
					<input name="rcp_member_role" class="rcp_level" id="rcp_member_role_teacher" type="radio" value="teacher" <?php if( esc_attr($role) == 'teacher' ) { echo 'checked="checked"'; } ?>  />
					<label for="rcp_member_role_teacher">I am a teacher</label>					
				</li>
			</ul>
		</td>
	</tr>

	<?php
}
add_action( 'rcp_edit_member_after', 'pw_rcp_add_member_edit_fields' );
 
/**
 * Determines if there are problems with the registration data submitted
 *
 */
function pw_rcp_validate_user_fields_on_register( $posted ) {
	if( empty( $posted['rcp_grade_level'] ) ) {
		rcp_errors()->add( 'invalid_grade_level', __( 'Please choose one Grade Level', 'rcp' ), 'register' );
	}
	if(is_user_logged_in() &&  empty( $posted['rcp_address1'] ) ) {
		rcp_errors()->add( 'invalid_address1', __( 'Please enter your Street Address', 'rcp' ), 'register' );
	}
	if(is_user_logged_in() &&  empty( $posted['rcp_city'] ) ) {
		rcp_errors()->add( 'invalid_city', __( 'Please enter your City', 'rcp' ), 'register' );
	}
	if(is_user_logged_in() &&  empty( $posted['rcp_state'] ) ) {
		rcp_errors()->add( 'invalid_state', __( 'Please choose one State', 'rcp' ), 'register' );
	}
	if( empty( $posted['rcp_zip'] ) ) {
		rcp_errors()->add( 'invalid_zip', __( 'Please enter your Zip Code', 'rcp' ), 'register' );
	}
	if( empty( $posted['rcp_member_role'] ) ) {
		rcp_errors()->add( 'invalid_member_role', __( 'Please specify if you are a Parent or Teacher', 'rcp' ), 'register' );
	}
}
add_action( 'rcp_form_errors', 'pw_rcp_validate_user_fields_on_register', 10 );
/**
 * Stores the information submitted during registration
 *
 */
function pw_rcp_save_user_fields_on_register( $posted, $user_id ) {
	if( ! empty( $posted['rcp_telephone'] ) ) {
		update_user_meta( $user_id, 'rcp_telephone', sanitize_text_field( $posted['rcp_telephone'] ) );
	}
	if( ! empty( $posted['rcp_grade_level'] ) ) {
		update_user_meta( $user_id, 'rcp_grade_level', sanitize_text_field( $posted['rcp_grade_level'] ) );
	}
	if( ! empty( $posted['rcp_comments'] ) ) {
		update_user_meta( $user_id, 'rcp_comments', sanitize_text_field( $posted['rcp_comments'] ) );
	}
	if( ! empty( $posted['rcp_address1'] ) ) {
		update_user_meta( $user_id, 'rcp_address1', sanitize_text_field( $posted['rcp_address1'] ) );
	}
	if( ! empty( $posted['rcp_address2'] ) ) {
		update_user_meta( $user_id, 'rcp_address2', sanitize_text_field( $posted['rcp_address2'] ) );
	}
	if( ! empty( $posted['rcp_city'] ) ) {
		update_user_meta( $user_id, 'rcp_city', sanitize_text_field( $posted['rcp_city'] ) );
	}
	if( ! empty( $posted['rcp_state'] ) ) {
		update_user_meta( $user_id, 'rcp_state', sanitize_text_field( $posted['rcp_state'] ) );
	}
	if( ! empty( $posted['rcp_zip'] ) ) {
		update_user_meta( $user_id, 'rcp_zip', sanitize_text_field( $posted['rcp_zip'] ) );
	}
	if( ! empty( $posted['rcp_member_role'] ) ) {
		update_user_meta( $user_id, 'rcp_member_role', sanitize_text_field( $posted['rcp_member_role'] ) );
	}
}
add_action( 'rcp_form_processing', 'pw_rcp_save_user_fields_on_register', 10, 2 );
/**
 * Stores the information submitted profile update
 *
 */
function pw_rcp_save_user_fields_on_profile_save( $user_id ) {
	if( ! empty( $_POST['rcp_telephone'] ) ) {
		update_user_meta( $user_id, 'rcp_telephone', sanitize_text_field( $_POST['rcp_telephone'] ) );
	}
	if( ! empty( $_POST['rcp_grade_level'] ) ) {
		update_user_meta( $user_id, 'rcp_grade_level', sanitize_text_field( $_POST['rcp_grade_level'] ) );
	}
	if( ! empty( $_POST['rcp_comments'] ) ) {
		update_user_meta( $user_id, 'rcp_comments', sanitize_text_field( $_POST['rcp_comments'] ) );
	}
	if( ! empty( $_POST['rcp_address1'] ) ) {
		update_user_meta( $user_id, 'rcp_address1', sanitize_text_field( $_POST['rcp_address1'] ) );
	}
	if( ! empty( $_POST['rcp_address2'] ) ) {
		update_user_meta( $user_id, 'rcp_address2', sanitize_text_field( $_POST['rcp_address2'] ) );
	}
	if( ! empty( $_POST['rcp_city'] ) ) {
		update_user_meta( $user_id, 'rcp_city', sanitize_text_field( $_POST['rcp_city'] ) );
	}
	if( ! empty( $_POST['rcp_state'] ) ) {
		update_user_meta( $user_id, 'rcp_state', sanitize_text_field( $_POST['rcp_state'] ) );
	}
	if( ! empty( $_POST['rcp_zip'] ) ) {
		update_user_meta( $user_id, 'rcp_zip', sanitize_text_field( $_POST['rcp_zip'] ) );
	}
	if( ! empty( $_POST['rcp_member_role'] ) ) {
		update_user_meta( $user_id, 'rcp_member_role', sanitize_text_field( $_POST['rcp_member_role'] ) );
	}	
}
add_action( 'rcp_user_profile_updated', 'pw_rcp_save_user_fields_on_profile_save', 10 );
add_action( 'rcp_edit_member', 'pw_rcp_save_user_fields_on_profile_save', 10 );