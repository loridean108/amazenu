<?php


/**
 * Himalayas Pro functions related to defining constants, adding files and WordPress core functionality.
 *
 * Defining some constants, loading all the required files and Adding some core functionality.
 * @uses add_theme_support() To add support for post thumbnails and automatic feed links.
 * @uses register_nav_menu() To add support for navigation menu.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @package ThemeGrill
 * @subpackage Himalayas Pro
 * @since Himalayas Pro 1.0
 */
add_action('after_setup_theme', 'himalayas_setup');
/**
 * All setup functionalities.
 *
 * @since 1.0
 */
if (!function_exists('himalayas_setup')) :

    function himalayas_setup() {

        /**
         * Set the content width based on the theme's design and stylesheet.
         */
        global $content_width;
        if (!isset($content_width))
            $content_width = 783;

        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         */
        load_theme_textdomain('himalayas', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head
        add_theme_support('automatic-feed-links');

        // This theme uses Featured Images (also known as post thumbnails) for per-post/per-page.
        add_theme_support('post-thumbnails');

        // Registering navigation menu.
        register_nav_menu('primary', __('Primary Menu', 'himalayas'));
        register_nav_menu('members', __('Members Menu', 'himalayas'));
        register_nav_menu('footer', __('Footer Menu', 'himalayas'));

        // Cropping the images to different sizes to be used in the theme
        add_image_size('himalayas-featured-post', 781, 512, true);
        add_image_size('himalayas-portfolio-image', 400, 350, true);
        add_image_size('himalayas-featured-image', 319, 142, true);
        add_image_size('himalayas-services', 470, 280, true);
        add_image_size('video-thumbnail', 400, 280, true);

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        // Adding excerpt option box for pages as well
        add_post_type_support('page', 'excerpt');

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
        ));
    }

endif;

/**
 * Define Directory Location Constants
 */
define('HIMALAYAS_PARENT_DIR', get_template_directory());
define('HIMALAYAS_CHILD_DIR', get_stylesheet_directory());
define('HIMALAYAS_INCLUDES_DIR', HIMALAYAS_PARENT_DIR . '/inc');
define('HIMALAYAS_CSS_DIR', HIMALAYAS_PARENT_DIR . '/css');
define('HIMALAYAS_JS_DIR', HIMALAYAS_PARENT_DIR . '/js');
define('HIMALAYAS_LANGUAGES_DIR', HIMALAYAS_PARENT_DIR . '/languages');
define('HIMALAYAS_ADMIN_DIR', HIMALAYAS_INCLUDES_DIR . '/admin');
define('HIMALAYAS_WIDGETS_DIR', HIMALAYAS_INCLUDES_DIR . '/widgets');
define('HIMALAYAS_ADMIN_IMAGES_DIR', HIMALAYAS_ADMIN_DIR . '/images');

/**
 * Define URL Location Constants
 */
define('HIMALAYAS_PARENT_URL', get_template_directory_uri());
define('HIMALAYAS_CHILD_URL', get_stylesheet_directory_uri());

define('HIMALAYAS_INCLUDES_URL', HIMALAYAS_PARENT_URL . '/inc');
define('HIMALAYAS_CSS_URL', HIMALAYAS_PARENT_URL . '/css');
define('HIMALAYAS_JS_URL', HIMALAYAS_PARENT_URL . '/js');
define('HIMALAYAS_LANGUAGES_URL', HIMALAYAS_PARENT_URL . '/languages');

define('HIMALAYAS_ADMIN_URL', HIMALAYAS_INCLUDES_URL . '/admin');
define('HIMALAYAS_WIDGETS_URL', HIMALAYAS_INCLUDES_URL . '/widgets');

define('HIMALAYAS_ADMIN_IMAGES_URL', HIMALAYAS_ADMIN_URL . '/images');

// Include files
require_once( HIMALAYAS_INCLUDES_DIR . '/functions.php' );
require_once( HIMALAYAS_INCLUDES_DIR . '/header-functions.php' );
require_once( HIMALAYAS_INCLUDES_DIR . '/customizer.php' );
require_once( HIMALAYAS_ADMIN_DIR . '/meta-boxes.php' );
require_once( HIMALAYAS_WIDGETS_DIR . '/widgets.php' );
/** Load license api */
require get_template_directory() . '/license.php';


function add_logout_link( $items, $args ) {
    if (is_user_logged_in()) {
        $items .= '<li><a href="'. wp_logout_url(home_url()).'">Log Out</a></li>';
    }else{
        $items .= '<li><a href="'. get_bloginfo('url') . '/login">Log In</a></li>';
    }

    return $items;
}

add_filter( 'wp_nav_menu_items', 'add_logout_link', 10, 2 );

function add_menu_icons_styles() {
    ?>

    <style>
        #adminmenu .menu-icon-video div.wp-menu-image:before {
            content: "\f236";
        }
    </style>

    <?php

}

add_action('admin_head', 'add_menu_icons_styles');

function print_dev($var) {
    echo '<pre>';
    print_r($var);
    echo '</pre>';
}

function get_grade_level_terms() {
    $term = get_term_by('slug', 'grade-level', 'category');
    $termchildren = get_term_children($term->term_id, 'category');

    $gradeTermsArray = array();

    foreach ($termchildren as $child) {
        $gradeTermsArray[] = get_term_by('id', $child, 'category');
    }

    return $gradeTermsArray;
}

function get_outcome_terms() {
    $term = get_term_by('slug', 'desired-outcome', 'category');
    $termchildren = get_term_children($term->term_id, 'category');
    $outcomeTermsArray = array();

    foreach ($termchildren as $child) {
        $outcomeTermsArray[] = get_term_by('id', $child, 'category');
    }

    return $outcomeTermsArray;
}

function get_activity_terms() {
    $term = get_term_by('slug', 'activity', 'category');
    $termchildren = get_term_children($term->term_id, 'category');
    $activityTermsArray = array();

    foreach ($termchildren as $child) {
        $activityTermsArray[] = get_term_by('id', $child, 'category');
    }

    return $activityTermsArray;
}

function get_post_category_x_tax($postID, $slugTax) {
    $term = get_term_by('slug', $slugTax, 'category');
    $categories = get_the_category($postID);

    if ($categories) {
        foreach ($categories as $cat) {
            if ($cat->parent == $term->term_id) {
                return $cat->name;
            }
        }
    }
    return;
}

function check_filters_videos(){
    /* SAVE GRADE LEVEL*/
    if (!empty($_GET['lvl'])) {
        if (!is_numeric($_GET['lvl'])) {
            header("Location: " . get_bloginfo('url') . '/grade-level/');
        } else {
            $termLvl = get_term($_GET['lvl'], 'category');

            if (!$termLvl) {
                header("Location: " . get_bloginfo('url') . '/grade-level/');
            }

            update_user_meta( get_current_user_id(), 'rcp_grade_level', sanitize_text_field( $_GET['lvl'] ) );
            header("Location: " . get_bloginfo('url') . '/desired-outcome/');
        }
    }

    /* SAVE DESIRED OUTCOME*/
    if (!empty($_GET['oc'])) {
        if (!is_numeric($_GET['oc'])) {
            header("Location: " . get_bloginfo('url') . '/desired-outcome/');
        } else {
            $termOC = get_term($_GET['oc'], 'category');

            if (!$termOC) {
                header("Location: " . get_bloginfo('url') . '/desired-outcome/');
            }

            update_user_meta( get_current_user_id(), 'rcp_desired_outcome', sanitize_text_field( $_GET['oc'] ) );
            header("Location: " . get_bloginfo('url') . '/activity/');
        }
    }

    /* SAVE ACTIVITY*/
    if (!empty($_GET['act'])) {
        if (!is_numeric($_GET['act'])) {
            header("Location: " . get_bloginfo('url') . '/activity/');
        } else {
            $termAct = get_term($_GET['act'], 'category');

            if (!$termAct) {
                header("Location: " . get_bloginfo('url') . '/activity/');
            }

            update_user_meta( get_current_user_id(), 'rcp_activity', sanitize_text_field( $_GET['act'] ) );
            header("Location: " . get_bloginfo('url') . '/recommended/');
        }
    }
}

add_action('init', 'check_filters_videos');

function save_filters_video(){
    if( ! empty( $_POST['grade_level'] ) ) {
        update_user_meta( get_current_user_id(), 'rcp_grade_level', sanitize_text_field( $_POST['grade_level'] ) );
    }
    if( ! empty( $_POST['desired-outcome'] ) ) {
        update_user_meta( get_current_user_id(), 'rcp_desired-outcome', sanitize_text_field( $_POST['desired-outcome'] ) );
    }
    if( ! empty( $_POST['activity'] ) ) {
        update_user_meta( get_current_user_id(), 'rcp_activity', sanitize_text_field( $_POST['activity'] ) );
    }
}

function extractYouTubeID($url) {

    /*
     * type1: http://www.youtube.com/watch?v=9Jr6OtgiOIw
     * type2: http://www.youtube.com/watch?v=9Jr6OtgiOIw&feature=related
     * type3: http://youtu.be/9Jr6OtgiOIw
     */
    $vid_id = "";
    $flag = false;
    if (isset($url) && !empty($url)) {
        /* case1 and 2 */
        $parts = explode("?", $url);
        if (isset($parts) && !empty($parts) && is_array($parts) && count($parts) > 1) {
            $params = explode("&", $parts[1]);
            if (isset($params) && !empty($params) && is_array($params)) {
                foreach ($params as $param) {
                    $kv = explode("=", $param);
                    if (isset($kv) && !empty($kv) && is_array($kv) && count($kv) > 1) {
                        if ($kv[0] == 'v') {
                            $vid_id = $kv[1];
                            $flag = true;
                            break;
                        }
                    }
                }
            }
        }

        /* case 3 */
        if (!$flag) {
            $needle = "youtu.be/";
            $pos = null;
            $pos = strpos($url, $needle);
            if ($pos !== false) {
                $start = $pos + strlen($needle);
                $vid_id = substr($url, $start, 11);
                $flag = true;
            }
        }
    }
    return $vid_id;
}

function get_vimeoid( $url ) {
    $regex = '~
        # Match Vimeo link and embed code
        (?:<iframe [^>]*src=")?         # If iframe match up to first quote of src
        (?:                             # Group vimeo url
                https?:\/\/             # Either http or https
                (?:[\w]+\.)*            # Optional subdomains
                vimeo\.com              # Match vimeo.com
                (?:[\/\w]*\/videos?)?   # Optional video sub directory this handles groups links also
                \/                      # Slash before Id
                ([0-9]+)                # $1: VIDEO_ID is numeric
                [^\s]*                  # Not a space
        )                               # End group
        "?                              # Match end quote if part of src
        (?:[^>]*></iframe>)?            # Match the end of the iframe
        (?:<p>.*</p>)?                  # Match any title information stuff
        ~ix';
    
    preg_match( $regex, $url, $matches );
    
    return $matches[1];
}

function get_vimeo_thumbnail( $videoID ){
    /*
    You may want to use oEmbed discovery instead of hard-coding the oEmbed endpoint.
    */
    $oembed_endpoint = 'http://vimeo.com/api/oembed';
    
    // Grab the video url from the url, or use default
    $video_url = 'http://vimeo.com/' . $videoID;
    
    // Create the URLs
    $json_url = $oembed_endpoint . '.json?url=' . rawurlencode($video_url) . '&width=640';
    $xml_url = $oembed_endpoint . '.xml?url=' . rawurlencode($video_url) . '&width=640';
    
    // Load in the oEmbed XML
    $oembed = simplexml_load_string(curl_get($xml_url));

    return $oembed->thumbnail_url;
}

// Curl helper function
function curl_get($url) {
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    $return = curl_exec($curl);
    curl_close($curl);
    return $return;
}

function get_current_user_role() {
    global $wp_roles;
    $current_user = wp_get_current_user();
    $roles = $current_user->roles;
    $role = array_shift($roles);
    return isset($wp_roles->role_names[$role]) ? translate_user_role($wp_roles->role_names[$role] ) : false;
}

add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
    $role_cur_user = get_current_user_role();
    if ($role_cur_user == 'Subscriber') {
      show_admin_bar(false);
    }
}

include('functions-custom.php');

?>