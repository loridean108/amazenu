<?php
/**
 * The template for displaying all single posts
 *
 * @package ThemeGrill
 * @subpackage Himalayas Pro
 * @since Himalayas Pro 1.0
 */
?>

<?php
$termLvl = get_term(@$_GET['lvl'], 'category');
$termOC = get_term(@$_GET['oc'], 'category');
$termAct = get_term(@$_GET['act'], 'category');
?>

<?php get_header(); ?>

<?php
do_action('himalayas_before_body_content');

$himalayas_layout = himalayas_layout_class();
?>

<div id="content" class="site-content">
    <main id="main" class="clearfix <?php echo $himalayas_layout; ?>">
        <div class="tg-container">

           <div id="primary">
                <?php if (@$termLvl->name) { ?>
                   <h2 class="entry-title">Suggested activities for: 
                       <?php echo (@$termLvl->name) ? '<span>' . $termLvl->name . '</span>' : ''; ?> 
                       <?php echo (@$termOC->name) ? ' <span>' . $termOC->name . '</span>' : ''; ?>
                       <?php echo (@$termAct->name) ? ' <span>' . $termAct->name . '</span>' : ''; ?>
                   </h2>
               <?php } ?>
                <div id="content-2">
                    <?php while (have_posts()) : the_post(); ?>
                        <article id = "post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <h2 class="entry-title"><?php the_title(); ?></h2>

                            <div class="entry-content video-wrapper">
                                <?php
                                the_content();
                           		if (get_field('vimeo_video_link')) {
                           			echo '<iframe src="https://player.vimeo.com/video/' . get_vimeoid(get_field('vimeo_video_link')) . '" width="500" height="375" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
                                //echo '<video src="https://player.vimeo.com/video/' . get_vimeoid(get_field('vimeo_video_link')) . '" ></video>';
                                }

                                //if (get_field('youtube_video_link')) {
                                    //$embed_code = wp_oembed_get(get_field('youtube_video_link'));
                                    //echo $embed_code;
                                //}
                               
                               ?>
                            </div>
                            <br/>
                           	<?php echo wpfp_link(); ?> 
	    					<a class="link-savedActivities" href="/my-saved-activities/">Go to your Saved Activities</a>
                           
                       </article>
                        <?php
                    endwhile;
                    ?>
                </div><!-- #content -->

                    <?php
                    //get_template_part( 'navigation', 'single' );

                    do_action('himalayas_before_comments_template');
                    // If comments are open or we have at least one comment, load up the comment template
                    if (comments_open() || '0' != get_comments_number())
                        comments_template();
                    do_action('himalayas_after_comments_template');
                    ?>
            </div><!-- #primary -->

                <?php himalayas_sidebar_select(); ?>
        </div>
    </main>
</div>

<?php do_action('himalayas_after_body_content'); ?>

<?php get_footer(); ?>