<?php

/**

 * Theme Header Section for our theme.

 *

 * @package ThemeGrill

 * @subpackage Himalayas Pro

 * @since Himalayas Pro 1.0

 */

?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

    <head>

        <meta charset="<?php bloginfo('charset'); ?>" />

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="profile" href="http://gmpg.org/xfn/11" />

        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

        <?php

        /**

         * This hook is important for wordpress plugins and other many things

         */

        wp_head();

        ?>
	
	<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','//connect.facebook.net/en_US/fbevents.js');

		fbq('init', '457881777749249');
		fbq('track', "PageView");</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=457881777749249&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->



    </head>



    <body <?php body_class(); ?>>


        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-73288832-1', 'auto');

            <?php if (get_current_user_id()): ?>
                
                ga('set', 'userId', <?php print get_current_user_id(); ?>);     // Set the user ID using signed-in user_id.
                
                ga('set', 'dimension1', <?php print get_current_user_id(); ?>); // Set the user ID
        
                <?php if (get_post_type() == 'video'): ?>
                    
                    ga('set', 'dimension2', <?php print get_the_ID(); ?>);          // Set the video ID
            
                    <?php $id = get_current_user_id(). '.' . get_the_ID(); ?>
                    
                    ga('set', 'dimension3', <?php print $id; ?>);

                <?php endif; ?> 
   
            <?php endif; ?>

            ga('send', 'pageview');

        </script>

        <?php // print get_post_type(); ?>
        <?php //print get_the_ID(); ?>
        <?php // print get_the_title(); ?>

        <?php do_action('himalayas_before'); ?>

        <div id="page" class="hfeed site">

            <?php do_action('himalayas_before_header'); ?>

            <header id="masthead" class="site-header clearfix" role="banner">

                <?php

                $himalayas_header_class = '';

                if (get_theme_mod('himalayas_sticky_on_off', 0) == 1) {

                    $himalayas_header_class .= 'non-stick';

                } else {

                    $himalayas_header_class .= 'stick';

                }

                if (get_theme_mod('himalayas_slide_on_off', 0) == 1 && is_front_page() && get_theme_mod('himalayas_trans_off', 0) != 1) {

                    $himalayas_header_class .= ' transparent';

                } else {

                    $himalayas_header_class .= ' non-transparent';

                }

                if (get_theme_mod('himalayas_header_logo_placement', 'header_text_only') == 'show_both') {

                    $himalayas_header_class .= ' show-both';

                }

                ?>

                <div class="header-wrapper clearfix <?php echo $himalayas_header_class; ?>">

                    <div class="tg-container">



                        <?php if (( get_theme_mod('himalayas_header_logo_placement', 'header_text_only') == 'show_both' || get_theme_mod('himalayas_header_logo_placement', 'header_text_only') == 'header_logo_only' ) && get_theme_mod('himalayas_logo', '') != '') { ?>



                            <div class="logo">

                                <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><img src="<?php echo esc_url(get_theme_mod('himalayas_logo')); ?>" alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"></a>

                            </div> <!-- logo-end -->

                            <?php

                        }

                        $screen_reader = '';

                        if (get_theme_mod('himalayas_header_logo_placement', 'header_text_only') == 'header_logo_only' || (get_theme_mod('himalayas_header_logo_placement', 'header_text_only') == 'none' )) {

                            $screen_reader = 'screen-reader-text';

                        }

                        ?>

                        <div id="header-text" class="<?php echo $screen_reader; ?>">

                            <?php if (is_front_page() || is_home()) : ?>

                                <h1 id="site-title">

                                    <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a>

                                </h1>

                            <?php else : ?>

                                <h3 id="site-title">

                                    <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a>

                                </h3>

                            <?php

                            endif;

                            $description = get_bloginfo('description', 'display');

                            if ($description || is_customize_preview()) :

                                ?>

                                <p id="site-description"><?php echo $description; ?></p>

                            <?php endif;

                            ?>

                        </div><!-- #header-text -->



                        <div class="menu-search-wrapper">



                            <div class="home-search">



                                <div class="search-icon">

                                    <i class="fa fa-search"> </i>

                                </div>



                                <div class="search-box">

                                    <div class="close"> &times; </div>

                                    <?php get_search_form(); ?>

                                </div>

                            </div> <!-- home-search-end -->



                            <nav id="site-navigation" class="main-navigation" role="navigation">

                                <p class="menu-toggle hide"></p>

                                <?php 
                                if (is_user_logged_in() && !is_home()) {
                                    wp_nav_menu(array('theme_location' => 'members', 'container_class' => 'menu-primary-container')); 
                                } else {
                                    wp_nav_menu(array('theme_location' => 'primary', 'container_class' => 'menu-primary-container')); 
                                }
                                ?>

                            </nav><!-- nav-end -->

                        </div><!-- Menu-search-wrapper end -->

                    </div><!-- tg-container -->

                </div><!-- header-wrapepr end -->





                <?php

                if (get_theme_mod('himalayas_slide_on_off') == 1 && is_front_page()) {

                    himalayas_pass_slider_parameters();

                    himalayas_featured_image_slider();

                }

                ?>

            </header>

            <?php do_action('himalayas_after_header'); ?>

            <?php do_action('himalayas_before_main'); ?>