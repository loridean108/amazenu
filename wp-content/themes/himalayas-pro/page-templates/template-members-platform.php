<?php
/**
 * Template Name: Members Platform
 *
 * Displays the Testimonial Template of the theme.
 *
 * @package ThemeGrill
 * @subpackage Himalayas Pro
 * @since Himalayas Pro 1.0
 */
?>

<?php get_header(); ?>

	<?php do_action( 'himalayas_before_body_content' );

	$himalayas_layout = himalayas_layout_class(); ?>

	<div id="content" class="site-content">
	   <main id="main" class="clearfix <?php echo $himalayas_layout; ?>">
	      <div class="tg-container">

				<div id="primary">
					<div id="content-2">
						<?php while ( have_posts() ) : the_post(); ?>
				
							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							    <?php do_action('himalayas_before_post_content'); ?>

							    <div class="entry-content">
							        <?php
							        the_content();
							        wp_link_pages(array(
							            'before' => '<div style="clear: both;"></div><div class="pagination clearfix">' . __('Pages:', 'himalayas'),
							            'after' => '</div>',
							            'link_before' => '<span>',
							            'link_after' => '</span>'
							        ));
							        ?>
							    </div>

							    <?php do_action('himalayas_after_post_content'); ?>
							</article>

							<?php
							do_action( 'himalayas_before_comments_template' );
							// If comments are open or we have at least one comment, load up the comment template
							if ( comments_open() || '0' != get_comments_number() )
								comments_template();
			      				do_action ( 'himalayas_after_comments_template' );

						endwhile; ?>
					</div><!-- #content -->
				</div><!-- #primary -->

				<?php  himalayas_sidebar_select(); ?>
			</div>
		</main>
	</div>

	<?php do_action( 'himalayas_after_body_content' ); ?>

<?php get_footer(); ?>