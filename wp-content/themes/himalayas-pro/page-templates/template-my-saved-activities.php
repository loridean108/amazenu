<?php
/**
 * Template Name: Videos - My Saved Activities
 *
 * Displays the Testimonial Template of the theme.
 *
 * @package ThemeGrill
 * @subpackage Himalayas Pro
 * @since Himalayas Pro 1.0
 */
?>

<?php get_header(); ?>

<?php
do_action('himalayas_before_body_content');

$himalayas_layout = himalayas_layout_class();
?>

<div id="content" class="site-content">
    <main id="main" class="clearfix <?php echo $himalayas_layout; ?>">
        <div class="tg-container">

            <div id="primary">
                <div id="content-2">
                    <?php
                    while (have_posts()) : the_post();
                        ?>

                        <article id = "post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <?php do_action('himalayas_before_post_content'); ?>

                            <?php
                            $outcome_terms = get_outcome_terms();

                            //print_dev($outcome_terms);
                            ?>

                            <h2 class="entry-title"><?php the_title(); ?></h2>

                            <div class="entry-content">
                                <?php
                                $favorites = wpfp_get_users_favorites();
//                                print_dev($favorites);
                                ?>

                                <div class="video-list my-favorites">
                                <?php if ($favorites) : ?>
                                    <?php foreach ($favorites as $favID) { ?>
                                        <?php
                                        $video = get_post($favID);
                                        //print_dev($video);
                                        ?>

                                        <?php if ($video) : ?>
                                            <?php
                                            if (get_post_field('vimeo_video_link', $video->ID)) {
                                                //$videoID = get_vimeoid(get_post_field('vimeo_video_link', $video->ID));
                                                ?>
                                            
                                               
                                                <div class="video-box">
                                                    <div class="video-content">
                                                        <a href="<?php echo get_the_permalink($video->ID); ?>"><h5><?php echo get_the_title($video->ID); ?></h5></a>
                                                        <a href="<?php echo get_the_permalink($video->ID); ?>">
                                                            <div class="content">
                                                                <?php $videoID = get_vimeoid(get_post_field('vimeo_video_link', $video->ID)); ?>
                                                                <img class="video-thub" src="<?php print get_vimeo_thumbnail($videoID); ?>" />
                                                                <img class="video-play" src="<?php echo get_template_directory_uri(); ?>/images/video-play.png" />
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="video-categories">
                                                        <table>
<!--                                                            <tr>
                                                                <th>Grade level:</th>
                                                                <td><span  class="btn-yellow"> <?php echo get_post_category_x_tax($video->ID, 'grade-level'); ?></span></td>
                                                            </tr>
 -->                                                           <tr>
                                                                <th>Desired Outcome:</th>
                                                                <td><span  class="btn-yellow"><?php echo get_post_category_x_tax($video->ID, 'desired-outcome'); ?></span></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Activity:</th>
                                                                <td><span  class="btn-yellow"><?php echo get_post_category_x_tax($video->ID, 'activity'); ?></span></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Duration:</th>
                                                                <td><span  class="btn-yellow"><?php echo get_post_field('video_duration', $video->ID); ?></span></td>
                                                            </tr>
                                                        </table>

                                                    </div>
                                                    <div class="btn-remove"><?php echo wpfp_link_html($video->ID, '<i class="fa fa-times-circle"></i> Remove', 'remove'); ?></div>
                                                </div>
                                            <?php } ?>
                                        <?php else : ?>
                                            <p>No results found...</p>
                                        <?php endif; ?>                                        
                                    <?php } ?>
                                    <?php else : ?>
                                        <p>You don't have any saved activities yet</p>
                                    <?php endif; ?>
                                </div>
                                <?php wp_reset_query(); ?>
                                <?php the_content(); ?>
                            </div>

                            <?php do_action('himalayas_after_post_content'); ?>
                        </article>

                        <?php
                        do_action('himalayas_before_comments_template');
                        // If comments are open or we have at least one comment, load up the comment template
                        if (comments_open() || '0' != get_comments_number())
                            comments_template();
                        do_action('himalayas_after_comments_template');

                    endwhile;
                    ?>
                </div><!-- #content-2 -->
            </div><!-- #primary -->

            <?php himalayas_sidebar_select(); ?>
        </div>
    </main>
</div>

<?php do_action('himalayas_after_body_content'); ?>

<?php get_footer(); ?>
