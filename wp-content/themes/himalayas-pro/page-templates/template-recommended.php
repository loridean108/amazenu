<?php
/**
 * Template Name: Videos - Recommended
 *
 * Displays the Testimonial Template of the theme.
 *
 * @package ThemeGrill
 * @subpackage Himalayas Pro
 * @since Himalayas Pro 1.0
 */
?>

<?php 
    global $current_user;
    get_currentuserinfo();
?>

<?php get_header(); ?>

<?php
do_action('himalayas_before_body_content');

$himalayas_layout = himalayas_layout_class();
?>

<div id="content" class="site-content">
    <main id="main" class="clearfix <?php echo $himalayas_layout; ?>">
        <div class="tg-container">

            <div id="primary">
                <div id="content-2">
                    <?php
                    while (have_posts()) : the_post();
                        ?>

                        <article id = "post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <?php do_action('himalayas_before_post_content'); ?>

                            <?php
                            $outcome_terms = get_outcome_terms();
                            $grade_level_id   = get_user_meta( get_current_user_id(), 'rcp_grade_level', true );
                            $desired_outcome_id   = get_user_meta( get_current_user_id(), 'rcp_desired_outcome', true );
                            $activity_id   = get_user_meta( get_current_user_id(), 'rcp_activity', true );
                            $grade_level = get_term($grade_level_id, 'category');
                            $desired_outcome = get_term($desired_outcome_id, 'category');
                            $activity = get_term($activity_id, 'category');
                            ?>

                            <h2 class="entry-title" style="display: inline-block;"><?php echo $current_user->first_name; ?>'s class:</h2>
                            <ul class="filter-panel">
                                <li><a class="btn btn-filter" href="<?php echo get_bloginfo('url') . '/grade-level/'; ?>"><?php echo $grade_level->name; ?></a></li>
                                <li><a class="btn btn-filter" href="<?php echo get_bloginfo('url') . '/desired-outcome/'; ?>"><?php echo $desired_outcome->name; ?></a></li>
                                <li><a class="btn btn-filter" href="<?php echo get_bloginfo('url') . '/activity/'; ?>"><?php echo $activity->name; ?></a></li>
                            </ul>

                            <div class="entry-content">
                                <h4>Choose your <?php echo $activity->name; ?> activity</h4>
                                <?php
                                $args = array(
                                    'post_type' => 'video',
                                    'category__and' => array($grade_level->term_id, $desired_outcome->term_id, $activity->term_id)
                                );
                                query_posts($args);
                                ?>
                                <div class="video-list">
                                    <?php if (have_posts()) : ?>
                                        <?php while (have_posts()) : the_post(); ?>
                                            <?php
                                            if (get_field('vimeo_video_link')) {
                                                ?>
                                                <a href="<?php the_permalink(); ?><?php echo '?lvl=' . $termLvl->term_id . '&oc=' . $termOC->term_id . '&act=' . $term->term_id; ?>">
                                                    <div class="video-box">

                                                        <div class="content">
                                                            <?php $videoID = get_vimeoid(get_field('vimeo_video_link')); ?>
                                                            <img class="video-thub" src="<?php print get_vimeo_thumbnail($videoID); ?>" />
                                                            <img class="video-play" src="<?php echo get_template_directory_uri(); ?>/images/video-play.png" />
                                                        </div>

                                                        <h5><?php the_title(); ?></h5>
                                                        <p class="duration">Duration: <?php echo get_post_field('video_duration', $video->ID); ?></p>
                                                    
                                                    </div>
                                                </a>
                                            <?php } ?>
                                        <?php endwhile; ?>
                                    <?php else : ?>
                                        <p class="no-results-found">No results found. Please make a different selection.</p>
                                    <?php endif; ?>
                                </div>
                                <?php wp_reset_query(); ?>
                                <?php the_content(); ?>
                            </div>

                            <?php do_action('himalayas_after_post_content'); ?>
                        </article>

                        <?php
                        do_action('himalayas_before_comments_template');
                        // If comments are open or we have at least one comment, load up the comment template
                        if (comments_open() || '0' != get_comments_number())
                            comments_template();
                        do_action('himalayas_after_comments_template');

                    endwhile;
                    ?>
                </div><!-- #content-2 -->
            </div><!-- #primary -->

            <?php himalayas_sidebar_select(); ?>
        </div>
    </main>
</div>

<?php do_action('himalayas_after_body_content'); ?>

<?php get_footer(); ?>