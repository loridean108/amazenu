<?php
/**
 * Template Name: Videos - Grade Level
 *
 * Displays the Testimonial Template of the theme.
 *
 * @package ThemeGrill
 * @subpackage Himalayas Pro
 * @since Himalayas Pro 1.0
 */
?>

<?php 
    global $current_user;
    get_currentuserinfo();
?>

<?php get_header(); ?>

<?php
do_action('himalayas_before_body_content');

$himalayas_layout = himalayas_layout_class();
?>

<div id="content" class="site-content">
    <main id="main" class="clearfix <?php echo $himalayas_layout; ?>">
        <div class="tg-container">

            <div id="primary">
                <div id="content-2">
                    <?php
                    while (have_posts()) : the_post();
                        ?>

                        <article id = "post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <?php 
                            do_action('himalayas_before_post_content');

                            $grade_level_terms = get_grade_level_terms();

                            ?>

                            <h2 class="entry-title" style="display: inline-block;">Welcome <?php echo $current_user->first_name; ?>!</h2>

                            <div class="entry-content">
                                <?php the_content(); ?>
                                

                                <div class="category-selector grades-selector">
                                    <?php foreach ($grade_level_terms as $term) { ?>
				                        <div class="category-box">
                                            <a href="<?php echo get_bloginfo('url') . '/desired-outcome/?lvl=' . $term->term_id; ?>">

                                                    <h4><?php echo $term->name; ?></h4>
                                                    <!--<p><?php echo ($term->description != '') ? $term->description : '...'; ?></p>-->
                                            </a>
					                   </div>
                                    <?php } ?>
                                </div>
                            </div>

                            <?php do_action('himalayas_after_post_content'); ?>
                        </article>

                        <?php
                        do_action('himalayas_before_comments_template');
                        // If comments are open or we have at least one comment, load up the comment template
                        if (comments_open() || '0' != get_comments_number())
                            comments_template();
                        do_action('himalayas_after_comments_template');

                    endwhile;
                    ?>
                </div><!-- #content-2 -->
            </div><!-- #primary -->

            <?php himalayas_sidebar_select(); ?>
        </div>
    </main>
</div>

<?php do_action('himalayas_after_body_content'); ?>

<?php get_footer(); ?>