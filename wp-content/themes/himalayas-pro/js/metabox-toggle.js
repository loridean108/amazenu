jQuery(document).ready( function() {
   jQuery('#page_template').change(function() {
      jQuery('#services-icon').toggle(jQuery(this).val() == 'page-templates/template-services.php');
      jQuery('#team-designation').toggle(jQuery(this).val() == 'page-templates/template-team.php');
      jQuery('#team-social').toggle(jQuery(this).val() == 'page-templates/template-team.php');
      jQuery('#testimonial-designation').toggle(jQuery(this).val() == 'page-templates/template-testimonial.php');
   }).change();
});