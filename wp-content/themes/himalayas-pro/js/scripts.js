/*global jQuery*/

(function ($, window) {

    "use strict";

    // custom namespace
    window.amaZEN = window.amaZEN || {};

    // global variables
    var windowSmall   =  $(window).width() < 768;
    

    amaZEN.global = {

        init: function(){
            this.navFixes();
            //this.animButton();
	       this.navToggle();
           this.removeFavorites();
        },


        navFixes: function(){

            $('document').on('scroll', function(){

                if ($('.header-wrapper').hasClass('stick')) {
                    console.log('true');
                    $('.caption-title').animate({
                        marginTop: '50px'
                    }, 500);
                } else {
                    $('.caption-title').animate({
                            marginTop: 0
                    }, 500);
                }

                return false;
            });

            
        },

        animButton: function(){
            $('.home-banner .button').on('click', function(e){
                e.preventDefault();
                var sectionTop = $('#howitworks').offset().top;

                $('body, html').animate({
                    scrollTop: sectionTop - 54
                }, 800)
            });
        },
	
    	navToggle: function(){
 /*
 //disabled to prevent double register of onclick method when static page is enabled. Duplicate code in js/himalayas-custom(min).js
    		$('.page .menu-toggle').on('click', function(){
    			$(this).next('.menu-primary-container').slideToggle();
    		})
*/
    	},


        removeFavorites: function(){
            $('.my-favorites .video-box .btn-remove .wpfp-link').on('click', function(){
                $(this).parent().parent().fadeOut( function(){
                    $(this).remove();

                    if ( $('.my-favorites').children().length === 0){
                        $('.my-favorites').append('<p>You do not have any saved activities.</p>');
                     }
                })
            });



        }



    }

    amaZEN.global.init();


}(jQuery, this));


jQuery(document).ready(function(){
    jQuery('#select_grade_level').change(function(){
        jQuery('#formGradeLvl').submit();
    });
});
