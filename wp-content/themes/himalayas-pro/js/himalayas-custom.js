jQuery(document).ready(function() {
   // StickyNav
   var stickyNavTop = jQuery('.header-wrapper').height();

   var stickyNav = function() {
      var scrollTop = jQuery(window).scrollTop();

      if (scrollTop > stickyNavTop) {
         jQuery('.header-wrapper').addClass('stick');
      }else {
         jQuery('.header-wrapper').removeClass('stick');
      }
   };
   stickyNav();

   jQuery(window).scroll(function() {
      stickyNav();
   });

   // OnePageNav
   if ( typeof jQuery.fn.onePageNav !== 'undefined' ) {
	   jQuery('#site-navigation').onePageNav({
	      currentClass: 'current-one-page-item',
	      changeHash: false,
	      scrollSpeed: 1500,
	      scrollThreshold: 0.5,
	      filter: '',
	      easing: 'swing',
	      begin: function() {
	         //I get fired when the animation is starting
	      },
	      end: function() {
	      	//I get fired when the animation is ending
	      },
	      scrollChange: function(jQuerycurrentListItem) {
	         //I get fired when you enter a section and I pass the list item of the section
	      }
	   });
	}

	// ScrollUp
	jQuery(window).scroll(function() {
      if (jQuery(this).scrollTop() > 100) {
         jQuery('.scrollup').fadeIn();
      } else {
         jQuery('.scrollup').fadeOut();
      }
   });

   jQuery('.scrollup').click(function() {
      jQuery("html, body").animate({
         scrollTop: 0
      }, 2000);
      return false;
   });

   // Search Icon
   jQuery('.search-icon').click(function() {
   	jQuery('.search-box').toggleClass('active');
   });

   // Search Box
   jQuery('.search-box .close').click(function() {
    	jQuery('.search-box').removeClass('active');
   });

   // Init WOW.js
	if (typeof WOW == 'function') {
    	new WOW().init();
   }

   // CounterUP
   if ( typeof jQuery.fn.counterUp !== 'undefined' ) {
    	jQuery('.counter').counterUp();
   }

   // Parallax Setting
   if ( typeof jQuery.fn.parallax !== 'undefined' ) {

	  	jQuery(window).on('load', function() {

			var width = Math.max(window.innerWidth, document.documentElement.clientWidth);

        	if ( width && width >= 768 ) {
            jQuery('.parallax-section').each(function() {
            	jQuery(this).parallax('center', 0.2, true);
            });
        	}

        	 if (width && width <= 768) {
	        jQuery('.menu-toggle,#site-navigation a').click(function() {
	            jQuery('#site-navigation .menu-primary-container,#site-navigation div.menu').slideToggle();
	        });
	    	}
    	});
	}

	if ( typeof jQuery.fn.bxSlider !== 'undefined' ) {
		if (typeof himalayas_slider_value !== 'undefined') {
			var transition_effect = himalayas_slider_value.transition_effect;
	   	var transition_delay = himalayas_slider_value.transition_delay;
	   	var transition_duration = himalayas_slider_value.transition_duration;
	   	jQuery('.bxslider').bxSlider({
	   	   auto: true,
	   	   pager: false,
	   	   mode: transition_effect,
	   	   speed: transition_duration,
	   	   pause: transition_delay,
	   	   adaptiveHeight: true,
	   	   autoHover: true
	   	});
	 	}

	   jQuery('.testimonial-bxslider').bxSlider({
	      auto: true,
	      pager: true,
	      speed: 1000,
	      pause: 5000,
	      adaptiveHeight: true,
	      autoHover: true,
	      controls: false
	   });

	   jQuery('.client-slider').bxSlider({
	      auto: false,
	      pager: false,
	      controls: true,
	      speed: 1000,
	      pause: 5000,
	      adaptiveHeight: true,
	      autoHover: true,
	      minSlides: 3,
	      maxSlides: 5,
	      slideWidth: 220,
	      slideMargin: 20,
	      nextText: '<i class="fa fa-angle-right"></i>',
	      prevText: '<i class="fa fa-angle-left"></i>',
	   });
	}

	if ( typeof jQuery.fn.magnificPopup !== 'undefined' ) {
		jQuery('.image-popup').magnificPopup({type: 'image'});
		// Gallery Image Popup
   	jQuery('.gallery-wrap').magnificPopup({
      	delegate: 'a',
      	type: 'image',
      	gallery: { enabled:true },
      	zoom: { enabled:true }
		});
   }
});