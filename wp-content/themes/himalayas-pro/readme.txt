== HIMALAYAS Pro  ==

Himalayas Pro is a premium one page WordPress theme.

/**********************************************************/

== COPYRIGHT AND LICENSE ==

External resources linked to the theme.
* Roboto by Christian Robertson https://www.google.com/fonts/specimen/Roboto
  Apache License, version 2.0
* Crimson Text by Sebastian Kosch https://www.google.com/fonts/specimen/Crimson+Text
  SIL Open Font License, 1.1

Resources packed within the theme.
* Font Awesome by Dave Gandy http://fortawesome.github.io/Font-Awesome/
  SIL OFL 1.1 http://scripts.sil.org/OFL.
* Images in the screenshot is from http://www.pexels.com/photo/sea-beach-surfer-coast-6942/. License: CC0
* HTML5 Shiv @afarkas @jdalton @jon_neal @rem | MIT/GPL2 Licensed
* Magnific-Popup by Dmitry Semenov | MIT Licensed
* jQuery-One-Page-Nav by Trevor Davis | MIT/GPL Licensed
* jQuery Parallax by Ian Lunn | MIT/GPL Licensed
* Bxslider by Steven Wanderski released under the MIT license
* Other custom js files are our own creation and is licensed under the same license as this theme.

All other resources and theme elements are licensed under the [GNU GPL](http://www.gnu.org/licenses/gpl-3.0.txt), version 3 or later.

Himalayas Pro WordPress Theme, Copyright 2015 ThemeGrill
Himalayas Pro is distributed under the terms of the GNU GPL

/**********************************************************/

== THEME USAGE ==

= Theme Instruction =
Get theme instruction at http://themegrill.com/theme-instruction/himalayas-pro/

= Free Support =
Get free support at http://themegrill.com/support-forum

= Theme Features Usage =
All available options can be used from Appearance->Customize

/**********************************************************/

== TRANSLATIONS ==
If you've translated this theme into your language, feel free to send the translation over to themegrill@gmail.com
and we will include it within the theme from next version update.

/**********************************************************/

== CHANGE LOG ==
= Version 1.0.1 =
* Responsive menu bug fixed.
* Background color options bug fixed.
* Content Background color option is removed.
* TG: Our Clients widget issue fixed.
* TG: Featured Post widget issue fixed.
* SEO Optimization.
* Minified the scripts and CSS code used in the theme.

= Version 1.0 =
* All brand new.